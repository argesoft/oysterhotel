﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Net;
using System.IO;
/// <summary>
/// Summary description for HttpRequests
/// </summary>
public static class HttpRequests
{
    public static string HttpPostRequest(string url, string post)
    {
        var encoding = new ASCIIEncoding();
        byte[] data = encoding.GetBytes(post);
        WebRequest request = WebRequest.Create(url);
        request.Method = "POST";
        request.ContentType = "application/x-www-form-urlencoded";
        request.ContentLength = data.Length;
        Stream stream = request.GetRequestStream();
        stream.Write(data, 0, data.Length);
        stream.Close();
        WebResponse response = request.GetResponse();
        String result;
        using (var sr = new StreamReader(response.GetResponseStream()))
        {
            result = sr.ReadToEnd();
            sr.Close();
        }
        return result;
    }


    public static string HttpGetRequest(string url, string[] headers)
    {
        String result;
        WebRequest request = WebRequest.Create(url);
        if (headers.Length > 0)
        {
            foreach (var header in headers)
            {
                request.Headers.Add(header);
            }
        }
        WebResponse response = request.GetResponse();
        using (var sr = new StreamReader(response.GetResponseStream()))
        {
            result = sr.ReadToEnd();
            sr.Close();
        }
        return result;
    }
}



