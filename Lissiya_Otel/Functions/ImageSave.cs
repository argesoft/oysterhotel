﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Linq;
using System.Web;

namespace Lissiya_Otel.Functions
{
    public class ImageSave
    {
        public static void InsertImage(System.Drawing.Image originalImage, string fileName)
        {
            var path = string.Empty;
            var pathThumb = string.Empty;
            var pathThumb40 = string.Empty;
            var extension = fileName.Substring(fileName.LastIndexOf('.'));
            path = System.IO.Path.Combine(HttpContext.Current.Server.MapPath("~/media/images/"), fileName);
            pathThumb = System.IO.Path.Combine(HttpContext.Current.Server.MapPath("~/media/images/thumbnails/"), fileName);
            pathThumb40 = System.IO.Path.Combine(HttpContext.Current.Server.MapPath("~/media/images/thumbnails40/"), fileName);
            SaveJpeg(path, originalImage, 100, extension);

            SaveJpeg(pathThumb, ScaleByPercent(originalImage, 60, extension), 50, extension);

            SaveJpeg(pathThumb40, ScaleByPercent(originalImage, 40, extension), 50, extension);
        }

        static Image ScaleByPercent(Image imgPhoto, int Percent, string extension)
        {
            float nPercent = ((float)Percent / 100);

            int sourceWidth = imgPhoto.Width;
            int sourceHeight = imgPhoto.Height;
            int sourceX = 0;
            int sourceY = 0;

            int destX = 0;
            int destY = 0;
            int destWidth = (int)(sourceWidth * nPercent);
            int destHeight = (int)(sourceHeight * nPercent);

            Bitmap bmPhoto = new Bitmap(destWidth, destHeight,
                                     PixelFormat.Format32bppPArgb);
            bmPhoto.SetResolution(imgPhoto.HorizontalResolution,
                                    imgPhoto.VerticalResolution);

            Graphics grPhoto = Graphics.FromImage(bmPhoto);
            grPhoto.InterpolationMode = InterpolationMode.High;

            grPhoto.DrawImage(imgPhoto,
                new Rectangle(destX, destY, destWidth, destHeight),
                new Rectangle(sourceX, sourceY, sourceWidth, sourceHeight),
                GraphicsUnit.Pixel);

            grPhoto.Dispose();
            return bmPhoto;
        }


        public static void SaveJpeg(string path, System.Drawing.Image img, int quality, string extension)
        {
            System.Drawing.Imaging.EncoderParameter qualityParam = new System.Drawing.Imaging.EncoderParameter(System.Drawing.Imaging.Encoder.Quality, quality);
            System.Drawing.Imaging.ImageCodecInfo jpegCodec = null;
            switch (extension)
            {
                case ".jpeg":
                    jpegCodec = System.Drawing.Imaging.ImageCodecInfo.GetImageEncoders().FirstOrDefault(_ => _.MimeType == "image/jpeg");
                    break;
                case ".png":
                    jpegCodec = System.Drawing.Imaging.ImageCodecInfo.GetImageEncoders().FirstOrDefault(_ => _.MimeType == "image/png");
                    break;
                default:
                    jpegCodec = System.Drawing.Imaging.ImageCodecInfo.GetImageEncoders().FirstOrDefault(_ => _.MimeType == "image/jpeg");
                    break;
            }

            System.Drawing.Imaging.EncoderParameters encoderParams = new System.Drawing.Imaging.EncoderParameters(1);
            encoderParams.Param[0] = qualityParam;
            System.IO.MemoryStream mss = new System.IO.MemoryStream();
            System.IO.FileStream fs = new System.IO.FileStream(path, System.IO.FileMode.Create, System.IO.FileAccess.ReadWrite);
            img.Save(mss, jpegCodec, encoderParams);
            byte[] buff = mss.ToArray();
            fs.Write(buff, 0, buff.Length);
            mss.Close();
            fs.Close();
        }

    }
}