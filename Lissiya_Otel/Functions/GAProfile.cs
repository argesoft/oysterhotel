﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GAConnect
{
    public class GAProfile
    {
        public string ID { get; set; }
        public DateTime Updated { get; set; }
        public string AccountID { get; set; }
        public string AccountName { get; set; }
        public string ProfileID { get; set; }
        public string WebPropertyID { get; set; }
        public string Currency { get; set; }
        public string TimeZone { get; set; }
        public string TableID { get; set; }
        public string Title { get; set; }
    }
}
