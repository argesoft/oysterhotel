﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Xml;

namespace GAConnect
{
    public class BaseData
    {

        public IEnumerable<KeyValuePair<Dimension, string>> Dimensions { get; set; }
        public IEnumerable<KeyValuePair<Metric, string>> Metrics { get; set; }

        private const string PageViewReportUrl =
          "https://www.googleapis.com/analytics/v2.4/data?ids={0}&dimensions={1}&metrics={2}&start-date={3}&end-date={4}&sort={5}&max-results={6}";
        internal static string authenticationKey { get; set; }



        private static XDocument getXMLData(string tableID, IEnumerable<Dimension> dimensions,
            IEnumerable<Metric> metrics, DateTime from, DateTime to, Metric sort, SortDirection direction, int maxrecords)
        {
            XDocument doc = null;
            if (authenticationKey.Length > 0)
            {
                var dimension = new StringBuilder();
                for (var i = 0; i < dimensions.Count(); i++)
                {
                    dimension.Append("ga:" + dimensions.ElementAt(i));
                    if (i < dimensions.Count() - 1)
                        dimension.Append(",");
                }
                var metric = new StringBuilder();
                for (var i = 0; i < metrics.Count(); i++)
                {
                    metric.Append("ga:" + metrics.ElementAt(i));
                    if (i < metrics.Count() - 1)
                        metric.Append(",");
                }
                var sorter = "ga:" + sort;
                if (direction == SortDirection.Descending)
                    sorter = "-" + sorter;
                var fromDate = from.ToString("yyyy-MM-dd");
                var toDate = to.ToString("yyyy-MM-dd");
                var header = new[] { "Authorization: GoogleLogin " + authenticationKey };
                var url = string.Format(PageViewReportUrl, "ga:" + tableID, dimension, metric, fromDate, toDate, sorter, maxrecords);

                doc = XDocument.Parse(HttpRequests.HttpGetRequest(url, header));
            }
            return doc;
        }


        public static IEnumerable<BaseData> GetBaseData(string tableID, IEnumerable<Dimension> dimensions,
            IEnumerable<Metric> metrics, DateTime from, DateTime to, Metric sort, SortDirection direction, int maxrecords)
        {
            IEnumerable<BaseData> data = null;
            XDocument xml = getXMLData(tableID, dimensions, metrics, from, to, sort, direction, maxrecords);
            if (xml != null)
            {
                XNamespace dxp = xml.Root.GetNamespaceOfPrefix("dxp");
                XNamespace dns = xml.Root.GetDefaultNamespace();
                data = xml.Root.Descendants(dns + "entry").Select(element => new BaseData
                {
                    Dimensions =
                      new List<KeyValuePair<Dimension, string>>(
                      element.Elements(dxp + "dimension").Select(
                        dimensionElement =>
                        new KeyValuePair<Dimension, string>(
                          dimensionElement.Attribute("name").Value.Replace("ga:", "")
                          .ParseEnum<Dimension>(),
                          dimensionElement.Attribute("value").Value))),
                    Metrics =
                      new List<KeyValuePair<Metric, string>>(
                      from metricElement in element.Elements(dxp + "metric")
                      select new KeyValuePair<Metric, string>(
                        metricElement.Attribute("name").Value.Replace("ga:", "")
                          .ParseEnum<Metric>(),
                        metricElement.Attribute("value").Value))
                });
            }
            return data;
        }

        public static XDocument GetProfilesData(string email, string pwd)
        {
            XDocument docAnalyticsData = null;
            if (authenticationKey.Length > 0)
            {
                var header = new[] { "Authorization: GoogleLogin " + authenticationKey };
                docAnalyticsData = XDocument.Parse(HttpRequests.HttpGetRequest("https://www.googleapis.com/analytics/v3/data", header));
            }
            return docAnalyticsData;
        }

    }
}
