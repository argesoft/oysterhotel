﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Lissiya_Otel.Functions
{
    public class DocumentSave
    {
        public static void InsertDocument(HttpPostedFileBase file, string filename)
        {
            var path = string.Empty;
            var extension = filename.Substring(filename.LastIndexOf('.'));
            path = System.IO.Path.Combine(HttpContext.Current.Server.MapPath("~/media/documents/"), filename);
            file.SaveAs(path);
        }

    }
}