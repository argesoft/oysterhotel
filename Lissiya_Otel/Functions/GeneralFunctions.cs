﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Text.RegularExpressions;
using System.Net.Mail;
using System.Net;
using Lissiya_Otel.Models;
using System.Text;
using PagedList;

namespace Lissiya_Otel.Functions
{
    public class GeneralFunctions
    {
        private static lissiyaEntities db = new lissiyaEntities();

        public static string Resolve(object queryString)
        {
            string language = string.Empty;
            if (queryString != null)
            {
                if (queryString.ToString() == "en")
                {
                    language = "en";
                }
                else
                {
                    language = "tr";
                }
            }
            else
            {
                language = "tr";
            }

            return language;
        }

        public static int GetLanguage(object queryString)
        {
            string language = string.Empty;
            if (queryString != null)
            {
                if (queryString.ToString() == "en")
                {
                    return 2;
                }
                else if (queryString.ToString() == "tr")
                {
                    return 1;
                }
                else if (queryString.ToString() == "ru")
                {
                    return 4;
                }
                else if (queryString.ToString() == "zh")
                {
                    return 8;
                }
                else if (queryString.ToString() == "jp")
                {
                    return 9;
                }
                else
                {
                    return 2;
                }
            }
            else
            {
                return 2;
            }
        }

        public static string GetSettingValue(string SettingName)
        {
            string Value = (from x in db.Settings where x.SettingName == SettingName select x).SingleOrDefault().SettingValue;

            return Value;
        }

        public static bool SetSettingValue(string SettingName, string SettingValue)
        {
            try
            {
                var Detail = (from x in db.Settings where x.SettingName == SettingName select x).SingleOrDefault();

                Detail.SettingValue = SettingValue;

                db.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static string GetCityName(int CityId)
        {
            string Value = (from x in db.Cities where x.CityId == CityId select x).SingleOrDefault().CityName;

            return Value;
        }

        public static string GetDistrictName(int DistrictId)
        {
            string Value = (from x in db.Districts where x.DistrictId == DistrictId select x).SingleOrDefault().DistrictName;

            return Value;
        }

        public static string ReplaceUrlRouting(string str)
        {
            str = str.ToLower();
            str = str.Replace("'", "");
            str = str.Replace(".", "");
            str = str.Replace(":", "");
            str = str.Replace(">", "");
            str = str.Replace("<", "");
            str = str.Replace("]", "");
            str = str.Replace("[", "");
            str = str.Replace("&", "");
            str = str.Replace("'", "");
            str = str.Replace(">", "");
            str = str.Replace("<", "");
            str = str.Replace("]", "");
            str = str.Replace("[", "");
            str = str.Replace("&", "");
            str = str.Replace("ç", "c");
            str = str.Replace("Ç", "c");
            str = str.Replace("ğ", "g");
            str = str.Replace("Ğ", "g");
            str = str.Replace("ı", "i");
            str = str.Replace("İ", "i");
            str = str.Replace("I", "i");
            str = str.Replace("ö", "o");
            str = str.Replace("Ö", "o");
            str = str.Replace("Ş", "s");
            str = str.Replace("ş", "s");
            str = str.Replace("Ü", "u");
            str = str.Replace("ü", "u");
            str = str.Replace(" ", "-");
            str = str.Replace("#", "sharp");
            return str;
        }

        public static Languages LanguageDetail(int LanguageId)
        {
            var LanguageDetail = (from x in db.Languages where x.LanguageId == LanguageId select x).SingleOrDefault();

            if (LanguageDetail != null)
            {
                return LanguageDetail;
            }
            else
            {
                return null;
            }
        }

        public static string RemoveHtml(string Html)
        {
            string deger;
            deger = Regex.Replace(Html, @"<(.|\n)*?>", string.Empty);

            return deger;
        }

        /// <summary>
        /// id 1 = Ekleme , id 2 = Güncelleme , id 3 = Silme
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static string ReturnSuccess(int id)
        {
            string success = "";
            switch (id)
            {
                case 1:
                    success = "Ekleme işlemi başarılı bir şekilde tamamlandı.";
                    break;

                case 2:
                    success = "Güncelleme işlemi başarılı bir şekilde tamamlandı.";
                    break;

                case 3:
                    success = "Silme işlemi başarılı bir şekilde tamamlandı.";
                    break;

                default:
                    success = "İşlem başarılı bir şekilde tamamlandı.";
                    break;
            }

            return success;
        }

        /// <summary>
        /// id 1 = Ekleme , id 2 = Güncelleme , id 3 = Silme,  id 4 = Dil Seçiniz
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static string ReturnError(int id)
        {
            string error = "";
            switch (id)
            {
                case 1:
                    error = "Ekleme işlemi sırasında bir hata oluştu. Lütfen daha sonra tekrar deneyiniz.";
                    break;

                case 2:
                    error = "Güncelleme işlemi sırasında bir hata oluştu. Lütfen daha sonra tekrar deneyiniz.";
                    break;

                case 3:
                    error = "Silme  işlemi sırasında bir hata oluştu. Lütfen daha sonra tekrar deneyiniz.";
                    break;

                case 4:
                    error = "Lütfen Dil Seçiniz";
                    break;

                default:
                    error = "İşlem sırasında bir hata oluştu. Lütfen daha sonra tekrar deneyiniz.";
                    break;
            }

            return error;
        }

        /// <summary>
        /// id = 1 Bu dilde ekleme yapılmış , 2 Kullanıcı Adı Kullanımdadır , 3 Admin Silinemez
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static string ReturnInfo(int id)
        {
            string info = "";
            switch (id)
            {
                case 1:
                    info = "Seçilen dilde ekleme yapılmıştır. Lütfen başka bir dil seçiniz.";
                    break;

                case 2:
                    info = "Bu Kullanıcı adı kullanımdadır.";
                    break;

                case 3:
                    info = "Admin Silinemez!";
                    break;

                case 4:
                    info = "Lütfen Dil Seçiniz";
                    break;

                default:
                    info = "İşlem sırasında bir hata oluştu. Lütfen daha sonra tekrar deneyiniz.";
                    break;
            }

            return info;
        }

        public static bool SendMail(string Host, int Port, string Username, string Password, string To, string From, int Ssl, int Credantials, string ContactNameSurname, string MailAddress, string Tarih1, string Tarih2, int KisiSayisi, string ContactMessage)
        {
            try
            {
                string host = Host;
                int port = Port;
                string username = Username;
                string password = Password;
                string from = From;
                MailMessage mailim = new MailMessage();

                mailim.To.Add(To);  // kime gidecek
                mailim.From = new MailAddress(username);   //kimden gidecek
                mailim.Subject = "Lissiya Hotel Rezervasyon Formu";   // e-mail konusu
                mailim.IsBodyHtml = true;
                mailim.Body = ("<table>" +
                            "<tr>" +
                                "<td>Ad Soyad : </td><td>" + ContactNameSurname + "</td>" +
                            "</tr>" +
                            "<tr>" +
                                "<td>E-Mail : </td><td>" + MailAddress + "</td>" +
                            "</tr>" +
                            "<tr>" +
                                "<td>Varış Tarihi : </td><td>" + Tarih1 + "</td>" +
                            "</tr>" +
                              "<tr>" +
                                "<td>Ayrılış Tarihi Tarihi : </td><td>" + Tarih2 + "</td>" +
                            "</tr>" +
                            "<tr>" +
                                "<td>Kişi Sayısı : </td><td>" + KisiSayisi + "</td>" +
                            "</tr>" +
                            "<tr>" +
                            "<td>Mesaj : </td><td>" + ContactMessage + "</td>" +
                            "</tr>" +
                            "</table>");
                //e-mail içeriği

                NetworkCredential yetki = new NetworkCredential(username, password);

                SmtpClient mail_client = new SmtpClient(host, port);

                if (Ssl == 1)
                {
                    mail_client.EnableSsl = true;
                }
                else
                {
                    mail_client.EnableSsl = false;
                }
                if (Credantials == 1)
                {
                    mail_client.UseDefaultCredentials = true;
                }
                else
                {
                    mail_client.UseDefaultCredentials = false;
                }

                mail_client.Credentials = yetki;
                mail_client.Send(mailim);

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static string ReturnDocumentImage(string filename)
        {
            string fileext = filename.Substring(filename.LastIndexOf('.'));

            if (fileext == ".pdf" || fileext == ".PDF")
            {
                return "/Areas/Admin/img/pdf.png";
            }
            else if (fileext == ".doc" || fileext == ".DOC" || fileext == ".docx" || fileext == ".DOCX")
            {
                return "/Areas/Admin/img/doc.png";
            }
            else if (fileext == ".xls" || fileext == ".XLS" || fileext == ".xlsx" || fileext == ".XLSX")
            {
                return "/Areas/Admin/img/excel.png";
            }
            else if (fileext == ".ppt" || fileext == ".PPT" || fileext == ".PPTX" || fileext == ".pptx")
            {
                return "/Areas/Admin/img/power.png";
            }
            else if (fileext == ".rar" || fileext == ".RAR")
            {
                return "/Areas/Admin/img/rar.png";
            }
            else if (fileext == ".txt" || fileext == ".TXT")
            {
                return "/Areas/Admin/img/txt.png";
            }
            else if (fileext == ".zip" || fileext == ".ZIP")
            {
                return "/Areas/Admin/img/zip.png";
            }

            return string.Empty;
        }

        public static MenuNames GetMenuDetail(int MenuId)
        {
            var Detail = (from x in db.MenuNames where x.MenuId == MenuId select x).SingleOrDefault();

            if (Detail != null)
            {
                return Detail;
            }

            return null;
        }

        public static ContentNames GetContentDetail(int ContentId)
        {
            var Detail = (from x in db.ContentNames where x.ContentId == ContentId select x).SingleOrDefault();

            if (Detail != null)
            {
                return Detail;
            }

            return null;
        }

        public static List<Dictionaries> Dictionary(int LanguageId)
        {
            var List = (from x in db.Dictionaries where x.LanguageId == LanguageId orderby x.Name ascending select x).ToList();

            if (List.Count > 0)
            {
                return List;
            }

            return null;
        }

        public static int GalleyImageDetailWithGalleryId(int Imageid, int GalleryId)
        {
            var Detail = (from x in db.GalleryImages where x.ImageId == Imageid && x.GalleryId == GalleryId select x).ToList();

            if (Detail.Count > 0)
            {
                return Detail.Count;
            }
            else
            {
                return 0;
            }
        }

        public static int GalleyImageDetailWithLanguageId(int Imageid, int LanguageId)
        {
            var Detail = (from x in db.GalleryImages where x.ImageId == Imageid && x.LanguageId == LanguageId select x).ToList();

            if (Detail.Count > 0)
            {
                return Detail.Count;
            }
            else
            {
                return 0;
            }
        }

        public static string DictionaryValue(int LanguageId, string Name)
        {
            var Detail = (from x in db.Dictionaries where x.LanguageId == LanguageId && x.Name == Name select x).SingleOrDefault();

            if (Detail != null)
            {
                return Detail.Value;
            }
            else
            {
                return null;
            }
        }
    }
}