﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Filters;
using System.Web.Mvc;
using Lissiya_Otel.Models;
using System.Web.Routing;

namespace Lissiya_Otel.Functions
{
    public class Authorization : System.Web.Mvc.ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            LoginAccounts acc = (LoginAccounts)HttpContext.Current.Session["account"];
            if (HttpContext.Current.Request.CurrentExecutionFilePath.StartsWith("/Admin") || HttpContext.Current.Request.CurrentExecutionFilePath.StartsWith("/admin"))
            {
                var db = new lissiyaEntities();
                if (acc == null)
                {

                    if (HttpContext.Current.Request.Cookies["rememberme"] != null)
                    {
                        String username = HttpContext.Current.Request.Cookies["rememberme"].Values["username"];
                        String password = HttpContext.Current.Request.Cookies["rememberme"].Values["password"];

                        LoginAccounts account = (from x in db.LoginAccounts where x.LoginName == username && x.LoginPassword == password select x).SingleOrDefault();
                        if (account != null)
                        {
                            HttpContext.Current.Session["account"] = account;
                            filterContext.Result = new RedirectResult("~/Admin/Home/Dashboard");
                            return;

                        }

                        filterContext.Result = new EmptyResult();
                        filterContext.Result = new RedirectResult("~/Admin/Home/Login");
                        return;
                    }


                    filterContext.Result = new EmptyResult();
                    filterContext.Result = new RedirectResult("~/Admin/Home/Login");
                    return;
                }

            }

            base.OnActionExecuting(filterContext);
        }

    }
}