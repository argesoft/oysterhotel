﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GAConnect
{
    public class GAData
    {
        public int Pageviews { get; set; }
        public int Bounces { get; set; }
        public int Entrances { get; set; }
        public int Exits { get; set; }
        public int NewVisits { get; set; }
        public double TimeOnPage { get; set; }
        public double TimeOnSite { get; set; }
        public int Visitors { get; set; }
        public int Visits { get; set; }
        public int UniquePageviews { get; set; }
        public string ExitPagePath { get; set; }
        public string LandingPagePath { get; set; }
        public string NextPagePath { get; set; }
        public string PagePath { get; set; }
        public string PageTitle { get; set; }
        public string PreviousPagePath { get; set; }
        public string SecondPagePath { get; set; }
        public string Browser { get; set; }
        public string BrowserVersion { get; set; }
        public string City { get; set; }
        public string ConnectionSpeed { get; set; }
        public string Country { get; set; }
        public string Date { get; set; }
        public string DaysSinceLastVisit { get; set; }
        public string Day { get; set; }
        public string FlashVersion { get; set; }
        public string Hostname { get; set; }
        public string IsMobile { get; set; }
        public string Hour { get; set; }
        public string JavaEnabled { get; set; }
        public string Language { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string Month { get; set; }
        public string NetworkDomain { get; set; }
        public string NetworkLocation { get; set; }
        public string OperatingSystem { get; set; }
        public string OperatingSystemVersion { get; set; }
        public string PageDepth { get; set; }
        public string Region { get; set; }
        public string ScreenColors { get; set; }
        public string ScreenResolution { get; set; }
        public string SubContinent { get; set; }
        public string UserDefinedValue { get; set; }
        public int VisitCount { get; set; }
        public int VisitLength { get; set; }
        public string VisitorType { get; set; }
        public int Week { get; set; }
        public int Year { get; set; }
        public string Source { get; set; }
    }
}