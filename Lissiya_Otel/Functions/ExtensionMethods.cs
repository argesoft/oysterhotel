﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GAConnect
{
    static class ExtensionMethods
    {
        public static T ParseEnum<T>(this string token)
        {
            return (T)Enum.Parse(typeof(T), token);
        }
    }
}
