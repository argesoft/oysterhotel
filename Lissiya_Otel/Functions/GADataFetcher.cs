﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Xml;
using System.IO;

namespace GAConnect
{
    public class GADataFetcher
    {
        public string Email { get; set; }
        public string Password { get; set; }
        private string AuthenticationKey { get; set; }
        private const string AuthenticationUrl = "https://www.google.com/accounts/ClientLogin";
        public GADataFetcher(string email, string password)
        {
            this.Email = email;
            this.Password = password;
            try
            {
                Authenticate();
                BaseData.authenticationKey = AuthenticationKey;
            }
            catch 
            {
                throw;
            }
        }

        private void Authenticate()
        {
            string AuthenticationPost = string.Format("accountType=GOOGLE&Email={0}&Passwd={1}&service=analytics&source=xxx-xxx",
                this.Email, this.Password);

            this.AuthenticationKey = null;
            string result = HttpRequests.HttpPostRequest(AuthenticationUrl, AuthenticationPost);
            var tokens = result.Split(new string[] { "\n" }, StringSplitOptions.RemoveEmptyEntries);
            foreach (var item in tokens)
            {
                if (item.StartsWith("Auth="))
                    AuthenticationKey = item;
            }
        }


        static IEnumerable<GAProfile> GetProfiles<T>(XDocument xml)
        where T : GAProfile, new()
        {
            XNamespace dxp = xml.Root.GetNamespaceOfPrefix("dxp");
            XNamespace dns = xml.Root.GetDefaultNamespace();
            IEnumerable<GAProfile> profiles = null;
            profiles = xml.Root.Descendants(dns + "entry").Select(f => new GAProfile
            {
                AccountID = f.Elements(dxp + "property").Where( x => x.Attribute("name").Value=="ga:accountId").First().Attribute("value").Value,
                AccountName = f.Elements(dxp + "property").Where(x => x.Attribute("name").Value == "ga:accountName").First().Attribute("value").Value,
                ProfileID = f.Elements(dxp + "property").Where(x => x.Attribute("name").Value == "ga:profileId").First().Attribute("value").Value,
                WebPropertyID = f.Elements(dxp + "property").Where(x => x.Attribute("name").Value == "ga:webPropertyId").First().Attribute("value").Value,
                Currency = f.Elements(dxp + "property").Where(x => x.Attribute("name").Value == "ga:currency").First().Attribute("value").Value,
                TimeZone = f.Elements(dxp + "property").Where(x => x.Attribute("name").Value == "ga:timezone").First().Attribute("value").Value,
                TableID = f.Element(dxp + "tableId").Value,
                Updated = DateTime.Parse(f.Element(dns + "updated").Value),
                ID = f.Element(dns + "id").Value,
                Title = f.Element(dns + "title").Value
            });
            return profiles;
        }
        public IEnumerable<GAProfile> GetUserProfiles()
        {
            var xml = BaseData.GetProfilesData(Email,Password);
            return GetProfiles<GAProfile>(xml);
        }

        public IEnumerable<GAData> GetAnalytics(string tableID, DateTime from, DateTime to, int max, List<Dimension> dimensions, List<Metric> metrics, Metric sort, SortDirection order)
        {
            IEnumerable<BaseData> data = BaseData.GetBaseData(tableID, dimensions, metrics, from, to, sort, order, max);
            return data.Select(d => new GAData
            {
                Pageviews = Convert.ToInt32(d.Metrics.FirstOrDefault(met => met.Key == Metric.pageviews).Value),
                Bounces = Convert.ToInt32(d.Metrics.FirstOrDefault(met => met.Key == Metric.bounces).Value),
                Entrances = Convert.ToInt32(d.Metrics.FirstOrDefault(met => met.Key == Metric.entrances).Value),
                Exits = Convert.ToInt32(d.Metrics.FirstOrDefault(met => met.Key == Metric.exits).Value),
                NewVisits = Convert.ToInt32(d.Metrics.FirstOrDefault(met => met.Key == Metric.newVisits).Value),
                TimeOnPage = Convert.ToDouble(d.Metrics.FirstOrDefault(met => met.Key == Metric.timeOnPage).Value),
                TimeOnSite = Convert.ToDouble(d.Metrics.FirstOrDefault(met => met.Key == Metric.timeOnSite).Value),
                Visitors = Convert.ToInt32(d.Metrics.FirstOrDefault(met => met.Key == Metric.visitors).Value),
                Visits = Convert.ToInt32(d.Metrics.FirstOrDefault(met => met.Key == Metric.visits).Value),
                UniquePageviews = Convert.ToInt32(d.Metrics.FirstOrDefault(met => met.Key == Metric.uniquePageviews).Value),
                ExitPagePath = d.Dimensions.FirstOrDefault(dim => dim.Key == Dimension.exitPagePath).Value,
                LandingPagePath = d.Dimensions.FirstOrDefault(dim => dim.Key == Dimension.landingPagePath).Value,
                NextPagePath = d.Dimensions.FirstOrDefault(dim => dim.Key == Dimension.nextPagePath).Value,
                PagePath = d.Dimensions.FirstOrDefault(dim => dim.Key == Dimension.pagePath).Value,
                PageTitle = d.Dimensions.FirstOrDefault(dim => dim.Key == Dimension.pageTitle).Value,
                PreviousPagePath = d.Dimensions.FirstOrDefault(dim => dim.Key == Dimension.previousPagePath).Value,
                SecondPagePath = d.Dimensions.FirstOrDefault(dim => dim.Key == Dimension.secondPagePath).Value,
                Browser = d.Dimensions.FirstOrDefault(dim => dim.Key == Dimension.browser).Value,
                BrowserVersion = d.Dimensions.FirstOrDefault(dim => dim.Key == Dimension.browserVersion).Value,
                City = d.Dimensions.FirstOrDefault(dim => dim.Key == Dimension.city).Value,
                ConnectionSpeed = d.Dimensions.FirstOrDefault(dim => dim.Key == Dimension.connectionSpeed).Value,
                Country = d.Dimensions.FirstOrDefault(dim => dim.Key == Dimension.country).Value,
                Date = d.Dimensions.FirstOrDefault(dim => dim.Key == Dimension.date).Value,
                DaysSinceLastVisit = d.Dimensions.FirstOrDefault(dim => dim.Key == Dimension.daysSinceLastVisit).Value,
                Day = d.Dimensions.FirstOrDefault(dim => dim.Key == Dimension.day).Value,
                FlashVersion = d.Dimensions.FirstOrDefault(dim => dim.Key == Dimension.flashVersion).Value,
                Hostname = d.Dimensions.FirstOrDefault(dim => dim.Key == Dimension.hostname).Value,
                IsMobile = d.Dimensions.FirstOrDefault(dim => dim.Key == Dimension.isMobile).Value,
                Hour = d.Dimensions.FirstOrDefault(dim => dim.Key == Dimension.hour).Value,
                JavaEnabled = d.Dimensions.FirstOrDefault(dim => dim.Key == Dimension.javaEnabled).Value,
                Language = d.Dimensions.FirstOrDefault(dim => dim.Key == Dimension.language).Value,
                Latitude = d.Dimensions.FirstOrDefault(dim => dim.Key == Dimension.latitude).Value,
                Longitude = d.Dimensions.FirstOrDefault(dim => dim.Key == Dimension.longitude).Value,
                Month = d.Dimensions.FirstOrDefault(dim => dim.Key == Dimension.month).Value,
                NetworkDomain = d.Dimensions.FirstOrDefault(dim => dim.Key == Dimension.networkDomain).Value,
                NetworkLocation = d.Dimensions.FirstOrDefault(dim => dim.Key == Dimension.networkLocation).Value,
                OperatingSystem = d.Dimensions.FirstOrDefault(dim => dim.Key == Dimension.operatingSystem).Value,
                OperatingSystemVersion = d.Dimensions.FirstOrDefault(dim => dim.Key == Dimension.operatingSystemVersion).Value,
                PageDepth = d.Dimensions.FirstOrDefault(dim => dim.Key == Dimension.pageDepth).Value,
                Region = d.Dimensions.FirstOrDefault(dim => dim.Key == Dimension.region).Value,
                ScreenColors = d.Dimensions.FirstOrDefault(dim => dim.Key == Dimension.screenColors).Value,
                ScreenResolution = d.Dimensions.FirstOrDefault(dim => dim.Key == Dimension.screenResolution).Value,
                SubContinent = d.Dimensions.FirstOrDefault(dim => dim.Key == Dimension.subContinent).Value,
                UserDefinedValue = d.Dimensions.FirstOrDefault(dim => dim.Key == Dimension.userDefinedValue).Value,
                VisitCount = Convert.ToInt32(d.Dimensions.FirstOrDefault(dim => dim.Key == Dimension.visitCount).Value),
                VisitLength = Convert.ToInt32(d.Dimensions.FirstOrDefault(dim => dim.Key == Dimension.visitLength).Value),
                VisitorType = d.Dimensions.FirstOrDefault(dim => dim.Key == Dimension.visitorType).Value,
                Week = Convert.ToInt32(d.Dimensions.FirstOrDefault(dim => dim.Key == Dimension.week).Value),
                Year = Convert.ToInt32(d.Dimensions.FirstOrDefault(dim => dim.Key == Dimension.year).Value),
                Source = d.Dimensions.FirstOrDefault(dim => dim.Key == Dimension.source).Value
            });
        }
    }
    public enum Dimension
    {
        exitPagePath,
        landingPagePath,
        nextPagePath,
        pagePath,
        pageTitle,
        previousPagePath,
        secondPagePath,
        browser,
        browserVersion,
        city,
        connectionSpeed,
        country,
        date,
        daysSinceLastVisit,
        day,
        flashVersion,
        hostname,
        isMobile,
        hour,
        javaEnabled,
        language,
        latitude,
        longitude,
        month,
        networkDomain,
        networkLocation,
        operatingSystem,
        operatingSystemVersion,
        pageDepth,
        region,
        screenColors,
        screenResolution,
        subContinent,
        userDefinedValue,
        visitCount,
        visitLength,
        visitorType,
        week,
        year,
        source,

    }
    public enum Metric
    {
        bounces,
        entrances,
        exits,
        newVisits,
        pageviews,
        timeOnPage,
        timeOnSite,
        visitors,
        visits,
        uniquePageviews
    }

    public enum SortDirection
    {
        Ascending,
        Descending
    }
}
