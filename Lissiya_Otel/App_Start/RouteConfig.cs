﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Lissiya_Otel
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.MapRoute("EnSayfalar", "En/{title}/{PageID}", new { controller = "Home", action = "Index", title = UrlParameter.Optional, PageID = UrlParameter.Optional, lang = "en" }, namespaces: new[] { "Lissiya_Otel.Controllers" });
            routes.MapRoute("trSayfalar", "tr/{title}/{PageID}", new { controller = "Home", action = "Index", title = UrlParameter.Optional, PageID = UrlParameter.Optional, lang = "tr" }, namespaces: new[] { "Lissiya_Otel.Controllers" });
            routes.MapRoute("ruSayfalar", "ru/{title}/{PageID}", new { controller = "Home", action = "Index", title = UrlParameter.Optional, PageID = UrlParameter.Optional, lang = "ru" }, namespaces: new[] { "Lissiya_Otel.Controllers" });
            routes.MapRoute("zhSayfalar", "zh/{title}/{PageID}", new { controller = "Home", action = "Index", title = UrlParameter.Optional, PageID = UrlParameter.Optional, lang = "zh" }, namespaces: new[] { "Lissiya_Otel.Controllers" });
            routes.MapRoute("jpSayfalar", "jp/{title}/{PageID}", new { controller = "Home", action = "Index", title = UrlParameter.Optional, PageID = UrlParameter.Optional, lang = "jp" }, namespaces: new[] { "Lissiya_Otel.Controllers" });

            routes.MapRoute("Home", "Home/{action}/{id}", new { controller = "Home", action = "Index", id = UrlParameter.Optional, lang = "en" }, namespaces: new[] { "Lissiya_Otel.Controllers" });
            // KONAKLAMA EN
            routes.MapRoute("Services", "services/{id}", new { controller = "Konaklama", action = "Index", id = UrlParameter.Optional, lang = "en" }, namespaces: new[] { "Lissiya_Otel.Controllers" });
            routes.MapRoute("ServiceDetail", "service-detail/{name}/{id}", new { controller = "Konaklama", action = "Detay", id = UrlParameter.Optional, lang = "en" }, namespaces: new[] { "Lissiya_Otel.Controllers" });

            // GEZİLECEK ÇEVRE EN
            routes.MapRoute("CulturalAdventures", "cultural-adventures/{id}", new { controller = "GezilecekCevre", action = "Index", id = UrlParameter.Optional, lang = "en" }, namespaces: new[] { "Lissiya_Otel.Controllers" });
            routes.MapRoute("CulturalAdventuresDetail", "cultural-adventures-detail/{name}/{id}", new { controller = "GezilecekCevre", action = "Detay", id = UrlParameter.Optional, lang = "en" }, namespaces: new[] { "Lissiya_Otel.Controllers" });

            // RAHATLAMA EN
            routes.MapRoute("Relief", "relief/{id}", new { controller = "Rahatlama", action = "Index", id = UrlParameter.Optional, lang = "en" }, namespaces: new[] { "Lissiya_Otel.Controllers" });
            routes.MapRoute("ReliefDetail", "relief-detail/{name}/{id}", new { controller = "Rahatlama", action = "Detay", id = UrlParameter.Optional, lang = "en" }, namespaces: new[] { "Lissiya_Otel.Controllers" });

            // RESTAURANT EN
            routes.MapRoute("RestaurantEN", "restauranten/{id}", new { controller = "Restaurant", action = "Index", id = UrlParameter.Optional, lang = "en" }, namespaces: new[] { "Lissiya_Otel.Controllers" });
            routes.MapRoute("RestaurantENDetail", "restauranten-detail/{name}/{id}", new { controller = "Restaurant", action = "Detay", id = UrlParameter.Optional, lang = "en" }, namespaces: new[] { "Lissiya_Otel.Controllers" });

            // YORUMLAR EN
            routes.MapRoute("Comments", "comments/{id}", new { controller = "Yorumlar", action = "Index", id = UrlParameter.Optional, lang = "en" }, namespaces: new[] { "Lissiya_Otel.Controllers" });

            // Galeri EN
            routes.MapRoute("Gallery", "gallery/{id}", new { controller = "Galeri", action = "Index", id = UrlParameter.Optional, lang = "en" }, namespaces: new[] { "Lissiya_Otel.Controllers" });

            // İletişim EN
            routes.MapRoute("Contact", "contact/{id}", new { controller = "Iletisim", action = "Index", id = UrlParameter.Optional, lang = "en" }, namespaces: new[] { "Lissiya_Otel.Controllers" });

            // Rezervasyon EN
            routes.MapRoute("Reservation", "reservation/{id}", new { controller = "Rezervasyon", action = "Index", id = UrlParameter.Optional, lang = "en" }, namespaces: new[] { "Lissiya_Otel.Controllers" });

            // İÇERİK DETAY EN
            routes.MapRoute("ContentDetail", "content-detail/{name}/{id}", new { controller = "IcerikDetay", action = "Index", id = UrlParameter.Optional, lang = "en" }, namespaces: new[] { "Lissiya_Otel.Controllers" });

            // NEWS
            routes.MapRoute("News", "news/{id}", new { controller = "Haberler", action = "Index", id = UrlParameter.Optional, lang = "en" }, namespaces: new[] { "Lissiya_Otel.Controllers" });

            routes.MapRoute("AnaSayfa", "ana-sayfa/{action}/{id}", new { controller = "Home", action = "Index", id = UrlParameter.Optional, }, namespaces: new[] { "Lissiya_Otel.Controllers" });
            // KONAKLAMA
            routes.MapRoute("Servisler", "servisler/{id}", new { controller = "Konaklama", action = "Index", id = UrlParameter.Optional }, namespaces: new[] { "Lissiya_Otel.Controllers" });
            routes.MapRoute("ServisDetay", "servis-detay/{name}/{id}", new { controller = "Konaklama", action = "Detay", id = UrlParameter.Optional }, namespaces: new[] { "Lissiya_Otel.Controllers" });

            // RAHATLAMA
            routes.MapRoute("Rahatlama", "rahatlama/{id}", new { controller = "Rahatlama", action = "Index", id = UrlParameter.Optional }, namespaces: new[] { "Lissiya_Otel.Controllers" });
            routes.MapRoute("RahatlamaDetay", "rahatlama-detay/{name}/{id}", new { controller = "Rahatlama", action = "Detay", id = UrlParameter.Optional }, namespaces: new[] { "Lissiya_Otel.Controllers" });

            // RESTAURANT
            routes.MapRoute("Restaurant", "restaurant/{id}", new { controller = "Restaurant", action = "Index", id = UrlParameter.Optional }, namespaces: new[] { "Lissiya_Otel.Controllers" });
            routes.MapRoute("RestaurantDetay", "restaurant-detay/{name}/{id}", new { controller = "Restaurant", action = "Detay", id = UrlParameter.Optional }, namespaces: new[] { "Lissiya_Otel.Controllers" });

            // YORUMLAR
            routes.MapRoute("Yorumlar", "yorumlar/{id}", new { controller = "Yorumlar", action = "Index", id = UrlParameter.Optional }, namespaces: new[] { "Lissiya_Otel.Controllers" });

            // GALERİ
            routes.MapRoute("Galeri", "galeri/{id}", new { controller = "Galeri", action = "Index", id = UrlParameter.Optional }, namespaces: new[] { "Lissiya_Otel.Controllers" });

            // GALERİ
            routes.MapRoute("Iletisim", "iletisim/{id}", new { controller = "Iletisim", action = "Index", id = UrlParameter.Optional }, namespaces: new[] { "Lissiya_Otel.Controllers" });

            // REZERVASYON
            routes.MapRoute("Rezervasyon", "rezervasyon/{id}", new { controller = "Rezervasyon", action = "Index", id = UrlParameter.Optional }, namespaces: new[] { "Lissiya_Otel.Controllers" });

            // İÇERİK DETAY
            routes.MapRoute("IcerikDetay", "icerik-detay/{name}/{id}", new { controller = "IcerikDetay", action = "Index", id = UrlParameter.Optional }, namespaces: new[] { "Lissiya_Otel.Controllers" });

            // HABERLER
            routes.MapRoute("Haberler", "haberler/{id}", new { controller = "Haberler", action = "Index", id = UrlParameter.Optional }, namespaces: new[] { "Lissiya_Otel.Controllers" });

            // GEZİLECEK ÇEVRE
            routes.MapRoute("GezilecekCevre", "gezilecek-cevre/{id}", new { controller = "GezilecekCevre", action = "Index", id = UrlParameter.Optional }, namespaces: new[] { "Lissiya_Otel.Controllers" });
            routes.MapRoute("GezilecekCevreDetay", "gezilecek-cevre-detay/{name}/{id}", new { controller = "GezilecekCevre", action = "Detay", id = UrlParameter.Optional }, namespaces: new[] { "Lissiya_Otel.Controllers" });

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional, lang = "en" },
                namespaces: new[] { "Lissiya_Otel.Controllers" }
            );
        }
    }
}