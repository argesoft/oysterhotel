﻿using Lissiya_Otel.Functions;
using Lissiya_Otel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Lissiya_Otel.Areas.Admin.Controllers
{
    public class NewsController : Controller
    {
        //
        // GET: /Admin/News/

        lissiyaEntities db = new lissiyaEntities();

        public ActionResult NewsNames()
        {
            var NewsNameList = (from x in db.NewsNames orderby x.OrderNumber ascending select x).ToList();

            ViewBag.LanguageList = LanguageController.Languages();

            return View(NewsNameList);
        }

        public ActionResult CreateNewsNames()
        {
            return View();
        }

        public ActionResult CreateNewsLanguage()
        {
            var LangaugeList = LanguageController.Languages();
            if (LangaugeList.Count > 0)
            {
                ViewBag.LanguageList = LangaugeList;
            }
            return View();
        }

        [HttpPost]
        public ActionResult CreateNewsNames(NewsNames bln)
        {
            try
            {
                var row = new NewsNames
                {
                    Status = 1,
                    Name = bln.Name,
                    OrderNumber = 0,
                    InsertedDate = DateTime.Now
                };
                db.NewsNames.Add(row);
                db.SaveChanges();
                TempData["Success"] = GeneralFunctions.ReturnSuccess(1);

                return RedirectToAction("CreateNewsLanguage", "News", new { id = row.NewsId });
            }
            catch (Exception)
            {
                TempData["Error"] = GeneralFunctions.ReturnError(1);
                return View();
            }

        }

        [HttpPost, ValidateInput(false)]
        public ActionResult CreateNewsLanguage(NewsLanguages blg, string updatecontrol, HttpPostedFileBase file)
        {
            var LangaugeList = LanguageController.Languages();
            if (LangaugeList.Count > 0)
            {
                ViewBag.LanguageList = LangaugeList;
            }
            try
            {
                if (blg.LanguageId != null)
                {
                    var Detail = (from x in db.NewsLanguages where x.NewsId == blg.NewsId && x.LanguageId == blg.LanguageId select x).SingleOrDefault();

                    if (Detail == null)
                    {
                        string image = string.Empty;
                        if (file != null)
                        {
                            var extension = file.FileName.Substring(file.FileName.LastIndexOf('.'));
                            image = Guid.NewGuid().ToString() + extension;
                            var imageToResize = System.Drawing.Image.FromStream(file.InputStream);
                            ImageSave.InsertImage(imageToResize, image);
                        }
                        var row = new NewsLanguages
                        {
                            InsertedDate = DateTime.Now,
                            LanguageId = blg.LanguageId,
                            OrderNumber = 0,
                            Title = blg.Title,
                            NewsId = blg.NewsId,
                            Description = blg.Description,
                            ImageName = image,
                            NewsDate = blg.NewsDate,
                            SeoDescription = blg.SeoDescription,
                            SeoKeywords = blg.SeoKeywords,
                            SeoTitle = blg.SeoTitle,
                            Status = 1,
                            Summary = blg.Summary


                        };

                        db.NewsLanguages.Add(row);
                        db.SaveChanges();
                        db.Dispose();
                        TempData["Success"] = GeneralFunctions.ReturnSuccess(1);
                        if (!string.IsNullOrEmpty(updatecontrol))
                        {
                            return RedirectToAction("UpdateNewsLanguage", "News", new { id = row.NewsId, LangId = row.LanguageId });
                        }
                        else
                        {
                            return RedirectToAction("CreateNewsLanguage", "News", new { id = row.NewsId });
                        }
                        
                    }
                    else
                    {
                        TempData["Info"] = GeneralFunctions.ReturnInfo(1);
                        if (!string.IsNullOrEmpty(updatecontrol))
                        {
                            return RedirectToAction("UpdateNewsLanguage", "News", new { id = blg.NewsId, LangId = blg.LanguageId });
                        }
                        else
                        {
                            return RedirectToAction("CreateNewsLanguage", "News", new { id = blg.NewsId });
                        }
                    }

                  

                }
                else
                {
                    TempData["Error"] = GeneralFunctions.ReturnError(4);
                    if (!string.IsNullOrEmpty(updatecontrol))
                    {
                        return RedirectToAction("UpdateNewsLanguage", "News", new { id = blg.NewsId, LangId = blg.LanguageId });
                    }
                    else
                    {
                        return RedirectToAction("CreateNewsLanguage", "News", new { id = blg.NewsId });
                    }

                }

            }
            catch (Exception)
            {
                TempData["Error"] = GeneralFunctions.ReturnError(1);
                if (!string.IsNullOrEmpty(updatecontrol))
                {
                    return RedirectToAction("UpdateNewsLanguage", "News", new { id = blg.NewsId, LangId = blg.LanguageId });
                }
                else
                {
                    return RedirectToAction("CreateNewsLanguage", "News", new { id = blg.NewsId });
                }

            }
        }

        public ActionResult UpdateNewsLanguage(int id)
        {
            int LanguageId = int.Parse(Request.QueryString["LangId"]);
            var Detail = (from x in db.NewsLanguages where x.NewsId == id && x.LanguageId == LanguageId select x).SingleOrDefault();
            if (Detail != null)
            {
                ViewBag.Detail = Detail;
                return View();
            }
            else
            {
                return View();
            }

        }

        [HttpPost, ValidateInput(false)]
        public ActionResult UpdateNewsLanguage(int id, NewsLanguages blg, HttpPostedFileBase file)
        {
            int LanguageId = int.Parse(Request.QueryString["LangId"]);
            var Detail = (from x in db.NewsLanguages where x.NewsId == id && x.LanguageId == LanguageId select x).SingleOrDefault();
            try
            {
                string logo = string.Empty;
                if (file != null)
                {
                    var extension = file.FileName.Substring(file.FileName.LastIndexOf('.'));
                    logo = Guid.NewGuid().ToString() + extension;
                    var imageToResize = System.Drawing.Image.FromStream(file.InputStream);
                    ImageSave.InsertImage(imageToResize, logo);
                    Detail.ImageName = logo;
                }

                Detail.NewsDate = blg.NewsDate;
                Detail.Description = blg.Description;
                Detail.SeoDescription = blg.SeoDescription;
                Detail.SeoKeywords = blg.SeoKeywords;
                Detail.SeoTitle = blg.SeoTitle;
                Detail.Summary = blg.Summary;
                Detail.Title = blg.Title;

                db.SaveChanges();
                db.Dispose();
                TempData["Success"] = GeneralFunctions.ReturnSuccess(2);
                ViewBag.Detail = Detail;
                return View();
            }
            catch (Exception)
            {
                TempData["Error"] = GeneralFunctions.ReturnError(2);
                ViewBag.Detail = Detail;
                return View();
            }

        }

        public ActionResult UpdateNewsNames(int id)
        {
            var Detail = (from x in db.NewsNames where x.NewsId == id select x).SingleOrDefault();

            return View(Detail);
        }

        [HttpPost]
        public ActionResult UpdateNewsNames(int id, NewsNames adn)
        {
            var Detail = (from x in db.NewsNames where x.NewsId == id select x).SingleOrDefault();
            try
            {
                Detail.Name = adn.Name;
                db.SaveChanges();
                db.Dispose();
                TempData["Success"] = GeneralFunctions.ReturnSuccess(2);
                return View(Detail);
            }
            catch (Exception)
            {
                TempData["Error"] = GeneralFunctions.ReturnError(2);
                return View(Detail);
            }


        }

        public ActionResult Delete(int id)
        {
            var detail = (from x in db.NewsNames where x.NewsId == id select x).FirstOrDefault();
            var NewsLanguagesList = (from x in db.NewsLanguages where x.NewsId == id select x).ToList();

            if (detail != null)
            {
                db.NewsNames.Remove(detail);

                foreach (var item in NewsLanguagesList)
                {
                    var imagename = item.ImageName;
                    if (System.IO.File.Exists(Server.MapPath("/media/images/" + imagename)))
                    {
                        System.IO.File.Delete(Server.MapPath("/media/images/" + imagename));
                    }

                    if (System.IO.File.Exists(Server.MapPath("/media/images/thumbnails" + imagename)))
                    {
                        System.IO.File.Delete(Server.MapPath("/media/images/thumbnails" + imagename));
                    }

                    if (System.IO.File.Exists(Server.MapPath("/media/images/thumbnails40" + imagename)))
                    {
                        System.IO.File.Delete(Server.MapPath("/media/images/thumbnails40" + imagename));
                    }
                    db.NewsLanguages.Remove(item);
                }

                db.SaveChanges();
            }
            db.Dispose();
            return RedirectToAction("NewsNames", "News");
        }

        public ActionResult DeleteLanguage(int id, int LangId)
        {
            var Detail = (from x in db.NewsLanguages where x.NewsId == id && x.LanguageId == LangId select x).SingleOrDefault();

            if (Detail != null)
            {
                db.NewsLanguages.Remove(Detail);
                db.SaveChanges();
                db.Dispose();
            }

            return RedirectToAction("CreateNewsLanguage", "News", new { id = id });
        }

        public JsonResult Sort(List<string> Ids)
        {

            try
            {
                for (int i = 1; i < Ids.Count + 1; i++)
                {
                    var id = int.Parse(Ids[i - 1]);
                    var News = (from x in db.NewsNames where x.NewsId == id select x).SingleOrDefault();
                    News.OrderNumber = i;
                    db.SaveChanges();
                }
                db.Dispose();
                return Json(new { Code = 200 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { Code = 500 }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult ChangeStatus(ChangeStatus1 chs)
        {
            try
            {
                var Detail = (from x in db.NewsNames where x.NewsId == chs.id select x).FirstOrDefault();

                if (chs.Status == 2)
                {
                    Detail.Status = 2;
                }
                else
                {
                    Detail.Status = 1;
                }

                db.SaveChanges();
                db.Dispose();

                return Json(new { Ok = "OK" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { Ok = "BAD" }, JsonRequestBehavior.DenyGet);
            }


        }

        [Serializable]
        public class ChangeStatus1
        {
            public int id { get; set; }
            public int Status { get; set; }
        }

    }
}
