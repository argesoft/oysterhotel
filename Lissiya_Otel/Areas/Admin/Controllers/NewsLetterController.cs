﻿using Lissiya_Otel.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Lissiya_Otel.Areas.Admin.Controllers
{
    public class NewsLetterController : Controller
    {
        //
        // GET: /Admin/NewsLetter/

        lissiyaEntities db = new lissiyaEntities();

        public ActionResult NewsLetters()
        {
            var NewsLetterList = (from x in db.NewsLetter orderby x.Email ascending select x).ToList();

            return View(NewsLetterList);
        }

        public ActionResult Delete(int id)
        {
            var detail = (from x in db.NewsLetter where x.Id == id select x).FirstOrDefault();

            if (detail != null)
            {
                db.NewsLetter.Remove(detail);
                db.SaveChanges();
            }
            db.Dispose();
            return RedirectToAction("NewsLetters", "NewsLetter");
        }

        public ActionResult ExportData()
        {
            GridView gv = new GridView();

            DataTable dt = new DataTable();
            dt.Columns.Add("E-Posta");

            var NewsLetters = (from x in db.NewsLetter orderby x.InsertedDate ascending select x).ToList();

            for (int i = 0; i < NewsLetters.Count; i++)
            {
                string EMail = NewsLetters[i].Email;


                dt.Rows.Add(EMail);

            }

            gv.DataSource = dt;
            gv.DataBind();
            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment; filename=NewsLetters.xls");
            Response.ContentType = "application/ms-excel";
            Response.ContentEncoding = Encoding.GetEncoding("ISO-8859-9");
            Response.Charset = "ISO-8859-9";
            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            gv.RenderControl(htw);
            Response.Output.Write(sw.ToString());
            Response.Flush();
            Response.End();

            return RedirectToAction("NewsLetters");
        }
    }
}
