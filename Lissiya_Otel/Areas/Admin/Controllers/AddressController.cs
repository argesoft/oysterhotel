﻿using Lissiya_Otel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Lissiya_Otel.Functions;
using System.IO;

namespace Lissiya_Otel.Areas.Admin.Controllers
{
    public class AddressController : Controller
    {
        //
        // GET: /Admin/Address/
        lissiyaEntities db = new lissiyaEntities();

        public ActionResult AddressNames()
        {
            var AddressNameList = (from x in db.AddressNames orderby x.OrderNumber ascending select x).ToList();

            ViewBag.LanguageList = LanguageController.Languages();

            return View(AddressNameList);
        }

        public ActionResult CreateAddressNames()
        {

            return View();
        }

        public ActionResult CreateAddressLanguage()
        {
            var LangaugeList = LanguageController.Languages();
            if (LangaugeList.Count > 0)
            {
                ViewBag.LanguageList = LangaugeList;
            }
            return View();
        }

        [HttpPost]
        public ActionResult CreateAddressNames(AddressNames adn)
        {
            try
            {
                var row = new AddressNames
                {
                    Status = 1,
                    Name = adn.Name,
                    OrderNumber = 0,
                    InsertedDate = DateTime.Now
                };
                db.AddressNames.Add(row);
                db.SaveChanges();
                TempData["Success"] = GeneralFunctions.ReturnSuccess(1);

                return RedirectToAction("CreateAddressLanguage", "Address", new { id = row.AddressId });
            }
            catch (Exception)
            {
                TempData["Error"] = GeneralFunctions.ReturnError(1);
                return View();
            }

        }

        [HttpPost]
        public ActionResult CreateAddressLanguage(AddressLanguages adl, string updatecontrol, HttpPostedFileBase file)
        {
            var LangaugeList = LanguageController.Languages();
            if (LangaugeList.Count > 0)
            {
                ViewBag.LanguageList = LangaugeList;
            }
            try
            {

                if (adl.LanguageId != null)
                {
                    var Detail = (from x in db.AddressLanguages where x.AddressId == adl.AddressId && x.LanguageId == adl.LanguageId select x).SingleOrDefault();

                    if (Detail == null)
                    {
                        string logo = string.Empty;
                        if (file != null)
                        {
                            var extension = file.FileName.Substring(file.FileName.LastIndexOf('.'));
                            logo = Guid.NewGuid().ToString() + extension;
                            var imageToResize = System.Drawing.Image.FromStream(file.InputStream);
                            ImageSave.InsertImage(imageToResize, logo);
                        }
                        var row = new AddressLanguages
                        {
                            Address = adl.Address,
                            AddressId = adl.AddressId,
                            Email = adl.Email,
                            Fax = adl.Fax,
                            InsertedDate = DateTime.Now,
                            LanguageId = adl.LanguageId,
                            Latitude = adl.Latitude,
                            Logo = logo,
                            Longtitude = adl.Longtitude,
                            OrderNumber = 0,
                            Phone1 = adl.Phone1,
                            Phone2 = adl.Phone2,
                            Status = 1,
                            Title = adl.Title
                        };

                        db.AddressLanguages.Add(row);
                        db.SaveChanges();
                        db.Dispose();
                        TempData["Success"] = GeneralFunctions.ReturnSuccess(1);
                        if (!string.IsNullOrEmpty(updatecontrol))
                        {
                            return RedirectToAction("UpdateAddressLanguage", "Address", new { id = row.AddressId, LangId = row.LanguageId });
                        }
                        else
                        {
                            return RedirectToAction("CreateAddressLanguage", "Address", new { id = row.AddressId });
                        }

                    }
                    else
                    {
                        TempData["Info"] = GeneralFunctions.ReturnInfo(1);
                        if (!string.IsNullOrEmpty(updatecontrol))
                        {
                            return RedirectToAction("UpdateAddressLanguage", "Address", new { id = adl.AddressId, LangId = adl.LanguageId });
                        }
                        else
                        {
                            return RedirectToAction("CreateAddressLanguage", "Address", new { id = adl.AddressId });
                        }
                    }
                }
                else
                {
                    TempData["Error"] = GeneralFunctions.ReturnError(4);
                    if (!string.IsNullOrEmpty(updatecontrol))
                    {
                        return RedirectToAction("UpdateAddressLanguage", "Address", new { id = adl.AddressId, LangId = adl.LanguageId });
                    }
                    else
                    {
                        return RedirectToAction("CreateAddressLanguage", "Address", new { id = adl.AddressId });
                    }
                }

            }
            catch (Exception)
            {
                TempData["Error"] = GeneralFunctions.ReturnError(1);
                if (!string.IsNullOrEmpty(updatecontrol))
                {
                    return RedirectToAction("UpdateAddressLanguage", "Address", new { id = adl.AddressId, LangId = adl.LanguageId });
                }
                else
                {
                    return RedirectToAction("CreateAddressLanguage", "Address", new { id = adl.AddressId });
                }

            }
        }

        public ActionResult UpdateAddressLanguage(int id)
        {
            int LanguageId = int.Parse(Request.QueryString["LangId"]);
            var Detail = (from x in db.AddressLanguages where x.AddressId == id && x.LanguageId == LanguageId select x).SingleOrDefault();
            if (Detail != null)
            {
                ViewBag.Detail = Detail;
                return View();
            }
            else
            {
                return View();
            }

        }

        [HttpPost]
        public ActionResult UpdateAddressLanguage(int id, AddressLanguages adl, HttpPostedFileBase file)
        {
            int LanguageId = int.Parse(Request.QueryString["LangId"]);
            var Detail = (from x in db.AddressLanguages where x.AddressId == id && x.LanguageId == LanguageId select x).SingleOrDefault();
            try
            {
                string logo = string.Empty;
                if (file != null)
                {
                    var extension = file.FileName.Substring(file.FileName.LastIndexOf('.'));
                    logo = Guid.NewGuid().ToString() + extension;
                    var imageToResize = System.Drawing.Image.FromStream(file.InputStream);
                    ImageSave.InsertImage(imageToResize, logo);
                    Detail.Logo = logo;
                }

                Detail.Address = adl.Address;
                Detail.Email = adl.Email;
                Detail.Fax = adl.Fax;
                Detail.Latitude = adl.Latitude;

                Detail.Longtitude = adl.Longtitude;
                Detail.Phone1 = adl.Phone1;
                Detail.Phone2 = adl.Phone2;
                Detail.Title = adl.Title;

                db.SaveChanges();
                db.Dispose();
                TempData["Success"] = GeneralFunctions.ReturnSuccess(2);
                ViewBag.Detail = Detail;
                return View();
            }
            catch (Exception)
            {
                TempData["Error"] = GeneralFunctions.ReturnError(2);
                ViewBag.Detail = Detail;
                return View();
            }

        }

        public ActionResult UpdateAddressNames(int id)
        {
            var Detail = (from x in db.AddressNames where x.AddressId == id select x).SingleOrDefault();

            return View(Detail);
        }

        [HttpPost]
        public ActionResult UpdateAddressNames(int id, AddressNames adn)
        {
            var Detail = (from x in db.AddressNames where x.AddressId == id select x).SingleOrDefault();
            try
            {

                Detail.Name = adn.Name;

                db.SaveChanges();
                db.Dispose();
                TempData["Success"] = GeneralFunctions.ReturnSuccess(2);
                return View(Detail);
            }
            catch (Exception)
            {
                TempData["Error"] = GeneralFunctions.ReturnError(2);
                return View(Detail);
            }


        }

        public ActionResult Delete(int id)
        {
            var detail = (from x in db.AddressNames where x.AddressId == id select x).FirstOrDefault();
            var AddressLanguagesList = (from x in db.AddressLanguages where x.AddressId == id select x).ToList();

            if (detail != null)
            {
                db.AddressNames.Remove(detail);

                foreach (var item in AddressLanguagesList)
                {
                    var imagename = item.Logo;
                    if (System.IO.File.Exists(Server.MapPath("/media/images/" + imagename)))
                    {
                        System.IO.File.Delete(Server.MapPath("/media/images/" + imagename));
                    }

                    if (System.IO.File.Exists(Server.MapPath("/media/images/thumbnails"+imagename)))
                    {
                        System.IO.File.Delete(Server.MapPath("/media/images/thumbnails" + imagename));
                    }

                    if (System.IO.File.Exists(Server.MapPath("/media/images/thumbnails40"+imagename)))
                    {
                        System.IO.File.Delete(Server.MapPath("/media/images/thumbnails40" + imagename));
                    }

                    db.AddressLanguages.Remove(item);

                }

                db.SaveChanges();
            }
            db.Dispose();
            return RedirectToAction("AddressNames", "Address");
        }

        public ActionResult DeleteLanguage(int id, int LangId)
        {
            var Detail = (from x in db.AddressLanguages where x.AddressId == id && x.LanguageId == LangId select x).SingleOrDefault();

            if (Detail != null)
            {
                db.AddressLanguages.Remove(Detail);
                db.SaveChanges();
                db.Dispose();
            }

            return RedirectToAction("CreateAddressLanguage", "Address", new { id = id });
        }

        public JsonResult Sort(List<string> Ids)
        {

            try
            {
                for (int i = 1; i < Ids.Count + 1; i++)
                {
                    var id = int.Parse(Ids[i - 1]);
                    var Address = (from x in db.AddressNames where x.AddressId == id select x).SingleOrDefault();
                    Address.OrderNumber = i;
                    db.SaveChanges();
                }
                db.Dispose();
                return Json(new { Code = 200 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { Code = 500 }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult ChangeStatus(ChangeStatus1 chs)
        {
            try
            {
                var Detail = (from x in db.AddressNames where x.AddressId == chs.id select x).SingleOrDefault();

                if (chs.Status == 2)
                {
                    Detail.Status = 2;
                }
                else
                {
                    Detail.Status = 1;
                }

                db.SaveChanges();
                db.Dispose();

                return Json(new { Ok = "OK" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { Ok = "BAD" }, JsonRequestBehavior.DenyGet);
            }


        }

        [Serializable]
        public class ChangeStatus1
        {
            public int id { get; set; }
            public int Status { get; set; }
        }

    }
}
