﻿using Lissiya_Otel.Functions;
using Lissiya_Otel.Models;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Lissiya_Otel.Areas.Admin.Controllers
{
    public class ProjectController : Controller
    {
        lissiyaEntities db = new lissiyaEntities();

        public ActionResult ProjectNames()
        {
            var ProjectNameList = (from x in db.ProjectNames orderby x.OrderNumber ascending select x).ToList();

            ViewBag.LanguageList = LanguageController.Languages();

            return View(ProjectNameList);
        }

        public ActionResult ProjectCategories()
        {
            var ProjectCategoryList = (from x in db.ProjectCategoryNames orderby x.OrderNumber ascending select x).ToList();

            ViewBag.LanguageList = LanguageController.Languages();

            return View(ProjectCategoryList);
        }

        public ActionResult CreateProjectNames()
        {
            var ProjectCategoryList = (from x in db.ProjectCategoryNames orderby x.Name ascending select x).ToList();

            ViewBag.ProjectCategoryList = ProjectCategoryList;

            return View();
        }

        public ActionResult CreateProjectLanguage()
        {
            var LangaugeList = LanguageController.Languages();
            if (LangaugeList.Count > 0)
            {
                ViewBag.LanguageList = LangaugeList;
            }
            return View();
        }

        public ActionResult CreateProjectCategory()
        {
            var CategoryList = (from x in db.ProjectCategoryNames orderby x.Name ascending select x).ToList();

            ViewBag.CategoryList = CategoryList;

            return View();
        }

        public ActionResult CreateProjectCategoryLanguage()
        {
            var LangaugeList = LanguageController.Languages();
            if (LangaugeList.Count > 0)
            {
                ViewBag.LanguageList = LangaugeList;
            }
            return View();
        }

        [HttpPost]
        public ActionResult CreateProjectNames(ProjectNames cnn)
        {
            try
            {
                var row = new ProjectNames
                {
                    Status = 1,
                    Name = cnn.Name,
                    OrderNumber = 0,
                    ProjectCategoryId = cnn.ProjectCategoryId,
                    InsertedDate = DateTime.Now
                };
                db.ProjectNames.Add(row);
                db.SaveChanges();
                TempData["Success"] = GeneralFunctions.ReturnSuccess(1);

                return RedirectToAction("CreateProjectLanguage", "Project", new { id = row.ProjectId });
            }
            catch (Exception)
            {
                TempData["Error"] = GeneralFunctions.ReturnError(1);
                return View();
            }

        }

        [HttpPost]
        public ActionResult CreateProjectCategory(ProjectCategoryNames rcn)
        {
            var CategoryList = (from x in db.ProjectCategoryNames orderby x.Name ascending select x).ToList();

            ViewBag.CategoryList = CategoryList;
            try
            {
                var row = new ProjectCategoryNames
                {
                    InsertedDate = DateTime.Now,
                    Name = rcn.Name,
                    Status = 1,
                    OrderNumber = 0,
                    TopCategoryId = rcn.TopCategoryId

                };

                db.ProjectCategoryNames.Add(row);
                db.SaveChanges();

                TempData["Success"] = GeneralFunctions.ReturnSuccess(1);
                return RedirectToAction("CreateProjectCategoryLanguage", "Project", new { id = row.ProjectCategoryId });
            }
            catch (Exception)
            {
                TempData["Error"] = GeneralFunctions.ReturnError(1);
                return View();
            }
        }

        [HttpPost]
        public ActionResult CreateProjectCategoryLanguage(ProjectCategoryLanguages rcl, string updatecontrol)
        {
            var LangaugeList = LanguageController.Languages();
            if (LangaugeList.Count > 0)
            {
                ViewBag.LanguageList = LangaugeList;
            }
            try
            {
                if (rcl.LanguageId != null)
                {
                    var Detail = (from x in db.ProjectCategoryLanguages where x.ProjectCategoryId == rcl.ProjectCategoryId && x.LanguageId == rcl.LanguageId select x).SingleOrDefault();

                    if (Detail == null)
                    {
                        var row = new ProjectCategoryLanguages
                        {
                            InsertedDate = DateTime.Now,
                            LanguageId = rcl.LanguageId,
                            OrderNumber = 0,
                            ProjectCategoryId = rcl.ProjectCategoryId,
                            Status = 1,
                            Title = rcl.Title
                            
                        };

                        db.ProjectCategoryLanguages.Add(row);
                        db.SaveChanges();
                        db.Dispose();
                        TempData["Success"] = GeneralFunctions.ReturnSuccess(1);
                        if (!string.IsNullOrEmpty(updatecontrol))
                        {
                            return RedirectToAction("UpdateProjectCategoryLanguage", "Project", new { id = row.ProjectCategoryId, LangId = row.LanguageId });
                        }
                        else
                        {
                            return RedirectToAction("CreateProjectCategoryLanguage", "Project", new { id = row.ProjectCategoryId });
                        }
                    }
                    else
                    {
                        TempData["Info"] = GeneralFunctions.ReturnInfo(1);
                        if (!string.IsNullOrEmpty(updatecontrol))
                        {
                            return RedirectToAction("UpdateProjectCategoryLanguage", "Project", new { id = rcl.ProjectCategoryId, LangId = rcl.LanguageId });
                        }
                        else
                        {
                            return RedirectToAction("CreateProjectCategoryLanguage", "Project", new { id = rcl.ProjectCategoryId });
                        }
                    }


                }
                else
                {
                    TempData["Error"] = GeneralFunctions.ReturnError(4);
                    if (!string.IsNullOrEmpty(updatecontrol))
                    {
                        return RedirectToAction("UpdateProjectCategoryLanguage", "Project", new { id = rcl.ProjectCategoryId, LangId = rcl.LanguageId });
                    }
                    else
                    {
                        return RedirectToAction("CreateProjectCategoryLanguage", "Project", new { id = rcl.ProjectCategoryId });
                    }
                }
            }
            catch (Exception)
            {
                TempData["Error"] = GeneralFunctions.ReturnError(1);
                if (!string.IsNullOrEmpty(updatecontrol))
                {
                    return RedirectToAction("UpdateProjectCategoryLanguage", "Project", new { id = rcl.ProjectCategoryId, LangId = rcl.LanguageId });
                }
                else
                {
                    return RedirectToAction("CreateProjectCategoryLanguage", "Project", new { id = rcl.ProjectCategoryId });
                }
            }
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult CreateProjectLanguage(ProjectLanguages clg, string updatecontrol, HttpPostedFileBase file)
        {
            var LangaugeList = LanguageController.Languages();
            if (LangaugeList.Count > 0)
            {
                ViewBag.LanguageList = LangaugeList;
            }
            try
            {
                if (clg.LanguageId != null)
                {
                    var Detail = (from x in db.ProjectLanguages where x.ProjectId == clg.ProjectId && x.LanguageId == clg.LanguageId select x).SingleOrDefault();

                    if (Detail == null)
                    {
                        string image = string.Empty;
                        if (file != null)
                        {
                            var extension = file.FileName.Substring(file.FileName.LastIndexOf('.'));
                            image = Guid.NewGuid().ToString() + extension;
                            var imageToResize = System.Drawing.Image.FromStream(file.InputStream);
                            ImageSave.InsertImage(imageToResize, image);
                        }

                        var row = new ProjectLanguages
                        {
                            InsertedDate = DateTime.Now,
                            LanguageId = clg.LanguageId,
                            OrderNumber = 0,
                            Title = clg.Title,
                            Description = clg.Description,
                            SeoDescription = clg.SeoDescription,
                            SeoKeywords = clg.SeoKeywords,
                            SeoTitle = clg.SeoTitle,
                            Status = 1,
                            ProjectId = clg.ProjectId,
                            Summary = clg.Summary,
                            CustomerName = clg.CustomerName,
                            Date = clg.Date,
                            Resim = image
                           
                        };

                        db.ProjectLanguages.Add(row);
                        db.SaveChanges();
                        db.Dispose();
                        TempData["Success"] = GeneralFunctions.ReturnSuccess(1);
                        if (!string.IsNullOrEmpty(updatecontrol))
                        {
                            return RedirectToAction("UpdateProjectLanguage", "Project", new { id = row.ProjectId, LangId = row.LanguageId });
                        }
                        else
                        {
                            return RedirectToAction("CreateProjectLanguage", "Project", new { id = row.ProjectId });
                        }
                    }
                    else
                    {
                        TempData["Info"] = GeneralFunctions.ReturnInfo(1);
                        if (!string.IsNullOrEmpty(updatecontrol))
                        {
                            return RedirectToAction("UpdateProjectLanguage", "Project", new { id = clg.ProjectId, LangId = clg.LanguageId });
                        }
                        else
                        {
                            return RedirectToAction("CreateProjectLanguage", "Project", new { id = clg.ProjectId });
                        }
                    }



                }
                else
                {
                    TempData["Error"] = GeneralFunctions.ReturnError(4);
                    if (!string.IsNullOrEmpty(updatecontrol))
                    {
                        return RedirectToAction("UpdateProjectLanguage", "Project", new { id = clg.ProjectId, LangId = clg.LanguageId });
                    }
                    else
                    {
                        return RedirectToAction("CreateProjectLanguage", "Project", new { id = clg.ProjectId });
                    }

                }

            }
            catch (Exception)
            {
                TempData["Error"] = GeneralFunctions.ReturnError(1);
                if (!string.IsNullOrEmpty(updatecontrol))
                {
                    return RedirectToAction("UpdateProjectLanguage", "Project", new { id = clg.ProjectId, LangId = clg.LanguageId });
                }
                else
                {
                    return RedirectToAction("CreateProjectLanguage", "Project", new { id = clg.ProjectId });
                }

            }
        }

        public ActionResult UpdateProjectLanguage(int id)
        {
            int LanguageId = int.Parse(Request.QueryString["LangId"]);
            var Detail = (from x in db.ProjectLanguages where x.ProjectId == id && x.LanguageId == LanguageId select x).SingleOrDefault();
            if (Detail != null)
            {
                ViewBag.Detail = Detail;
                return View();
            }
            else
            {
                return View();
            }

        }

        [HttpPost, ValidateInput(false)]
        public ActionResult UpdateProjectLanguage(int id, ProjectLanguages blg, HttpPostedFileBase file)
        {
            int LanguageId = int.Parse(Request.QueryString["LangId"]);
            var Detail = (from x in db.ProjectLanguages where x.ProjectId == id && x.LanguageId == LanguageId select x).SingleOrDefault();
            try
            {
                string logo = string.Empty;
                if (file != null)
                {
                    var extension = file.FileName.Substring(file.FileName.LastIndexOf('.'));
                    logo = Guid.NewGuid().ToString() + extension;
                    var imageToResize = System.Drawing.Image.FromStream(file.InputStream);
                    ImageSave.InsertImage(imageToResize, logo);
                    Detail.Resim = logo;
                }

                Detail.Description = blg.Description;
                Detail.SeoDescription = blg.SeoDescription;
                Detail.SeoKeywords = blg.SeoKeywords;
                Detail.SeoTitle = blg.SeoTitle;
                Detail.Summary = blg.Summary;
                Detail.Title = blg.Title;
                Detail.CustomerName = blg.CustomerName;
                Detail.Date = blg.Date;

                db.SaveChanges();
                db.Dispose();
                TempData["Success"] = GeneralFunctions.ReturnSuccess(2);
                ViewBag.Detail = Detail;
                return View();
            }
            catch (Exception)
            {
                TempData["Error"] = GeneralFunctions.ReturnError(2);
                ViewBag.Detail = Detail;
                return View();
            }

        }

        public ActionResult UpdateProjectNames(int id)
        {
            var Detail = (from x in db.ProjectNames where x.ProjectId == id select x).SingleOrDefault();
            var ProjectCategoryList = (from x in db.ProjectCategoryNames orderby x.Name descending select x).ToList();

            ViewBag.ProjectCategoryList = ProjectCategoryList;

            return View(Detail);
        }

        [HttpPost]
        public ActionResult UpdateProjectNames(int id, ProjectNames cnn)
        {
            var Detail = (from x in db.ProjectNames where x.ProjectId == id select x).SingleOrDefault();
            try
            {
                var ProjectCategoryList = (from x in db.ProjectCategoryNames orderby x.Name descending select x).ToList();

                ViewBag.ProjectCategoryList = ProjectCategoryList;
                Detail.Name = cnn.Name;
                db.SaveChanges();
                db.Dispose();
                TempData["Success"] = GeneralFunctions.ReturnSuccess(2);
                return View(Detail);
            }
            catch (Exception)
            {
                TempData["Error"] = GeneralFunctions.ReturnError(2);
                return View(Detail);
            }
        }

        public ActionResult UpdateProjectCategory(int id)
        {
            var Detail = (from x in db.ProjectCategoryNames where x.ProjectCategoryId == id select x).SingleOrDefault();
            var CategoryList = (from x in db.ProjectCategoryNames orderby x.Name ascending select x).ToList();

            ViewBag.CategoryList = CategoryList;
            return View(Detail);
        }

        [HttpPost]
        public ActionResult UpdateProjectCategory(int id, ProjectCategoryNames rcn)
        {
            var CategoryList = (from x in db.ProjectCategoryNames orderby x.Name ascending select x).ToList();

            ViewBag.CategoryList = CategoryList;
            var Detail = (from x in db.ProjectCategoryNames where x.ProjectCategoryId == id select x).SingleOrDefault();
            try
            {
                Detail.Name = rcn.Name;
                db.SaveChanges();
                db.Dispose();
                TempData["Success"] = GeneralFunctions.ReturnSuccess(2);
                return View(Detail);
            }
            catch (Exception)
            {
                TempData["Error"] = GeneralFunctions.ReturnError(2);
                return View(Detail);
            }
        }

        public ActionResult UpdateProjectCategoryLanguage(int id)
        {
            int LanguageId = int.Parse(Request.QueryString["LangId"]);
            var Detail = (from x in db.ProjectCategoryLanguages where x.ProjectCategoryId == id && x.LanguageId == LanguageId select x).SingleOrDefault();
            if (Detail != null)
            {
                ViewBag.Detail = Detail;
                return View();
            }
            else
            {
                return View();
            }

        }

        [HttpPost, ValidateInput(false)]
        public ActionResult UpdateProjectCategoryLanguage(int id, ProjectCategoryLanguages blg)
        {
            int LanguageId = int.Parse(Request.QueryString["LangId"]);
            var Detail = (from x in db.ProjectCategoryLanguages where x.ProjectCategoryId == id && x.LanguageId == LanguageId select x).SingleOrDefault();
            try
            {

                Detail.Title = blg.Title;

                db.SaveChanges();
                db.Dispose();
                TempData["Success"] = GeneralFunctions.ReturnSuccess(2);
                ViewBag.Detail = Detail;
                return View();
            }
            catch (Exception)
            {
                TempData["Error"] = GeneralFunctions.ReturnError(2);
                ViewBag.Detail = Detail;
                return View();
            }

        }

        public ActionResult Delete(int id)
        {
            var detail = (from x in db.ProjectNames where x.ProjectId == id select x).FirstOrDefault();
            var ProjectLanguagesList = (from x in db.ProjectLanguages where x.ProjectId == id select x).ToList();

            if (detail != null)
            {
                db.ProjectNames.Remove(detail);

                foreach (var item in ProjectLanguagesList)
                {
                    db.ProjectLanguages.Remove(item);
                }

                db.SaveChanges();
            }
            db.Dispose();
            return RedirectToAction("ProjectNames", "Project");
        }

        public ActionResult DeleteLanguage(int id, int LangId)
        {
            var Detail = (from x in db.ProjectLanguages where x.ProjectId == id && x.LanguageId == LangId select x).SingleOrDefault();

            if (Detail != null)
            {
                db.ProjectLanguages.Remove(Detail);
                db.SaveChanges();
                db.Dispose();
            }

            return RedirectToAction("CreateProjectLanguage", "Project", new { id = id });
        }

        public ActionResult DeleteCategoryLanguage(int id, int LangId)
        {
            var Detail = (from x in db.ProjectCategoryLanguages where x.ProjectCategoryId == id && x.LanguageId == LangId select x).SingleOrDefault();

            if (Detail != null)
            {
                db.ProjectCategoryLanguages.Remove(Detail);
                db.SaveChanges();
                db.Dispose();
            }

            return RedirectToAction("CreateProjectCategoryLanguage", "Project", new { id = id });
        }

        public JsonResult SortCategory(List<string> Ids)
        {

            try
            {
                for (int i = 1; i < Ids.Count + 1; i++)
                {
                    var id = int.Parse(Ids[i - 1]);
                    var ProjectCategory = (from x in db.ProjectCategoryNames where x.ProjectCategoryId == id select x).SingleOrDefault();
                    ProjectCategory.OrderNumber = i;
                    db.SaveChanges();
                }
                db.Dispose();
                return Json(new { Code = 200 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { Code = 500 }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult Sort(List<string> Ids)
        {

            try
            {
                for (int i = 1; i < Ids.Count + 1; i++)
                {
                    var id = int.Parse(Ids[i - 1]);
                    var Project = (from x in db.ProjectNames where x.ProjectId == id select x).SingleOrDefault();
                    Project.OrderNumber = i;
                    db.SaveChanges();
                }
                db.Dispose();
                return Json(new { Code = 200 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { Code = 500 }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ProjectImages(int id)
        {
            var List = (from x in db.ProjectLanguages join g in db.ProjectImageLanguages on x.ProjectLanguageId equals g.ProjectLanguageId where x.ProjectId == id orderby g.OrderNumber ascending select new { x, g }).ToList();

            ViewBag.ProjectLanguageList = (from x in db.ProjectLanguages where x.ProjectId == id select x).ToList();


            var ImageList = new List<ProjectImageLanguages>();

            foreach (var item in List)
            {
                ImageList.Add(new ProjectImageLanguages
                {
                    CoverPic = item.g.CoverPic,
                    Description = item.g.Description,
                    ProjectImageLanguageId = item.g.ProjectImageLanguageId,
                    ProjectLanguageId = item.g.ProjectLanguageId,
                    ImageName = item.g.ImageName,
                    InsertedDate = item.g.InsertedDate,
                    OrderNumber = item.g.OrderNumber,
                    Status = item.g.Status,
                    Title = item.g.Title
                });
            }

            return View(ImageList);
        }

        public ActionResult CreateProjectImage(int id)
        {
            var ProjectLanguageList = (from x in db.ProjectLanguages where x.ProjectId == id select x).ToList();

            ViewBag.ProjectLanguageList = ProjectLanguageList;

            return View();
        }

        [HttpPost]
        public ActionResult CreateProjectImage(int id, ProjectImageLanguages ciml, HttpPostedFileBase[] file)
        {
            var ProjectLanguageList = (from x in db.ProjectLanguages where x.ProjectId == id select x).ToList();

            ViewBag.ProjectLanguageList = ProjectLanguageList;
            try
            {
                if (file != null)
                {
                    foreach (var item in file)
                    {
                        string image = string.Empty;

                        var extension = item.FileName.Substring(item.FileName.LastIndexOf('.'));
                        image = Guid.NewGuid().ToString() + extension;
                        var imageToResize = System.Drawing.Image.FromStream(item.InputStream);
                        ImageSave.InsertImage(imageToResize, image);


                        var row = new ProjectImageLanguages
                        {
                            CoverPic = false,
                            Status = 1,
                            Description = "",
                            ImageName = image,
                            Title = "",
                            ProjectLanguageId = ciml.ProjectLanguageId,
                            
                            OrderNumber = 0,
                            InsertedDate = DateTime.Now
                        };
                        db.ProjectImageLanguages.Add(row);
                        db.SaveChanges();
                        TempData["Success"] = GeneralFunctions.ReturnSuccess(1);
                    }
                    db.Dispose();
                }

                return RedirectToAction("CreateProjectImage", "Project", new { id = id });
            }
            catch (Exception)
            {
                return RedirectToAction("CreateProjectImage", "Project", new { id = id });
            }

        }

        public ActionResult UpdateProjectImage(int id)
        {
            var Detail = (from x in db.ProjectImageLanguages where x.ProjectImageLanguageId == id select x).SingleOrDefault();

            if (Detail != null)
            {
                return View(Detail);
            }
            else
            {
                return View();
            }
        }

        [HttpPost]
        public ActionResult UpdateProjectImage(int id, ProjectImageLanguages ciml, HttpPostedFileBase file, bool? Tab)
        {
            var Detail = (from x in db.ProjectImageLanguages where x.ProjectImageLanguageId == id select x).SingleOrDefault();
            try
            {
                bool TabControl = false;
                if (Tab != null)
                {
                    TabControl = true;
                }
                string image = string.Empty;
                if (file != null)
                {
                    var extension = file.FileName.Substring(file.FileName.LastIndexOf('.'));
                    image = Guid.NewGuid().ToString() + extension;
                    var imageToResize = System.Drawing.Image.FromStream(file.InputStream);
                    ImageSave.InsertImage(imageToResize, image);
                    Detail.ImageName = image;
                }
                if (TabControl)
                {
                    var ProjectImageList = (from x in db.ProjectImageLanguages where x.ProjectLanguageId == Detail.ProjectLanguageId select x).ToList();

                    if (ProjectImageList.Count > 0)
                    {
                        foreach (var item in ProjectImageList)
                        {
                            item.CoverPic = false;
                            db.SaveChanges();
                        }
                    }

                    Detail.CoverPic = true;
                }

                Detail.Description = ciml.Description;
                Detail.Title = ciml.Title;

                db.SaveChanges();
                db.Dispose();
                TempData["Success"] = GeneralFunctions.ReturnSuccess(2);
                return View(Detail);
            }
            catch (Exception)
            {
                TempData["Error"] = GeneralFunctions.ReturnError(2);
                return View(Detail);
            }

        }

        public ActionResult DeleteProjectImage(int id)
        {
            var detail = (from x in db.ProjectImageLanguages where x.ProjectImageLanguageId == id select x).FirstOrDefault();
            var ProjectDetail = (from x in db.ProjectLanguages where x.ProjectLanguageId == detail.ProjectLanguageId select x).SingleOrDefault();
            if (detail != null)
            {
                var imagename = detail.ImageName;
                if (System.IO.File.Exists(Server.MapPath("/media/images/" + imagename)))
                {
                    System.IO.File.Delete(Server.MapPath("/media/images/" + imagename));
                }

                if (System.IO.File.Exists(Server.MapPath("/media/images/thumbnails" + imagename)))
                {
                    System.IO.File.Delete(Server.MapPath("/media/images/thumbnails" + imagename));
                }

                if (System.IO.File.Exists(Server.MapPath("/media/images/thumbnails40" + imagename)))
                {
                    System.IO.File.Delete(Server.MapPath("/media/images/thumbnails40" + imagename));
                }
                db.ProjectImageLanguages.Remove(detail);


                db.SaveChanges();
            }
            db.Dispose();
            return RedirectToAction("ProjectImages", "Project", new { id = ProjectDetail.ProjectId });
        }

        public JsonResult Imagesort(List<string> Ids)
        {
            try
            {
                for (int i = 1; i < Ids.Count + 1; i++)
                {
                    if (!string.IsNullOrEmpty(Ids[i - 1]))
                    {
                        var id = int.Parse(Ids[i - 1]);

                        var Image = (from x in db.ProjectImageLanguages where x.ProjectImageLanguageId == id select x).SingleOrDefault();
                        Image.OrderNumber = i;
                        db.SaveChanges();
                    }

                }
                db.Dispose();
                return Json(new { Code = 200 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { Code = 500 }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ProjectVideos(int id)
        {
            var List = (from x in db.ProjectLanguages join g in db.ProjectVideoLanguages on x.ProjectLanguageId equals g.ProjectLanguageId where x.ProjectId == id orderby g.OrderNumber ascending select new { x, g }).ToList();

            ViewBag.ProjectLanguageList = (from x in db.ProjectLanguages where x.ProjectId == id select x).ToList();


            var VideoList = new List<ProjectVideoLanguages>();

            foreach (var item in List)
            {
                VideoList.Add(new ProjectVideoLanguages
                {

                    Description = item.g.Description,
                    ProjectVideoLanguageId = item.g.ProjectVideoLanguageId,
                    ProjectLanguageId = item.g.ProjectLanguageId,
                    InsertedDate = item.g.InsertedDate,
                    OrderNumber = item.g.OrderNumber,
                    VideoIdentity = item.g.VideoIdentity,
                    VideoType = item.g.VideoType,
                    Status = item.g.Status,
                    Title = item.g.Title
                });
            }

            return View(VideoList);
        }

        public ActionResult CreateProjectVideo(int id)
        {
            var ProjectLanguageList = (from x in db.ProjectLanguages where x.ProjectId == id select x).ToList();

            ViewBag.ProjectLanguageList = ProjectLanguageList;

            return View();
        }

        [HttpPost]
        public ActionResult CreateProjectVideo(int id, ProjectVideoLanguages gvl)
        {
            var ProjectLanguageList = (from x in db.ProjectLanguages where x.ProjectId == id select x).ToList();

            ViewBag.ProjectLanguageList = ProjectLanguageList;
            try
            {

                string videoidentity = string.Empty;
                if (gvl.VideoType == 1)
                {
                    var VideoUrl = new Uri(gvl.VideoIdentity);
                    NameValueCollection nvc = new NameValueCollection(HttpUtility.ParseQueryString(VideoUrl.Query));
                    var VideoIdentity = nvc["v"];
                    videoidentity = VideoIdentity;
                }
                else
                {
                    var urlssplit = gvl.VideoIdentity.Split('/');

                    videoidentity = urlssplit[urlssplit.Length - 1].ToString();
                }

                var row = new ProjectVideoLanguages
                {
                    VideoType = gvl.VideoType,
                    Status = 1,
                    Description = gvl.Description,
                    VideoIdentity = videoidentity,
                    Title = gvl.Title,
                    ProjectLanguageId = gvl.ProjectLanguageId,
                    OrderNumber = 0,
                    InsertedDate = DateTime.Now
                };
                db.ProjectVideoLanguages.Add(row);
                db.SaveChanges(); db.Dispose();
                TempData["Success"] = GeneralFunctions.ReturnSuccess(1);

                return RedirectToAction("CreateProjectVideo", "Project", new { id = id });
            }
            catch (Exception)
            {
                return RedirectToAction("CreateProjectVideo", "Project", new { id = id });
            }

        }

        public ActionResult UpdateProjectVideo(int id)
        {
            var Detail = (from x in db.ProjectVideoLanguages where x.ProjectVideoLanguageId == id select x).SingleOrDefault();

            if (Detail != null)
            {
                return View(Detail);
            }
            else
            {
                return View();
            }
        }

        [HttpPost]
        public ActionResult UpdateProjectVideo(int id, ProjectVideoLanguages gvl)
        {
            var Detail = (from x in db.ProjectVideoLanguages where x.ProjectVideoLanguageId == id select x).SingleOrDefault();
            try
            {
                string videoidentity = string.Empty;
                if (gvl.VideoType == 1)
                {
                    var VideoUrl = new Uri(gvl.VideoIdentity);
                    NameValueCollection nvc = new NameValueCollection(HttpUtility.ParseQueryString(VideoUrl.Query));
                    var VideoIdentity = nvc["v"];
                    videoidentity = VideoIdentity;
                    Detail.VideoIdentity = videoidentity;
                }
                else
                {
                    var urlssplit = gvl.VideoIdentity.Split('/');
                    videoidentity = urlssplit[urlssplit.Length - 1].ToString();
                    Detail.VideoIdentity = videoidentity;
                }

                Detail.Description = gvl.Description;
                Detail.Title = gvl.Title;
                Detail.VideoType = gvl.VideoType;

                db.SaveChanges();
                db.Dispose();
                TempData["Success"] = GeneralFunctions.ReturnSuccess(2);
                return View(Detail);
            }
            catch (Exception)
            {
                TempData["Error"] = GeneralFunctions.ReturnError(2);
                return View(Detail);
            }

        }

        public ActionResult DeleteProjectVideo(int id)
        {
            var detail = (from x in db.ProjectVideoLanguages where x.ProjectVideoLanguageId == id select x).FirstOrDefault();
            var ProjectDetail = (from x in db.ProjectLanguages where x.ProjectLanguageId == detail.ProjectLanguageId select x).SingleOrDefault();
            if (detail != null)
            {
                db.ProjectVideoLanguages.Remove(detail);
                db.SaveChanges();
            }
            db.Dispose();
            return RedirectToAction("ProjectVideos", "Project", new { id = ProjectDetail.ProjectId });
        }

        public JsonResult Videosort(List<string> Ids)
        {
            try
            {
                for (int i = 1; i < Ids.Count + 1; i++)
                {
                    if (!string.IsNullOrEmpty(Ids[i - 1]))
                    {
                        var id = int.Parse(Ids[i - 1]);

                        var Video = (from x in db.ProjectVideoLanguages where x.ProjectVideoLanguageId == id select x).SingleOrDefault();
                        Video.OrderNumber = i;
                        db.SaveChanges();
                    }

                }
                db.Dispose();
                return Json(new { Code = 200 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { Code = 500 }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public JsonResult ChangeStatus(ChangeStatus1 chs)
        {
            try
            {
                var Detail = (from x in db.ProjectNames where x.ProjectId == chs.id select x).FirstOrDefault();

                if (chs.Status == 2)
                {
                    Detail.Status = 2;
                }
                else
                {
                    Detail.Status = 1;
                }

                db.SaveChanges();
                db.Dispose();

                return Json(new { Ok = "OK" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { Ok = "BAD" }, JsonRequestBehavior.DenyGet);
            }


        }

        [Serializable]
        public class ChangeStatus1
        {
            public int id { get; set; }
            public int Status { get; set; }
        }


        [HttpPost]
        public JsonResult ChangeStatusCategory(ChangeStatusCategory1 chs)
        {
            try
            {
                var Detail = (from x in db.ProjectCategoryNames where x.ProjectCategoryId == chs.id select x).FirstOrDefault();

                if (chs.Status == 2)
                {
                    Detail.Status = 2;
                }
                else
                {
                    Detail.Status = 1;
                }

                db.SaveChanges();
                db.Dispose();

                return Json(new { Ok = "OK" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { Ok = "BAD" }, JsonRequestBehavior.DenyGet);
            }


        }

        [Serializable]
        public class ChangeStatusCategory1
        {
            public int id { get; set; }
            public int Status { get; set; }
        }

    }
}
