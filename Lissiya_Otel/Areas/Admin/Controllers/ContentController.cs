﻿using Lissiya_Otel.Functions;
using Lissiya_Otel.Models;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Lissiya_Otel.Areas.Admin.Controllers
{
    public class ContentController : Controller
    {
        lissiyaEntities db = new lissiyaEntities();

        public ActionResult ContentNames()
        {
            var ContentNameList = (from x in db.ContentNames orderby x.OrderNumber ascending select x).ToList();

            ViewBag.LanguageList = LanguageController.Languages();

            return View(ContentNameList);
        }

        public ActionResult CreateContentNames()
        {
            var ContentList = (from x in db.ContentNames orderby x.Name ascending select x).ToList();

            var MenuList = (from x in db.MenuNames orderby x.MenuId ascending select x).ToList();

            ViewBag.MenuList = MenuList;

            return View();
        }

        public ActionResult CreateContentLanguage()
        {
            var LangaugeList = LanguageController.Languages();
            if (LangaugeList.Count > 0)
            {
                ViewBag.LanguageList = LangaugeList;
            }
            return View();
        }

        [HttpPost]
        public ActionResult CreateContentNames(ContentNames cnn)
        {
            try
            {
                int TopContentId = 0;
                if (cnn.TopContentId != null)
                {
                    TopContentId = cnn.TopContentId.Value;
                }
                var row = new ContentNames
                {
                    Status = 1,
                    Name = cnn.Name,
                    OrderNumber = 0,
                    InsertedDate = DateTime.Now,
                    TopContentId = TopContentId
                };
                db.ContentNames.Add(row);
                db.SaveChanges();
                TempData["Success"] = GeneralFunctions.ReturnSuccess(1);

                return RedirectToAction("CreateContentLanguage", "Content", new { id = row.ContentId });
            }
            catch (Exception)
            {
                TempData["Error"] = GeneralFunctions.ReturnError(1);
                return View();
            }

        }

        [HttpPost, ValidateInput(false)]
        public ActionResult CreateContentLanguage(ContentLanguages clg, string updatecontrol, HttpPostedFileBase file)
        {
            var LangaugeList = LanguageController.Languages();
            if (LangaugeList.Count > 0)
            {
                ViewBag.LanguageList = LangaugeList;
            }
            try
            {
                if (clg.LanguageId != null)
                {
                    var Detail = (from x in db.ContentLanguages where x.ContentId == clg.ContentId && x.LanguageId == clg.LanguageId select x).SingleOrDefault();

                    if (Detail == null)
                    {
                        string image = string.Empty;
                        if (file != null)
                        {
                            var extension = file.FileName.Substring(file.FileName.LastIndexOf('.'));
                            image = Guid.NewGuid().ToString() + extension;
                            var imageToResize = System.Drawing.Image.FromStream(file.InputStream);
                            ImageSave.InsertImage(imageToResize, image);
                        }
                        var row = new ContentLanguages
                        {
                            InsertedDate = DateTime.Now,
                            LanguageId = clg.LanguageId,
                            OrderNumber = 0,
                            Title = clg.Title,
                            ContentId = clg.ContentId,
                            Description = clg.Description,
                            Hit = 0,
                            SeoDescription = clg.SeoDescription,
                            SeoKeywords = clg.SeoKeywords,
                            SeoTitle = clg.SeoTitle,
                            Status = 1,
                            Summary = clg.Summary

                        };

                        db.ContentLanguages.Add(row);
                        db.SaveChanges();
                        db.Dispose();
                        TempData["Success"] = GeneralFunctions.ReturnSuccess(1);
                        if (!string.IsNullOrEmpty(updatecontrol))
                        {
                            return RedirectToAction("UpdateContentLanguage", "Content", new { id = row.ContentId, LangId = row.LanguageId });
                        }
                        else
                        {
                            return RedirectToAction("CreateContentLanguage", "Content", new { id = row.ContentId });
                        }
                    }
                    else
                    {
                        TempData["Info"] = GeneralFunctions.ReturnInfo(1);
                        if (!string.IsNullOrEmpty(updatecontrol))
                        {
                            return RedirectToAction("UpdateContentLanguage", "Content", new { id = clg.ContentId, LangId = clg.LanguageId });
                        }
                        else
                        {
                            return RedirectToAction("CreateContentLanguage", "Content", new { id = clg.ContentId });
                        }
                    }

                }
                else
                {
                    TempData["Error"] = GeneralFunctions.ReturnError(4);
                    if (!string.IsNullOrEmpty(updatecontrol))
                    {
                        return RedirectToAction("UpdateContentLanguage", "Content", new { id = clg.ContentId, LangId = clg.LanguageId });
                    }
                    else
                    {
                        return RedirectToAction("CreateContentLanguage", "Content", new { id = clg.ContentId });
                    }

                }

            }
            catch (Exception)
            {
                TempData["Error"] = GeneralFunctions.ReturnError(1);
                if (!string.IsNullOrEmpty(updatecontrol))
                {
                    return RedirectToAction("UpdateContentLanguage", "Content", new { id = clg.ContentId, LangId = clg.LanguageId });
                }
                else
                {
                    return RedirectToAction("CreateContentLanguage", "Content", new { id = clg.ContentId });
                }

            }
        }

        public ActionResult UpdateContentLanguage(int id)
        {
            int LanguageId = int.Parse(Request.QueryString["LangId"]);
            var Detail = (from x in db.ContentLanguages where x.ContentId == id && x.LanguageId == LanguageId select x).SingleOrDefault();
            if (Detail != null)
            {
                ViewBag.Detail = Detail;
                return View();
            }
            else
            {
                return View();
            }

        }

        [HttpPost, ValidateInput(false)]
        public ActionResult UpdateContentLanguage(int id, ContentLanguages blg, HttpPostedFileBase file)
        {
            int LanguageId = int.Parse(Request.QueryString["LangId"]);
            var Detail = (from x in db.ContentLanguages where x.ContentId == id && x.LanguageId == LanguageId select x).SingleOrDefault();
            try
            {


                Detail.Description = blg.Description;
                Detail.SeoDescription = blg.SeoDescription;
                Detail.SeoKeywords = blg.SeoKeywords;
                Detail.SeoTitle = blg.SeoTitle;
                Detail.Summary = blg.Summary;
                Detail.Title = blg.Title;

                db.SaveChanges();
                db.Dispose();
                TempData["Success"] = GeneralFunctions.ReturnSuccess(2);
                ViewBag.Detail = Detail;
                return View();
            }
            catch (Exception)
            {
                TempData["Error"] = GeneralFunctions.ReturnError(2);
                ViewBag.Detail = Detail;
                return View();
            }

        }

        public ActionResult UpdateContentNames(int id)
        {
            var ContentList = (from x in db.ContentNames orderby x.Name ascending select x).ToList();

            //ViewBag.ContentList = ContentList;
            var MenuList = (from x in db.MenuNames orderby x.MenuId ascending select x).ToList();

            ViewBag.MenuList = MenuList;

            var Detail = (from x in db.ContentNames where x.ContentId == id select x).SingleOrDefault();

            return View(Detail);
        }

        [HttpPost]
        public ActionResult UpdateContentNames(int id, ContentNames cnn)
        {
            var ContentList = (from x in db.ContentNames orderby x.Name ascending select x).ToList();

            ViewBag.ContentList = ContentList;
            var Detail = (from x in db.ContentNames where x.ContentId == id select x).SingleOrDefault();
            try
            {
                Detail.Name = cnn.Name;
                Detail.TopContentId = cnn.TopContentId;
                db.SaveChanges();
                db.Dispose();
                TempData["Success"] = GeneralFunctions.ReturnSuccess(2);
                return View(Detail);
            }
            catch (Exception)
            {
                TempData["Error"] = GeneralFunctions.ReturnError(2);
                return View(Detail);
            }


        }

        public ActionResult Delete(int id)
        {
            var detail = (from x in db.ContentNames where x.ContentId == id select x).FirstOrDefault();
            var ContentLanguagesList = (from x in db.ContentLanguages where x.ContentId == id select x).ToList();

            if (detail != null)
            {
                db.ContentNames.Remove(detail);

                foreach (var item in ContentLanguagesList)
                {
                    db.ContentLanguages.Remove(item);
                }

                db.SaveChanges();
            }
            db.Dispose();
            return RedirectToAction("ContentNames", "Content");
        }

        public ActionResult DeleteLanguage(int id, int LangId)
        {
            var Detail = (from x in db.ContentLanguages where x.ContentId == id && x.LanguageId == LangId select x).SingleOrDefault();

            if (Detail != null)
            {
                db.ContentLanguages.Remove(Detail);
                db.SaveChanges();
                db.Dispose();
            }

            return RedirectToAction("CreateContentLanguage", "Content", new { id = id });
        }

        public JsonResult Sort(List<string> Ids)
        {

            try
            {
                for (int i = 1; i < Ids.Count + 1; i++)
                {
                    var id = int.Parse(Ids[i - 1]);
                    var Content = (from x in db.ContentNames where x.ContentId == id select x).SingleOrDefault();
                    Content.OrderNumber = i;
                    db.SaveChanges();
                }
                db.Dispose();
                return Json(new { Code = 200 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { Code = 500 }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ContentImages(int id)
        {
            var List = (from x in db.ContentLanguages join g in db.ContentImageLanguages on x.ContentLanguageId equals g.ContentLanguageId where x.ContentId == id orderby g.OrderNumber ascending select new { x, g }).ToList();

            ViewBag.ContentLanguageList = (from x in db.ContentLanguages where x.ContentId == id select x).ToList();


            var ImageList = new List<ContentImageLanguages>();

            foreach (var item in List)
            {
                ImageList.Add(new ContentImageLanguages
                {
                    CoverPic = item.g.CoverPic,
                    Description = item.g.Description,
                    ContentImageLanguageId = item.g.ContentImageLanguageId,
                    ContentLanguageId = item.g.ContentLanguageId,
                    ImageName = item.g.ImageName,
                    InsertedDate = item.g.InsertedDate,
                    OrderNumber = item.g.OrderNumber,
                    Status = item.g.Status,
                    Title = item.g.Title
                });
            }

            return View(ImageList);
        }

        public ActionResult CreateContentImage(int id)
        {
            var ContentLanguageList = (from x in db.ContentLanguages where x.ContentId == id select x).ToList();

            ViewBag.ContentLanguageList = ContentLanguageList;

            return View();
        }

        [HttpPost]
        public ActionResult CreateContentImage(int id, ContentImageLanguages ciml, HttpPostedFileBase[] file)
        {
            var ContentLanguageList = (from x in db.ContentLanguages where x.ContentId == id select x).ToList();

            ViewBag.ContentLanguageList = ContentLanguageList;
            try
            {
                if (file != null)
                {
                    foreach (var item in file)
                    {
                        string image = string.Empty;

                        var extension = item.FileName.Substring(item.FileName.LastIndexOf('.'));
                        image = Guid.NewGuid().ToString() + extension;
                        var imageToResize = System.Drawing.Image.FromStream(item.InputStream);
                        ImageSave.InsertImage(imageToResize, image);


                        var row = new ContentImageLanguages
                        {
                            CoverPic = false,
                            Status = 1,
                            Description = "",
                            ImageName = image,
                            Title = "",
                            ContentLanguageId = ciml.ContentLanguageId,
                            OrderNumber = 0,
                            InsertedDate = DateTime.Now
                        };
                        db.ContentImageLanguages.Add(row);
                        db.SaveChanges();
                        TempData["Success"] = GeneralFunctions.ReturnSuccess(1);
                    }
                    db.Dispose();
                }

                return RedirectToAction("CreateContentImage", "Content", new { id = id });
            }
            catch (Exception)
            {
                return RedirectToAction("CreateContentImage", "Content", new { id = id });
            }

        }

        public ActionResult UpdateContentImage(int id)
        {
            var Detail = (from x in db.ContentImageLanguages where x.ContentImageLanguageId == id select x).SingleOrDefault();

            if (Detail != null)
            {
                return View(Detail);
            }
            else
            {
                return View();
            }
        }

        [HttpPost]
        public ActionResult UpdateContentImage(int id, ContentImageLanguages ciml, HttpPostedFileBase file, bool? Tab)
        {
            var Detail = (from x in db.ContentImageLanguages where x.ContentImageLanguageId == id select x).SingleOrDefault();
            try
            {
                bool TabControl = false;
                if (Tab != null)
                {
                    TabControl = true;
                }
                string image = string.Empty;
                if (file != null)
                {
                    var extension = file.FileName.Substring(file.FileName.LastIndexOf('.'));
                    image = Guid.NewGuid().ToString() + extension;
                    var imageToResize = System.Drawing.Image.FromStream(file.InputStream);
                    ImageSave.InsertImage(imageToResize, image);
                    Detail.ImageName = image;
                }
                if (TabControl)
                {

                    var ContentImageList = (from x in db.ContentImageLanguages where x.ContentLanguageId == Detail.ContentLanguageId select x).ToList();

                    if (ContentImageList.Count > 0)
                    {
                        foreach (var item in ContentImageList)
                        {
                            item.CoverPic = false;
                            db.SaveChanges();
                        }
                    }
                    Detail.CoverPic = true;
                }

                Detail.Description = ciml.Description;
                Detail.Title = ciml.Title;

                db.SaveChanges();
                db.Dispose();
                TempData["Success"] = GeneralFunctions.ReturnSuccess(2);
                return View(Detail);
            }
            catch (Exception)
            {
                TempData["Error"] = GeneralFunctions.ReturnError(2);
                return View(Detail);
            }

        }

        public ActionResult DeleteContentImage(int id)
        {
            var detail = (from x in db.ContentImageLanguages where x.ContentImageLanguageId == id select x).FirstOrDefault();
            var ContentDetail = (from x in db.ContentLanguages where x.ContentLanguageId == detail.ContentLanguageId select x).SingleOrDefault();
            if (detail != null)
            {
                var imagename = detail.ImageName;
                if (System.IO.File.Exists(Server.MapPath("/media/images/" + imagename)))
                {
                    System.IO.File.Delete(Server.MapPath("/media/images/" + imagename));
                }

                if (System.IO.File.Exists(Server.MapPath("/media/images/thumbnails" + imagename)))
                {
                    System.IO.File.Delete(Server.MapPath("/media/images/thumbnails" + imagename));
                }

                if (System.IO.File.Exists(Server.MapPath("/media/images/thumbnails40" + imagename)))
                {
                    System.IO.File.Delete(Server.MapPath("/media/images/thumbnails40" + imagename));
                }

                db.ContentImageLanguages.Remove(detail);

                db.SaveChanges();
            }
            db.Dispose();
            return RedirectToAction("ContentImages", "Content", new { id = ContentDetail.ContentId });
        }

        public JsonResult Imagesort(List<string> Ids)
        {
            try
            {
                for (int i = 1; i < Ids.Count + 1; i++)
                {
                    if (!string.IsNullOrEmpty(Ids[i - 1]))
                    {
                        var id = int.Parse(Ids[i - 1]);

                        var Image = (from x in db.ContentImageLanguages where x.ContentImageLanguageId == id select x).SingleOrDefault();
                        Image.OrderNumber = i;
                        db.SaveChanges();
                    }

                }
                db.Dispose();
                return Json(new { Code = 200 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { Code = 500 }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ContentVideos(int id)
        {
            var List = (from x in db.ContentLanguages join g in db.ContentVideoLanguages on x.ContentLanguageId equals g.ContentLanguageId where x.ContentId == id orderby g.OrderNumber ascending select new { x, g }).ToList();

            ViewBag.ContentLanguageList = (from x in db.ContentLanguages where x.ContentId == id select x).ToList();


            var VideoList = new List<ContentVideoLanguages>();

            foreach (var item in List)
            {
                VideoList.Add(new ContentVideoLanguages
                {

                    Description = item.g.Description,
                    ContentVideoLanguageId = item.g.ContentVideoLanguageId,
                    ContentLanguageId = item.g.ContentLanguageId,
                    InsertedDate = item.g.InsertedDate,
                    OrderNumber = item.g.OrderNumber,
                    VideoIdentity = item.g.VideoIdentity,
                    VideoType = item.g.VideoType,
                    Status = item.g.Status,
                    Title = item.g.Title
                });
            }

            return View(VideoList);
        }

        public ActionResult CreateContentVideo(int id)
        {
            var ContentLanguageList = (from x in db.ContentLanguages where x.ContentId == id select x).ToList();

            ViewBag.ContentLanguageList = ContentLanguageList;

            return View();
        }

        [HttpPost]
        public ActionResult CreateContentVideo(int id, ContentVideoLanguages gvl)
        {
            var ContentLanguageList = (from x in db.ContentLanguages where x.ContentId == id select x).ToList();

            ViewBag.ContentLanguageList = ContentLanguageList;
            try
            {

                string videoidentity = string.Empty;
                if (gvl.VideoType == 1)
                {
                    var VideoUrl = new Uri(gvl.VideoIdentity);
                    NameValueCollection nvc = new NameValueCollection(HttpUtility.ParseQueryString(VideoUrl.Query));
                    var VideoIdentity = nvc["v"];
                    videoidentity = VideoIdentity;
                }
                else
                {
                    var urlssplit = gvl.VideoIdentity.Split('/');

                    videoidentity = urlssplit[urlssplit.Length - 1].ToString();
                }

                var row = new ContentVideoLanguages
                {
                    VideoType = gvl.VideoType,
                    Status = 1,
                    Description = gvl.Description,
                    VideoIdentity = videoidentity,
                    Title = gvl.Title,
                    ContentLanguageId = gvl.ContentLanguageId,
                    OrderNumber = 0,
                    InsertedDate = DateTime.Now
                };
                db.ContentVideoLanguages.Add(row);
                db.SaveChanges(); db.Dispose();
                TempData["Success"] = GeneralFunctions.ReturnSuccess(1);

                return RedirectToAction("CreateContentVideo", "Content", new { id = id });
            }
            catch (Exception)
            {
                return RedirectToAction("CreateContentVideo", "Content", new { id = id });
            }

        }

        public ActionResult UpdateContentVideo(int id)
        {
            var Detail = (from x in db.ContentVideoLanguages where x.ContentVideoLanguageId == id select x).SingleOrDefault();

            if (Detail != null)
            {
                return View(Detail);
            }
            else
            {
                return View();
            }
        }

        [HttpPost]
        public ActionResult UpdateContentVideo(int id, ContentVideoLanguages gvl)
        {
            var Detail = (from x in db.ContentVideoLanguages where x.ContentVideoLanguageId == id select x).SingleOrDefault();
            try
            {
                string videoidentity = string.Empty;
                if (gvl.VideoType == 1)
                {
                    var VideoUrl = new Uri(gvl.VideoIdentity);
                    NameValueCollection nvc = new NameValueCollection(HttpUtility.ParseQueryString(VideoUrl.Query));
                    var VideoIdentity = nvc["v"];
                    videoidentity = VideoIdentity;
                    Detail.VideoIdentity = videoidentity;
                }
                else
                {
                    var urlssplit = gvl.VideoIdentity.Split('/');
                    videoidentity = urlssplit[urlssplit.Length - 1].ToString();
                    Detail.VideoIdentity = videoidentity;
                }

                Detail.Description = gvl.Description;
                Detail.Title = gvl.Title;
                Detail.VideoType = gvl.VideoType;

                db.SaveChanges();
                db.Dispose();
                TempData["Success"] = GeneralFunctions.ReturnSuccess(2);
                return View(Detail);
            }
            catch (Exception)
            {
                TempData["Error"] = GeneralFunctions.ReturnError(2);
                return View(Detail);
            }

        }

        public ActionResult DeleteContentVideo(int id)
        {
            var detail = (from x in db.ContentVideoLanguages where x.ContentVideoLanguageId == id select x).FirstOrDefault();
            var ContentDetail = (from x in db.ContentLanguages where x.ContentLanguageId == detail.ContentLanguageId select x).SingleOrDefault();
            if (detail != null)
            {
                db.ContentVideoLanguages.Remove(detail);
                db.SaveChanges();
            }
            db.Dispose();
            return RedirectToAction("ContentVideos", "Content", new { id = ContentDetail.ContentId });
        }

        public JsonResult Videosort(List<string> Ids)
        {
            try
            {
                for (int i = 1; i < Ids.Count + 1; i++)
                {
                    if (!string.IsNullOrEmpty(Ids[i - 1]))
                    {
                        var id = int.Parse(Ids[i - 1]);

                        var Video = (from x in db.ContentVideoLanguages where x.ContentVideoLanguageId == id select x).SingleOrDefault();
                        Video.OrderNumber = i;
                        db.SaveChanges();
                    }

                }
                db.Dispose();
                return Json(new { Code = 200 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { Code = 500 }, JsonRequestBehavior.AllowGet);
            }

        }

        public ActionResult ContentDocuments(int id)
        {
            var List = (from x in db.ContentLanguages join g in db.ContentDocumentLanguages on x.ContentLanguageId equals g.ContentLanguageId where x.ContentId == id orderby g.OrderNumber ascending select new { x, g }).ToList();

            ViewBag.ContentLanguageList = (from x in db.ContentLanguages where x.ContentId == id select x).ToList();


            var DocumentList = new List<ContentDocumentLanguages>();

            foreach (var item in List)
            {
                DocumentList.Add(new ContentDocumentLanguages
                {
                    ContentDocumentLanguageId = item.g.ContentDocumentLanguageId,
                    ContentLanguageId = item.g.ContentLanguageId,
                    DocumentName = item.g.DocumentName,
                    InsertedDate = item.g.InsertedDate,
                    OrderNumber = item.g.OrderNumber,
                    Status = item.g.Status,
                    Title = item.g.Title
                });
            }

            return View(DocumentList);
        }

        public ActionResult CreateContentDocument(int id)
        {
            var ContentLanguageList = (from x in db.ContentLanguages where x.ContentId == id select x).ToList();

            ViewBag.ContentLanguageList = ContentLanguageList;

            return View();
        }

        [HttpPost]
        public ActionResult CreateContentDocument(int id, ContentDocumentLanguages ciml, HttpPostedFileBase[] file)
        {
            var ContentLanguageList = (from x in db.ContentLanguages where x.ContentId == id select x).ToList();

            ViewBag.ContentLanguageList = ContentLanguageList;
            try
            {
                if (file != null)
                {
                    foreach (var item in file)
                    {
                        string DocumentFile = string.Empty;
                        var extension = item.FileName.Substring(item.FileName.LastIndexOf('.'));
                        DocumentFile = Guid.NewGuid().ToString() + extension;
                        DocumentSave.InsertDocument(item, DocumentFile);

                        var row = new ContentDocumentLanguages
                        {
                            Status = 1,
                            DocumentName = DocumentFile,
                            Title = "",
                            ContentLanguageId = ciml.ContentLanguageId,
                            OrderNumber = 0,
                            InsertedDate = DateTime.Now
                        };
                        db.ContentDocumentLanguages.Add(row);
                        db.SaveChanges();
                        TempData["Success"] = GeneralFunctions.ReturnSuccess(1);
                    }
                    db.Dispose();
                }

                return RedirectToAction("CreateContentDocument", "Content", new { id = id });
            }
            catch (Exception)
            {
                return RedirectToAction("CreateContentDocument", "Content", new { id = id });
            }

        }

        public ActionResult UpdateContentDocument(int id)
        {
            var Detail = (from x in db.ContentDocumentLanguages where x.ContentDocumentLanguageId == id select x).SingleOrDefault();

            if (Detail != null)
            {
                return View(Detail);
            }
            else
            {
                return View();
            }
        }

        [HttpPost]
        public ActionResult UpdateContentDocument(int id, ContentDocumentLanguages ciml, HttpPostedFileBase file)
        {
            var Detail = (from x in db.ContentDocumentLanguages where x.ContentDocumentLanguageId == id select x).SingleOrDefault();
            try
            {
                
                string DocumentFile = string.Empty;
                if (file != null)
                {

                    var extension = file.FileName.Substring(file.FileName.LastIndexOf('.'));
                    DocumentFile = Guid.NewGuid().ToString() + extension;
                    DocumentSave.InsertDocument(file, DocumentFile);
                    Detail.DocumentName = DocumentFile;
                }
             
                Detail.Title = ciml.Title;

                db.SaveChanges();
                db.Dispose();
                TempData["Success"] = GeneralFunctions.ReturnSuccess(2);
                return View(Detail);
            }
            catch (Exception)
            {
                TempData["Error"] = GeneralFunctions.ReturnError(2);
                return View(Detail);
            }

        }

        public ActionResult DeleteContentDocument(int id)
        {
            var detail = (from x in db.ContentDocumentLanguages where x.ContentDocumentLanguageId == id select x).FirstOrDefault();
            var ContentDetail = (from x in db.ContentLanguages where x.ContentLanguageId == detail.ContentLanguageId select x).SingleOrDefault();
            if (detail != null)
            {

                var documentname = detail.DocumentName;
                if (System.IO.File.Exists(Server.MapPath("/media/images/" + documentname)))
                {
                    System.IO.File.Delete(Server.MapPath("/media/images/" + documentname));
                }

                db.ContentDocumentLanguages.Remove(detail);

                db.SaveChanges();
            }
            db.Dispose();
            return RedirectToAction("ContentDocuments", "Content", new { id = ContentDetail.ContentId });
        }

        public JsonResult Documentsort(List<string> Ids)
        {
            try
            {
                for (int i = 1; i < Ids.Count + 1; i++)
                {
                    if (!string.IsNullOrEmpty(Ids[i - 1]))
                    {
                        var id = int.Parse(Ids[i - 1]);

                        var Document = (from x in db.ContentDocumentLanguages where x.ContentDocumentLanguageId == id select x).SingleOrDefault();
                        Document.OrderNumber = i;
                        db.SaveChanges();
                    }

                }
                db.Dispose();
                return Json(new { Code = 200 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { Code = 500 }, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpPost]
        public JsonResult ChangeStatus(ChangeStatus1 chs)
        {
            try
            {
                var Detail = (from x in db.ContentNames where x.ContentId == chs.id select x).FirstOrDefault();

                if (chs.Status == 2)
                {
                    Detail.Status = 2;
                }
                else
                {
                    Detail.Status = 1;
                }

                db.SaveChanges();
                db.Dispose();

                return Json(new { Ok = "OK" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { Ok = "BAD" }, JsonRequestBehavior.DenyGet);
            }


        }

        [Serializable]
        public class ChangeStatus1
        {
            public int id { get; set; }
            public int Status { get; set; }
        }

    }
}
