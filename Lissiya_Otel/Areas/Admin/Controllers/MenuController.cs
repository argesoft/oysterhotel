﻿using Lissiya_Otel.Functions;
using Lissiya_Otel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Lissiya_Otel.Areas.Admin.Controllers
{
    public class MenuController : Controller
    {
        //
        // GET: /Admin/Menu/

        lissiyaEntities db = new lissiyaEntities();

        public ActionResult MenuNames()
        {
            var MenuNameList = (from x in db.MenuNames orderby x.OrderNumber ascending where x.MenuPositionType == 1 orderby x.OrderNumber ascending select x).ToList();

            ViewBag.LanguageList = LanguageController.Languages();

            ViewBag.FooterMenuList = (from x in db.MenuNames orderby x.OrderNumber ascending where x.MenuPositionType == 2 orderby x.OrderNumber ascending select x).ToList();

            return View(MenuNameList);
        }

        public ActionResult CreateMenuNames()
        {
            var MenuList = (from x in db.MenuNames where x.MenuPositionType == 1 orderby x.Name ascending select x).ToList();

            ViewBag.MenuList = MenuList;

            return View();
        }

        public ActionResult CreateMenuLanguage()
        {
            var LangaugeList = LanguageController.Languages();
            if (LangaugeList.Count > 0)
            {
                ViewBag.LanguageList = LangaugeList;
            }

            var ContentList = (from x in db.ContentNames orderby x.Name ascending select x).ToList();

            ViewBag.ContentList = ContentList;

            return View();
        }

        [HttpPost]
        public ActionResult CreateMenuNames(MenuNames mnn)
        {
            try
            {
                var row = new MenuNames
                {
                    Status = 1,
                    Name = mnn.Name,
                    TopMenuId = mnn.TopMenuId,
                    MenuPositionType = mnn.MenuPositionType,
                    OrderNumber = 0,
                    InsertedDate = DateTime.Now
                };
                db.MenuNames.Add(row);
                db.SaveChanges();
                TempData["Success"] = GeneralFunctions.ReturnSuccess(1);

                return RedirectToAction("CreateMenuLanguage", "Menu", new { id = row.MenuId });
            }
            catch (Exception)
            {
                TempData["Error"] = GeneralFunctions.ReturnError(1);
                return View();
            }

        }

        [HttpPost, ValidateInput(false)]
        public ActionResult CreateMenuLanguage(MenuLanguages blg, string updatecontrol, HttpPostedFileBase file)
        {
            var LangaugeList = LanguageController.Languages();
            if (LangaugeList.Count > 0)
            {
                ViewBag.LanguageList = LangaugeList;
            }
            var ContentList = (from x in db.ContentNames orderby x.Name ascending select x).ToList();

            ViewBag.ContentList = ContentList;
            try
            {
                if (blg.LanguageId != null)
                {
                    var Detail = (from x in db.MenuLanguages where x.MenuId == blg.MenuId && x.LanguageId == blg.LanguageId select x).SingleOrDefault();

                    if (Detail == null)
                    {
                        string Document = string.Empty;
                        if (file != null)
                        {
                            var extension = file.FileName.Substring(file.FileName.LastIndexOf('.'));
                            Document = Guid.NewGuid().ToString() + extension;
                        }
                        var row = new MenuLanguages();
                        if (blg.Type == 1)
                        {
                            row = new MenuLanguages
                            {
                                Type = blg.Type,
                                InsertedDate = DateTime.Now,
                                LanguageId = blg.LanguageId,
                                OrderNumber = 0,
                                Title = blg.Title,
                                MenuId = blg.MenuId,
                                MenuDocument = Document,
                                Status = 1,
                                ContentId = blg.ContentId,
                                Url = blg.Url

                            };
                            db.MenuLanguages.Add(row);

                        }
                        else if (blg.Type == 2)
                        {
                            row = new MenuLanguages
                            {
                                Type = blg.Type,
                                InsertedDate = DateTime.Now,
                                LanguageId = blg.LanguageId,
                                OrderNumber = 0,
                                Title = blg.Title,
                                MenuId = blg.MenuId,
                                MenuDocument = Document,
                                Status = 1,
                                ContentId = 0,
                                Url = blg.Url

                            };
                            db.MenuLanguages.Add(row);
                        }
                        else
                        {
                            row = new MenuLanguages
                            {
                                Type = blg.Type,
                                InsertedDate = DateTime.Now,
                                LanguageId = blg.LanguageId,
                                OrderNumber = 0,
                                Title = blg.Title,
                                MenuId = blg.MenuId,
                                MenuDocument = Document,
                                Status = 1,
                                ContentId = 0,
                                Url = blg.Url

                            };
                            db.MenuLanguages.Add(row);
                        }



                        db.SaveChanges();
                        db.Dispose();
                        TempData["Success"] = GeneralFunctions.ReturnSuccess(1);
                        if (!string.IsNullOrEmpty(updatecontrol))
                        {
                            return RedirectToAction("UpdateMenuLanguage", "Menu", new { id = row.MenuId, LangId = row.LanguageId });
                        }
                        else
                        {
                            return RedirectToAction("CreateMenuLanguage", "Menu", new { id = row.MenuId });
                        }
                    }
                    else
                    {
                        TempData["Info"] = GeneralFunctions.ReturnInfo(1);
                        if (!string.IsNullOrEmpty(updatecontrol))
                        {
                            return RedirectToAction("UpdateMenuLanguage", "Menu", new { id = blg.MenuId, LangId = blg.LanguageId });
                        }
                        else
                        {
                            return RedirectToAction("CreateMenuLanguage", "Menu", new { id = blg.MenuId });
                        }
                    }

                   

                }
                else
                {
                    TempData["Error"] = GeneralFunctions.ReturnError(4);
                    if (!string.IsNullOrEmpty(updatecontrol))
                    {
                        return RedirectToAction("UpdateMenuLanguage", "Menu", new { id = blg.MenuId, LangId = blg.LanguageId });
                    }
                    else
                    {
                        return RedirectToAction("CreateMenuLanguage", "Menu", new { id = blg.MenuId });
                    }

                }

            }
            catch (Exception)
            {
                TempData["Error"] = GeneralFunctions.ReturnError(1);
                if (!string.IsNullOrEmpty(updatecontrol))
                {
                    return RedirectToAction("UpdateMenuLanguage", "Menu", new { id = blg.MenuId, LangId = blg.LanguageId });
                }
                else
                {
                    return RedirectToAction("CreateMenuLanguage", "Menu", new { id = blg.MenuId });
                }
            }
        }

        public ActionResult UpdateMenuLanguage(int id)
        {
            int LanguageId = int.Parse(Request.QueryString["LangId"]);
            var Detail = (from x in db.MenuLanguages where x.MenuId == id && x.LanguageId == LanguageId select x).SingleOrDefault();

            var ContentList = (from x in db.ContentNames orderby x.Name ascending select x).ToList();

            ViewBag.ContentList = ContentList;

            if (Detail != null)
            {
                ViewBag.Detail = Detail;
                return View();
            }
            else
            {
                return View();
            }
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult UpdateMenuLanguage(int id, MenuLanguages blg, HttpPostedFileBase file)
        {
            int LanguageId = int.Parse(Request.QueryString["LangId"]);
            var ContentList = (from x in db.ContentNames orderby x.Name ascending select x).ToList();

            ViewBag.ContentList = ContentList;
            var Detail = (from x in db.MenuLanguages where x.MenuId == id && x.LanguageId == LanguageId select x).SingleOrDefault();
            try
            {
                string Document = string.Empty;
                if (file != null)
                {
                    var extension = file.FileName.Substring(file.FileName.LastIndexOf('.'));
                    Document = Guid.NewGuid().ToString() + extension;
                    Detail.MenuDocument = Document;
                }
                Detail.ContentId = blg.ContentId;
                Detail.Type = blg.Type;
                Detail.Url = blg.Url;
                Detail.Title = blg.Title;

                db.SaveChanges();
                db.Dispose();
                TempData["Success"] = GeneralFunctions.ReturnSuccess(2);
                ViewBag.Detail = Detail;
                return View();
            }
            catch (Exception)
            {
                TempData["Error"] = GeneralFunctions.ReturnError(2);
                ViewBag.Detail = Detail;
                return View();
            }

        }

        public ActionResult UpdateMenuNames(int id)
        {
            var Detail = (from x in db.MenuNames where x.MenuId == id select x).SingleOrDefault();

            var MenuList = (from x in db.MenuNames where x.MenuPositionType == 1 orderby x.Name ascending select x).ToList();

            ViewBag.MenuList = MenuList;

            return View(Detail);
        }

        [HttpPost]
        public ActionResult UpdateMenuNames(int id, MenuNames adn)
        {
            var Detail = (from x in db.MenuNames where x.MenuId == id select x).SingleOrDefault();
            try
            {
                Detail.Name = adn.Name;
                Detail.TopMenuId = adn.TopMenuId;
                db.SaveChanges();
                
                TempData["Success"] = GeneralFunctions.ReturnSuccess(2);
                var MenuList = (from x in db.MenuNames where x.MenuPositionType == 1 orderby x.Name ascending select x).ToList();
                
                ViewBag.MenuList = MenuList;
                return View(Detail);
            }
            catch (Exception)
            {
                TempData["Error"] = GeneralFunctions.ReturnError(2);
                return View(Detail);
            }


        }

        public ActionResult Delete(int id)
        {
            var detail = (from x in db.MenuNames where x.MenuId == id &&x.MenuId!=17 && x.MenuId!=29 &&x.MenuId!=31 select x).FirstOrDefault();
            var MenuLanguagesList = (from x in db.MenuLanguages where x.MenuId == id select x).ToList();

            if (detail != null)
            {
                db.MenuNames.Remove(detail);

                foreach (var item in MenuLanguagesList)
                {
                    db.MenuLanguages.Remove(item);
                }

                db.SaveChanges();
            }
            db.Dispose();
            return RedirectToAction("MenuNames", "Menu");
        }

        public ActionResult DeleteLanguage(int id, int LangId)
        {
            var Detail = (from x in db.MenuLanguages where x.MenuId == id && x.LanguageId == LangId select x).SingleOrDefault();

            if (Detail != null)
            {
                db.MenuLanguages.Remove(Detail);
                db.SaveChanges();
                db.Dispose();
            }

            return RedirectToAction("CreateMenuLanguage", "Menu", new { id = id });
        }

        public JsonResult Sort(List<string> Ids)
        {

            try
            {
                for (int i = 1; i < Ids.Count + 1; i++)
                {
                    var id = int.Parse(Ids[i - 1]);
                    var Menu = (from x in db.MenuNames where x.MenuId == id && x.MenuPositionType == 1 select x).SingleOrDefault();
                    Menu.OrderNumber = i;
                    db.SaveChanges();
                }
                db.Dispose();
                return Json(new { Code = 200 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { Code = 500 }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult SortFooter(List<string> Ids)
        {

            try
            {
                for (int i = 1; i < Ids.Count + 1; i++)
                {
                    var id = int.Parse(Ids[i - 1]);
                    var Menu = (from x in db.MenuNames where x.MenuId == id && x.MenuPositionType == 2 select x).SingleOrDefault();
                    Menu.OrderNumber = i;
                    db.SaveChanges();
                }
                db.Dispose();
                return Json(new { Code = 200 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { Code = 500 }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult ChangeStatus(ChangeStatus1 chs)
        {
            try
            {
                var Detail = (from x in db.MenuNames where x.MenuId == chs.id select x).FirstOrDefault();

                if (chs.Status == 2)
                {
                    Detail.Status = 2;
                }
                else
                {
                    Detail.Status = 1;
                }

                db.SaveChanges();
                db.Dispose();

                return Json(new { Ok = "OK" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { Ok = "BAD" }, JsonRequestBehavior.DenyGet);
            }


        }

        [Serializable]
        public class ChangeStatus1
        {
            public int id { get; set; }
            public int Status { get; set; }
        }

    }
}
