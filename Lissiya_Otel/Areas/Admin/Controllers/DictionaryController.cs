﻿using Lissiya_Otel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;

namespace Lissiya_Otel.Areas.Admin.Controllers
{
    public class DictionaryController : Controller
    {
        //
        // GET: /Admin/Dictionary/
        lissiyaEntities db = new lissiyaEntities();

        public ActionResult DictionaryList()
        {
            ViewBag.LanguageList = LanguageController.Languages();

            return View();

        }

        [HttpPost]
        public ActionResult UpdateDictionary(string value, string pk)
        {
            int DictionaryId = int.Parse(pk);
            var Detail = (from x in db.Dictionaries where x.DictionaryId == DictionaryId select x).SingleOrDefault();

            Detail.Value = value;

            db.SaveChanges();

            ViewBag.LanguageList = LanguageController.Languages();


            return RedirectToAction("DictionaryList", "Dictionary");
        }

        public JsonResult DictionaryLangList(int id)
        {
            var List = (from x in db.Dictionaries where x.LanguageId == id orderby x.Name select x).ToList();

            return Json(new { DictionaryList = List }, JsonRequestBehavior.AllowGet);
        }

    }
}
