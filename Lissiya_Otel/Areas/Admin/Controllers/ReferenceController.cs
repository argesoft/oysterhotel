﻿using Lissiya_Otel.Functions;
using Lissiya_Otel.Models;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Lissiya_Otel.Areas.Admin.Controllers
{
    public class ReferenceController : Controller
    {
        lissiyaEntities db = new lissiyaEntities();

        public ActionResult ReferenceNames()
        {
            var ReferenceNameList = (from x in db.ReferenceNames orderby x.OrderNumber ascending select x).ToList();

            ViewBag.LanguageList = LanguageController.Languages();

            return View(ReferenceNameList);
        }

        public ActionResult ReferenceCategories()
        {
            var ReferenceCategoryList = (from x in db.ReferenceCategoryNames orderby x.OrderNumber ascending select x).ToList();

            ViewBag.LanguageList = LanguageController.Languages();

            return View(ReferenceCategoryList);
        }

        public ActionResult CreateReferenceNames()
        {
            var ReferenceCategoryList = (from x in db.ReferenceCategoryNames orderby x.Name ascending select x).ToList();

            ViewBag.ReferenceCategoryList = ReferenceCategoryList;

            return View();
        }

        public ActionResult CreateReferenceLanguage()
        {
            var LangaugeList = LanguageController.Languages();
            if (LangaugeList.Count > 0)
            {
                ViewBag.LanguageList = LangaugeList;
            }
            return View();
        }

        public ActionResult CreateReferenceCategory()
        {
            return View();
        }

        public ActionResult CreateReferenceCategoryLanguage()
        {
            var LangaugeList = LanguageController.Languages();
            if (LangaugeList.Count > 0)
            {
                ViewBag.LanguageList = LangaugeList;
            }
            return View();
        }

        [HttpPost]
        public ActionResult CreateReferenceNames(ReferenceNames cnn)
        {
            try
            {
                var row = new ReferenceNames
                {
                    Status = 1,
                    Name = cnn.Name,
                    OrderNumber = 0,
                    ReferenceCategoryId = cnn.ReferenceCategoryId,
                    InsertedDate = DateTime.Now
                };
                db.ReferenceNames.Add(row);
                db.SaveChanges();
                TempData["Success"] = GeneralFunctions.ReturnSuccess(1);

                return RedirectToAction("CreateReferenceLanguage", "Reference", new { id = row.ReferenceId });
            }
            catch (Exception)
            {
                TempData["Error"] = GeneralFunctions.ReturnError(1);
                return View();
            }

        }

        [HttpPost]
        public ActionResult CreateReferenceCategory(ReferenceCategoryNames rcn)
        {

            try
            {
                var row = new ReferenceCategoryNames
                {
                    InsertedDate = DateTime.Now,
                    Name = rcn.Name,
                    Status = 1,
                    OrderNumber= 0
                };

                db.ReferenceCategoryNames.Add(row);
                db.SaveChanges();

                TempData["Success"] = GeneralFunctions.ReturnSuccess(1);
                return RedirectToAction("CreateReferenceCategoryLanguage", "Reference", new { id = row.ReferenceCategoryId });
            }
            catch (Exception)
            {
                TempData["Error"] = GeneralFunctions.ReturnError(1);
                return View();
            }
        }

        [HttpPost]
        public ActionResult CreateReferenceCategoryLanguage(ReferenceCategoryLanguages rcl, string updatecontrol)
        {
            var LangaugeList = LanguageController.Languages();
            if (LangaugeList.Count > 0)
            {
                ViewBag.LanguageList = LangaugeList;
            }
            try
            {
                if (rcl.LanguageId != null)
                {
                    var Detail = (from x in db.ReferenceCategoryLanguages where x.ReferenceCategoryId == rcl.ReferenceCategoryId && x.LanguageId == rcl.LanguageId select x).SingleOrDefault();

                    if (Detail == null)
                    {
                        var row = new ReferenceCategoryLanguages
                        {
                            InsertedDate = DateTime.Now,
                            LanguageId = rcl.LanguageId,
                            OrderNumber = 0,
                            ReferenceCategoryId = rcl.ReferenceCategoryId,
                            Status = 1,
                            Title = rcl.Title

                        };

                        db.ReferenceCategoryLanguages.Add(row);
                        db.SaveChanges();
                        db.Dispose();
                        TempData["Success"] = GeneralFunctions.ReturnSuccess(1);
                        if (!string.IsNullOrEmpty(updatecontrol))
                        {
                            return RedirectToAction("UpdateReferenceCategoryLanguage", "Reference", new { id = row.ReferenceCategoryId, LangId = row.LanguageId });
                        }
                        else
                        {
                            return RedirectToAction("CreateReferenceCategoryLanguage", "Reference", new { id = row.ReferenceCategoryId });
                        }
                    }
                    else
                    {
                        TempData["Info"] = GeneralFunctions.ReturnInfo(1);
                        if (!string.IsNullOrEmpty(updatecontrol))
                        {
                            return RedirectToAction("UpdateReferenceCategoryLanguage", "Reference", new { id = rcl.ReferenceCategoryId, LangId = rcl.LanguageId });
                        }
                        else
                        {
                            return RedirectToAction("CreateReferenceCategoryLanguage", "Reference", new { id = rcl.ReferenceCategoryId });
                        }
                    }
                   

                }
                else
                {
                    TempData["Error"] = GeneralFunctions.ReturnError(4);
                    if (!string.IsNullOrEmpty(updatecontrol))
                    {
                        return RedirectToAction("UpdateReferenceCategoryLanguage", "Reference", new { id = rcl.ReferenceCategoryId, LangId = rcl.LanguageId });
                    }
                    else
                    {
                        return RedirectToAction("CreateReferenceCategoryLanguage", "Reference", new { id = rcl.ReferenceCategoryId });
                    }
                }
            }
            catch (Exception)
            {
                TempData["Error"] = GeneralFunctions.ReturnError(1);
                if (!string.IsNullOrEmpty(updatecontrol))
                {
                    return RedirectToAction("UpdateReferenceCategoryLanguage", "Reference", new { id = rcl.ReferenceCategoryId, LangId = rcl.LanguageId });
                }
                else
                {
                    return RedirectToAction("CreateReferenceCategoryLanguage", "Reference", new { id = rcl.ReferenceCategoryId });
                }
            }
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult CreateReferenceLanguage(ReferenceLanguages clg, string updatecontrol, HttpPostedFileBase file)
        {
            var LangaugeList = LanguageController.Languages();
            if (LangaugeList.Count > 0)
            {
                ViewBag.LanguageList = LangaugeList;
            }
            try
            {
                if (clg.LanguageId != null)
                {
                    var Detail = (from x in db.ReferenceLanguages where x.ReferenceId == clg.ReferenceId && x.LanguageId == clg.LanguageId select x).SingleOrDefault();

                    if (Detail == null)
                    {
                        string image = string.Empty;
                        if (file != null)
                        {
                            var extension = file.FileName.Substring(file.FileName.LastIndexOf('.'));
                            image = Guid.NewGuid().ToString() + extension;
                            var imageToResize = System.Drawing.Image.FromStream(file.InputStream);
                            ImageSave.InsertImage(imageToResize, image);
                        }

                        var row = new ReferenceLanguages
                        {
                            InsertedDate = DateTime.Now,
                            LanguageId = clg.LanguageId,
                            OrderNumber = 0,
                            Title = clg.Title,
                            Description = clg.Description,
                            SeoDescription = clg.SeoDescription,
                            SeoKeywords = clg.SeoKeywords,
                            SeoTitle = clg.SeoTitle,
                            Status = 1,
                            ReferenceId = clg.ReferenceId,
                            Summary = clg.Summary,
                            CustomerName = clg.CustomerName,
                            Date = clg.Date,
                            Link = clg.Link,
                            Resim = image
                        };

                        db.ReferenceLanguages.Add(row);
                        db.SaveChanges();
                        db.Dispose();
                        TempData["Success"] = GeneralFunctions.ReturnSuccess(1);
                        if (!string.IsNullOrEmpty(updatecontrol))
                        {
                            return RedirectToAction("UpdateReferenceLanguage", "Reference", new { id = row.ReferenceId, LangId = row.LanguageId });
                        }
                        else
                        {
                            return RedirectToAction("CreateReferenceLanguage", "Reference", new { id = row.ReferenceId });
                        }
                    }
                    else
                    {
                        TempData["Info"] = GeneralFunctions.ReturnInfo(1);
                        if (!string.IsNullOrEmpty(updatecontrol))
                        {
                            return RedirectToAction("UpdateReferenceLanguage", "Reference", new { id = clg.ReferenceId, LangId = clg.LanguageId });
                        }
                        else
                        {
                            return RedirectToAction("CreateReferenceLanguage", "Reference", new { id = clg.ReferenceId });
                        }
                    }

                 

                }
                else
                {
                    TempData["Error"] = GeneralFunctions.ReturnError(4);
                    if (!string.IsNullOrEmpty(updatecontrol))
                    {
                        return RedirectToAction("UpdateReferenceLanguage", "Reference", new { id = clg.ReferenceId, LangId = clg.LanguageId });
                    }
                    else
                    {
                        return RedirectToAction("CreateReferenceLanguage", "Reference", new { id = clg.ReferenceId });
                    }

                }

            }
            catch (Exception)
            {
                TempData["Error"] = GeneralFunctions.ReturnError(1);
                if (!string.IsNullOrEmpty(updatecontrol))
                {
                    return RedirectToAction("UpdateReferenceLanguage", "Reference", new { id = clg.ReferenceId, LangId = clg.LanguageId });
                }
                else
                {
                    return RedirectToAction("CreateReferenceLanguage", "Reference", new { id = clg.ReferenceId });
                }

            }
        }

        public ActionResult UpdateReferenceLanguage(int id)
        {
            int LanguageId = int.Parse(Request.QueryString["LangId"]);
            var Detail = (from x in db.ReferenceLanguages where x.ReferenceId == id && x.LanguageId == LanguageId select x).SingleOrDefault();
            if (Detail != null)
            {
                ViewBag.Detail = Detail;
                return View();
            }
            else
            {
                return View();
            }

        }

        [HttpPost, ValidateInput(false)]
        public ActionResult UpdateReferenceLanguage(int id, ReferenceLanguages blg, HttpPostedFileBase file)
        {
            int LanguageId = int.Parse(Request.QueryString["LangId"]);
            var Detail = (from x in db.ReferenceLanguages where x.ReferenceId == id && x.LanguageId == LanguageId select x).SingleOrDefault();
            try
            {
                string logo = string.Empty;
                if (file != null)
                {
                    var extension = file.FileName.Substring(file.FileName.LastIndexOf('.'));
                    logo = Guid.NewGuid().ToString() + extension;
                    var imageToResize = System.Drawing.Image.FromStream(file.InputStream);
                    ImageSave.InsertImage(imageToResize, logo);
                    Detail.Resim = logo;
                }

                Detail.Description = blg.Description;
                Detail.SeoDescription = blg.SeoDescription;
                Detail.SeoKeywords = blg.SeoKeywords;
                Detail.SeoTitle = blg.SeoTitle;
                Detail.Summary = blg.Summary;
                Detail.Title = blg.Title;
                Detail.Link = blg.Link;
                Detail.CustomerName = blg.CustomerName;
                Detail.Date = blg.Date;

                db.SaveChanges();
                db.Dispose();
                TempData["Success"] = GeneralFunctions.ReturnSuccess(2);
                ViewBag.Detail = Detail;
                return View();
            }
            catch (Exception)
            {
                TempData["Error"] = GeneralFunctions.ReturnError(2);
                ViewBag.Detail = Detail;
                return View();
            }

        }

        public ActionResult UpdateReferenceNames(int id)
        {
            var Detail = (from x in db.ReferenceNames where x.ReferenceId == id select x).SingleOrDefault();
            var ReferenceCategoryList = (from x in db.ReferenceCategoryNames orderby x.Name descending select x).ToList();

            ViewBag.ReferenceCategoryList = ReferenceCategoryList;

            return View(Detail);
        }

        [HttpPost]
        public ActionResult UpdateReferenceNames(int id, ReferenceNames cnn)
        {
            var Detail = (from x in db.ReferenceNames where x.ReferenceId == id select x).SingleOrDefault();
            try
            {
                var ReferenceCategoryList = (from x in db.ReferenceCategoryNames orderby x.Name descending select x).ToList();

                ViewBag.ReferenceCategoryList = ReferenceCategoryList;
                Detail.Name = cnn.Name;
                db.SaveChanges();
                db.Dispose();
                TempData["Success"] = GeneralFunctions.ReturnSuccess(2);
                return View(Detail);
            }
            catch (Exception)
            {
                TempData["Error"] = GeneralFunctions.ReturnError(2);
                return View(Detail);
            }
        }

        public ActionResult UpdateReferenceCategory(int id)
        {
            var Detail = (from x in db.ReferenceCategoryNames where x.ReferenceCategoryId == id select x).SingleOrDefault();

            return View(Detail);
        }

        [HttpPost]
        public ActionResult UpdateReferenceCategory(int id, ReferenceCategoryNames rcn)
        {
            var Detail = (from x in db.ReferenceCategoryNames where x.ReferenceCategoryId == id select x).SingleOrDefault();
            try
            {
                Detail.Name = rcn.Name;
                db.SaveChanges();
                db.Dispose();
                TempData["Success"] = GeneralFunctions.ReturnSuccess(2);
                return View(Detail);
            }
            catch (Exception)
            {
                TempData["Error"] = GeneralFunctions.ReturnError(2);
                return View(Detail);
            }
        }

        public ActionResult UpdateReferenceCategoryLanguage(int id)
        {
            int LanguageId = int.Parse(Request.QueryString["LangId"]);
            var Detail = (from x in db.ReferenceCategoryLanguages where x.ReferenceCategoryId == id && x.LanguageId == LanguageId select x).SingleOrDefault();
            if (Detail != null)
            {
                ViewBag.Detail = Detail;
                return View();
            }
            else
            {
                return View();
            }

        }

        [HttpPost, ValidateInput(false)]
        public ActionResult UpdateReferenceCategoryLanguage(int id, ReferenceCategoryLanguages blg)
        {
            int LanguageId = int.Parse(Request.QueryString["LangId"]);
            var Detail = (from x in db.ReferenceCategoryLanguages where x.ReferenceCategoryId == id && x.LanguageId == LanguageId select x).SingleOrDefault();
            try
            {

                Detail.Title = blg.Title;

                db.SaveChanges();
                db.Dispose();
                TempData["Success"] = GeneralFunctions.ReturnSuccess(2);
                ViewBag.Detail = Detail;
                return View();
            }
            catch (Exception)
            {
                TempData["Error"] = GeneralFunctions.ReturnError(2);
                ViewBag.Detail = Detail;
                return View();
            }

        }

        public ActionResult Delete(int id)
        {
            var detail = (from x in db.ReferenceNames where x.ReferenceId == id select x).FirstOrDefault();
            var ReferenceLanguagesList = (from x in db.ReferenceLanguages where x.ReferenceId == id select x).ToList();

            if (detail != null)
            {
                db.ReferenceNames.Remove(detail);

                foreach (var item in ReferenceLanguagesList)
                {
                    db.ReferenceLanguages.Remove(item);
                }

                db.SaveChanges();
            }
            db.Dispose();
            return RedirectToAction("ReferenceNames", "Reference");
        }

        public ActionResult DeleteLanguage(int id, int LangId)
        {
            var Detail = (from x in db.ReferenceLanguages where x.ReferenceId == id && x.LanguageId == LangId select x).SingleOrDefault();

            if (Detail != null)
            {
                db.ReferenceLanguages.Remove(Detail);
                db.SaveChanges();
                db.Dispose();
            }

            return RedirectToAction("CreateReferenceLanguage", "Reference", new { id = id });
        }

        public ActionResult DeleteCategoryLanguage(int id, int LangId)
        {
            var Detail = (from x in db.ReferenceCategoryLanguages where x.ReferenceCategoryId == id && x.LanguageId == LangId select x).SingleOrDefault();

            if (Detail != null)
            {
                db.ReferenceCategoryLanguages.Remove(Detail);
                db.SaveChanges();
                db.Dispose();
            }

            return RedirectToAction("CreateReferenceCategoryLanguage", "Reference", new { id = id });
        }

        public JsonResult SortCategory(List<string> Ids)
        {

            try
            {
                for (int i = 1; i < Ids.Count + 1; i++)
                {
                    var id = int.Parse(Ids[i - 1]);
                    var RefCategory = (from x in db.ReferenceCategoryNames where x.ReferenceCategoryId == id select x).SingleOrDefault();
                    RefCategory.OrderNumber = i;
                    db.SaveChanges();
                }
                db.Dispose();
                return Json(new { Code = 200 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { Code = 500 }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult Sort(List<string> Ids)
        {

            try
            {
                for (int i = 1; i < Ids.Count + 1; i++)
                {
                    var id = int.Parse(Ids[i - 1]);
                    var Reference = (from x in db.ReferenceNames where x.ReferenceId == id select x).SingleOrDefault();
                    Reference.OrderNumber = i;
                    db.SaveChanges();
                }
                db.Dispose();
                return Json(new { Code = 200 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { Code = 500 }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ReferenceImages(int id)
        {
            var List = (from x in db.ReferenceLanguages join g in db.ReferenceImageLanguages on x.ReferenceLanguageId equals g.ReferenceLanguageId where x.ReferenceId == id orderby g.OrderNumber ascending select new { x, g }).ToList();

            ViewBag.ReferenceLanguageList = (from x in db.ReferenceLanguages where x.ReferenceId == id select x).ToList();


            var ImageList = new List<ReferenceImageLanguages>();

            foreach (var item in List)
            {
                ImageList.Add(new ReferenceImageLanguages
                {
                    CoverPic = item.g.CoverPic,
                    Description = item.g.Description,
                    ReferenceImageLanguageId = item.g.ReferenceImageLanguageId,
                    ReferenceLanguageId = item.g.ReferenceLanguageId,
                    ImageName = item.g.ImageName,
                    InsertedDate = item.g.InsertedDate,
                    OrderNumber = item.g.OrderNumber,
                    Status = item.g.Status,
                    Title = item.g.Title
                });
            }

            return View(ImageList);
        }

        public ActionResult CreateReferenceImage(int id)
        {
            var ReferenceLanguageList = (from x in db.ReferenceLanguages where x.ReferenceId == id select x).ToList();

            ViewBag.ReferenceLanguageList = ReferenceLanguageList;

            return View();
        }

        [HttpPost]
        public ActionResult CreateReferenceImage(int id, ReferenceImageLanguages ciml, HttpPostedFileBase[] file)
        {
            var ReferenceLanguageList = (from x in db.ReferenceLanguages where x.ReferenceId == id select x).ToList();

            ViewBag.ReferenceLanguageList = ReferenceLanguageList;
            try
            {
                if (file != null)
                {
                    foreach (var item in file)
                    {
                        string image = string.Empty;

                        var extension = item.FileName.Substring(item.FileName.LastIndexOf('.'));
                        image = Guid.NewGuid().ToString() + extension;
                        var imageToResize = System.Drawing.Image.FromStream(item.InputStream);
                        ImageSave.InsertImage(imageToResize, image);


                        var row = new ReferenceImageLanguages
                        {
                            CoverPic = false,
                            Status = 1,
                            Description = "",
                            ImageName = image,
                            Title = "",
                            ReferenceLanguageId = ciml.ReferenceLanguageId,
                            OrderNumber = 0,
                            InsertedDate = DateTime.Now
                        };
                        db.ReferenceImageLanguages.Add(row);
                        db.SaveChanges();
                        TempData["Success"] = GeneralFunctions.ReturnSuccess(1);
                    }
                    db.Dispose();
                }

                return RedirectToAction("CreateReferenceImage", "Reference", new { id = id });
            }
            catch (Exception)
            {
                return RedirectToAction("CreateReferenceImage", "Reference", new { id = id });
            }

        }

        public ActionResult UpdateReferenceImage(int id)
        {
            var Detail = (from x in db.ReferenceImageLanguages where x.ReferenceImageLanguageId == id select x).SingleOrDefault();

            if (Detail != null)
            {
                return View(Detail);
            }
            else
            {
                return View();
            }
        }

        [HttpPost]
        public ActionResult UpdateReferenceImage(int id, ReferenceImageLanguages ciml, HttpPostedFileBase file, bool? Tab)
        {
            var Detail = (from x in db.ReferenceImageLanguages where x.ReferenceImageLanguageId == id select x).SingleOrDefault();
            try
            {
                bool TabControl = false;
                if (Tab != null)
                {
                    TabControl = true;
                }
                string image = string.Empty;
                if (file != null)
                {
                    var extension = file.FileName.Substring(file.FileName.LastIndexOf('.'));
                    image = Guid.NewGuid().ToString() + extension;
                    var imageToResize = System.Drawing.Image.FromStream(file.InputStream);
                    ImageSave.InsertImage(imageToResize, image);
                    Detail.ImageName = image;
                }
                if (TabControl)
                {

                    var ReferenceImageList = (from x in db.ReferenceImageLanguages where x.ReferenceLanguageId == Detail.ReferenceLanguageId select x).ToList();

                    if (ReferenceImageList.Count > 0)
                    {
                        foreach (var item in ReferenceImageList)
                        {
                            item.CoverPic = false;
                            db.SaveChanges();
                        }
                    }
                    Detail.CoverPic = true;
                }

                Detail.Description = ciml.Description;
                Detail.Title = ciml.Title;

                db.SaveChanges();
                db.Dispose();
                TempData["Success"] = GeneralFunctions.ReturnSuccess(2);
                return View(Detail);
            }
            catch (Exception)
            {
                TempData["Error"] = GeneralFunctions.ReturnError(2);
                return View(Detail);
            }

        }

        public ActionResult DeleteReferenceImage(int id)
        {
            var detail = (from x in db.ReferenceImageLanguages where x.ReferenceImageLanguageId == id select x).FirstOrDefault();
            var ReferenceDetail = (from x in db.ReferenceLanguages where x.ReferenceLanguageId == detail.ReferenceLanguageId select x).SingleOrDefault();
            if (detail != null)
            {
                var imagename = detail.ImageName;
                if (System.IO.File.Exists(Server.MapPath("/media/images/" + imagename)))
                {
                    System.IO.File.Delete(Server.MapPath("/media/images/" + imagename));
                }

                if (System.IO.File.Exists(Server.MapPath("/media/images/thumbnails" + imagename)))
                {
                    System.IO.File.Delete(Server.MapPath("/media/images/thumbnails" + imagename));
                }

                if (System.IO.File.Exists(Server.MapPath("/media/images/thumbnails40" + imagename)))
                {
                    System.IO.File.Delete(Server.MapPath("/media/images/thumbnails40" + imagename));
                }
                db.ReferenceImageLanguages.Remove(detail);


                db.SaveChanges();
            }
            db.Dispose();
            return RedirectToAction("ReferenceImages", "Reference", new { id = ReferenceDetail.ReferenceId });
        }

        public JsonResult Imagesort(List<string> Ids)
        {
            try
            {
                for (int i = 1; i < Ids.Count + 1; i++)
                {
                    if (!string.IsNullOrEmpty(Ids[i - 1]))
                    {
                        var id = int.Parse(Ids[i - 1]);

                        var Image = (from x in db.ReferenceImageLanguages where x.ReferenceImageLanguageId == id select x).SingleOrDefault();
                        Image.OrderNumber = i;
                        db.SaveChanges();
                    }

                }
                db.Dispose();
                return Json(new { Code = 200 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { Code = 500 }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ReferenceVideos(int id)
        {
            var List = (from x in db.ReferenceLanguages join g in db.ReferenceVideoLanguages on x.ReferenceLanguageId equals g.ReferenceLanguageId where x.ReferenceId == id orderby g.OrderNumber ascending select new { x, g }).ToList();

            ViewBag.ReferenceLanguageList = (from x in db.ReferenceLanguages where x.ReferenceId == id select x).ToList();


            var VideoList = new List<ReferenceVideoLanguages>();

            foreach (var item in List)
            {
                VideoList.Add(new ReferenceVideoLanguages
                {

                    Description = item.g.Description,
                    ReferenceVideoLanguageId = item.g.ReferenceVideoLanguageId,
                    ReferenceLanguageId = item.g.ReferenceLanguageId,
                    InsertedDate = item.g.InsertedDate,
                    OrderNumber = item.g.OrderNumber,
                    VideoIdentity = item.g.VideoIdentity,
                    VideoType = item.g.VideoType,
                    Status = item.g.Status,
                    Title = item.g.Title
                });
            }

            return View(VideoList);
        }

        public ActionResult CreateReferenceVideo(int id)
        {
            var ReferenceLanguageList = (from x in db.ReferenceLanguages where x.ReferenceId == id select x).ToList();

            ViewBag.ReferenceLanguageList = ReferenceLanguageList;

            return View();
        }

        [HttpPost]
        public ActionResult CreateReferenceVideo(int id, ReferenceVideoLanguages gvl)
        {
            var ReferenceLanguageList = (from x in db.ReferenceLanguages where x.ReferenceId == id select x).ToList();

            ViewBag.ReferenceLanguageList = ReferenceLanguageList;
            try
            {

                string videoidentity = string.Empty;
                if (gvl.VideoType == 1)
                {
                    var VideoUrl = new Uri(gvl.VideoIdentity);
                    NameValueCollection nvc = new NameValueCollection(HttpUtility.ParseQueryString(VideoUrl.Query));
                    var VideoIdentity = nvc["v"];
                    videoidentity = VideoIdentity;
                }
                else
                {
                    var urlssplit = gvl.VideoIdentity.Split('/');

                    videoidentity = urlssplit[urlssplit.Length - 1].ToString();
                }

                var row = new ReferenceVideoLanguages
                {
                    VideoType = gvl.VideoType,
                    Status = 1,
                    Description = gvl.Description,
                    VideoIdentity = videoidentity,
                    Title = gvl.Title,
                    ReferenceLanguageId = gvl.ReferenceLanguageId,
                    OrderNumber = 0,
                    InsertedDate = DateTime.Now
                };
                db.ReferenceVideoLanguages.Add(row);
                db.SaveChanges(); db.Dispose();
                TempData["Success"] = GeneralFunctions.ReturnSuccess(1);

                return RedirectToAction("CreateReferenceVideo", "Reference", new { id = id });
            }
            catch (Exception)
            {
                return RedirectToAction("CreateReferenceVideo", "Reference", new { id = id });
            }

        }

        public ActionResult UpdateReferenceVideo(int id)
        {
            var Detail = (from x in db.ReferenceVideoLanguages where x.ReferenceVideoLanguageId == id select x).SingleOrDefault();

            if (Detail != null)
            {
                return View(Detail);
            }
            else
            {
                return View();
            }
        }

        [HttpPost]
        public ActionResult UpdateReferenceVideo(int id, ReferenceVideoLanguages gvl)
        {
            var Detail = (from x in db.ReferenceVideoLanguages where x.ReferenceVideoLanguageId == id select x).SingleOrDefault();
            try
            {
                string videoidentity = string.Empty;
                if (gvl.VideoType == 1)
                {
                    var VideoUrl = new Uri(gvl.VideoIdentity);
                    NameValueCollection nvc = new NameValueCollection(HttpUtility.ParseQueryString(VideoUrl.Query));
                    var VideoIdentity = nvc["v"];
                    videoidentity = VideoIdentity;
                    Detail.VideoIdentity = videoidentity;
                }
                else
                {
                    var urlssplit = gvl.VideoIdentity.Split('/');
                    videoidentity = urlssplit[urlssplit.Length - 1].ToString();
                    Detail.VideoIdentity = videoidentity;
                }

                Detail.Description = gvl.Description;
                Detail.Title = gvl.Title;
                Detail.VideoType = gvl.VideoType;

                db.SaveChanges();
                db.Dispose();
                TempData["Success"] = GeneralFunctions.ReturnSuccess(2);
                return View(Detail);
            }
            catch (Exception)
            {
                TempData["Error"] = GeneralFunctions.ReturnError(2);
                return View(Detail);
            }

        }

        public ActionResult DeleteReferenceVideo(int id)
        {
            var detail = (from x in db.ReferenceVideoLanguages where x.ReferenceVideoLanguageId == id select x).FirstOrDefault();
            var ReferenceDetail = (from x in db.ReferenceLanguages where x.ReferenceLanguageId == detail.ReferenceLanguageId select x).SingleOrDefault();
            if (detail != null)
            {
                db.ReferenceVideoLanguages.Remove(detail);
                db.SaveChanges();
            }
            db.Dispose();
            return RedirectToAction("ReferenceVideos", "Reference", new { id = ReferenceDetail.ReferenceId });
        }

        public JsonResult Videosort(List<string> Ids)
        {
            try
            {
                for (int i = 1; i < Ids.Count + 1; i++)
                {
                    if (!string.IsNullOrEmpty(Ids[i - 1]))
                    {
                        var id = int.Parse(Ids[i - 1]);

                        var Video = (from x in db.ReferenceVideoLanguages where x.ReferenceVideoLanguageId == id select x).SingleOrDefault();
                        Video.OrderNumber = i;
                        db.SaveChanges();
                    }

                }
                db.Dispose();
                return Json(new { Code = 200 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { Code = 500 }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public JsonResult ChangeStatus(ChangeStatus1 chs)
        {
            try
            {
                var Detail = (from x in db.ReferenceNames where x.ReferenceId == chs.id select x).FirstOrDefault();

                if (chs.Status == 2)
                {
                    Detail.Status = 2;
                }
                else
                {
                    Detail.Status = 1;
                }

                db.SaveChanges();
                db.Dispose();

                return Json(new { Ok = "OK" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { Ok = "BAD" }, JsonRequestBehavior.DenyGet);
            }


        }

        [Serializable]
        public class ChangeStatus1
        {
            public int id { get; set; }
            public int Status { get; set; }
        }

        [HttpPost]
        public JsonResult ChangeStatusCategory(ChangeStatusCategory1 chs)
        {
            try
            {
                var Detail = (from x in db.ReferenceCategoryNames where x.ReferenceCategoryId == chs.id select x).FirstOrDefault();

                if (chs.Status == 2)
                {
                    Detail.Status = 2;
                }
                else
                {
                    Detail.Status = 1;
                }

                db.SaveChanges();
                db.Dispose();

                return Json(new { Ok = "OK" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { Ok = "BAD" }, JsonRequestBehavior.DenyGet);
            }


        }

        [Serializable]
        public class ChangeStatusCategory1
        {
            public int id { get; set; }
            public int Status { get; set; }
        }



    }
}
