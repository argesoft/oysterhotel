﻿using Lissiya_Otel.Functions;
using Lissiya_Otel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Lissiya_Otel.Areas.Admin.Controllers
{
    public class LoginAccountsController : Controller
    {
        //
        // GET: /Admin/LoginAccounts/

        lissiyaEntities db = new lissiyaEntities();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult LoginAccounts()
        {
            var List = new List<LoginAccounts>();
            if (Session["account"] != null)
            {
                LoginAccounts acc = Session["account"] as LoginAccounts;
                if (acc.Role == 2)
                {
                     List = (from x in db.LoginAccounts where x.LoginAccountId == acc.LoginAccountId select x).ToList();
                }
                else
                {
                     List = (from x in db.LoginAccounts select x).ToList();
                }
               
            }
            else
            {
                return RedirectToAction("Dashboard", "Home");
            }

            return View(List);
        }

        public ActionResult UpdateLoginAccount(int id)
        {
            
            var Detail = (from x in db.LoginAccounts where x.LoginAccountId == id select x).SingleOrDefault();

            if (Session["account"] != null )
            {
                LoginAccounts acc = Session["account"] as LoginAccounts;
                if (acc.Role == 1)
                {
                    if (Detail == null)
                    {
                        return RedirectToAction("Dashboard", "Home");
                    }
                }
                else
                {
                    if (Detail != null)
                    {
                        if (Detail.Role == 2)
                        {
                            if (Session["account"] != null)
                            {
                                acc = Session["account"] as LoginAccounts;
                                if (Detail.LoginAccountId != acc.LoginAccountId)
                                {
                                    return RedirectToAction("Dashboard", "Home");
                                }
                            }
                            else
                            {
                                return RedirectToAction("Login", "Home");
                            }
                        }
                        else
                        {
                            if (Session["account"] != null)
                            {
                                acc = Session["account"] as LoginAccounts;
                                if (acc.Role == 2)
                                {
                                    return RedirectToAction("Dashboard", "Home");
                                }
                            }
                            else
                            {
                                return RedirectToAction("Login", "Home");
                            }
                        }
                    }
                    else
                    {
                        return RedirectToAction("Dashboard", "Home");
                    }
                }
            }
            else
            {
                return RedirectToAction("Login", "Home");
            }

           
           
            return View(Detail);
        }

        [HttpPost]
        public ActionResult UpdateLoginAccount(int id, LoginAccounts la)
        {
            var Detail = (from x in db.LoginAccounts where x.LoginAccountId == id select x).SingleOrDefault();

            try
            {
                
                Detail.LoginName = la.LoginName;
                Detail.LoginPassword = la.LoginPassword;
                Detail.NameSurname = la.NameSurname;

                db.SaveChanges();
                db.Dispose();
                TempData["Success"] = GeneralFunctions.ReturnSuccess(2);
                return View(Detail);
            }
            catch (Exception)
            {
                TempData["Error"] = GeneralFunctions.ReturnError(2);

                return View(Detail);
            }

        }

        public ActionResult CreateLoginAccount()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CreateLoginAccount(LoginAccounts la)
        {
            try
            {
                var Detail = (from x in db.LoginAccounts where x.LoginName == la.LoginName select x).SingleOrDefault();

                if (Detail == null)
                {
                    var row = new LoginAccounts
                    {
                        Status = 1,
                        LoginName = la.LoginName,
                        LoginPassword = la.LoginPassword,
                        NameSurname = la.NameSurname,
                        InsertedDate = DateTime.Now,
                        Role = 2
                    };
                    db.LoginAccounts.Add(row);
                    db.SaveChanges();
                    TempData["Success"] = GeneralFunctions.ReturnSuccess(1);
                    return View();
                }
                else
                {
                    TempData["Info"] = GeneralFunctions.ReturnInfo(2);
                    return View();
                }
            }
            catch (Exception)
            {
                TempData["Error"] = GeneralFunctions.ReturnError(1);
                return View();
            }

        }

        public ActionResult Delete(int id)
        {
            var detail = (from x in db.LoginAccounts where x.LoginAccountId == id select x).SingleOrDefault();

            if (detail != null)
            {
                if (detail.Role == 1)
                {
                    TempData["Info"] = GeneralFunctions.ReturnInfo(3);
                }
                else
                {
                    db.LoginAccounts.Remove(detail);
                    db.SaveChanges();
                }
            }
            db.Dispose();
            return RedirectToAction("LoginAccounts", "LoginAccounts");
        }

    }
}
