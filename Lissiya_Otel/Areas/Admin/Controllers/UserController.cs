﻿using Lissiya_Otel.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Lissiya_Otel.Areas.Admin.Controllers
{
    public class UserController : Controller
    {
        lissiyaEntities db = new lissiyaEntities();

        public ActionResult Users()
        {
            var UserList = (from x in db.Users select x).ToList();

            return View(UserList);
        }

        public ActionResult Delete(int id)
        {
            var detail = (from x in db.Users where x.UserId == id select x).FirstOrDefault();


            if (detail != null)
            {
                db.Users.Remove(detail);

                db.SaveChanges();
            }
            db.Dispose();
            return RedirectToAction("Users", "User");
        }


        [HttpPost]
        public JsonResult ChangeStatus(ChangeStatus1 chs)
        {
            try
            {
                var Detail = (from x in db.Users where x.UserId == chs.id select x).FirstOrDefault();

                if (chs.Status == 2)
                {
                    Detail.Status = 2;
                }
                else
                {
                    Detail.Status = 1;
                }

                db.SaveChanges();
                db.Dispose();

                return Json(new { Ok = "OK" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { Ok = "BAD" }, JsonRequestBehavior.DenyGet);
            }


        }

        [Serializable]
        public class ChangeStatus1
        {
            public int id { get; set; }
            public int Status { get; set; }
        }

        public ActionResult ExportData()
        {
            GridView gv = new GridView();

            DataTable dt = new DataTable();
            dt.Columns.Add("E-Posta");
            dt.Columns.Add("Ad Soyad");
            var Users = (from x in db.Users orderby x.InsertedDate ascending select x).ToList();

            for (int i = 0; i < Users.Count; i++)
            {
                string EMail = Users[i].Email;
                string NameSurname = Users[i].NameSurname;

                dt.Rows.Add(EMail,NameSurname);

            }

            gv.DataSource = dt;
            gv.DataBind();
            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment; filename=Users.xls");
            Response.ContentType = "application/ms-excel";
            Response.ContentEncoding = Encoding.GetEncoding("ISO-8859-9");
            Response.Charset = "ISO-8859-9";
            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            gv.RenderControl(htw);
            Response.Output.Write(sw.ToString());
            Response.Flush();
            Response.End();

            return RedirectToAction("Users");
        }
    }
}
