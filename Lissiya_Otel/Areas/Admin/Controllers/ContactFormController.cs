﻿using Lissiya_Otel.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Lissiya_Otel.Areas.Admin.Controllers
{
    public class ContactFormController : Controller
    {
        //
        // GET: /Admin/ContactForm/

        lissiyaEntities db = new lissiyaEntities();

        public ActionResult ContactForms()
        {
            var ContactFormList = (from x in db.ContactForm orderby x.Email ascending select x).ToList();

            return View(ContactFormList);
        }

        public ActionResult Delete(int id)
        {
            var detail = (from x in db.ContactForm where x.ContactFormId == id select x).FirstOrDefault();

            if (detail != null)
            {
                db.ContactForm.Remove(detail);
                db.SaveChanges();
            }
            db.Dispose();
            return RedirectToAction("ContactForms", "ContactForm");
        }

        public ActionResult ExportData()
        {
            GridView gv = new GridView();

            DataTable dt = new DataTable();
            dt.Columns.Add("E-Posta");
            dt.Columns.Add("Ad Soyad");
            dt.Columns.Add("Ip");
            dt.Columns.Add("Mesaj");

            var ContactForms = (from x in db.ContactForm orderby x.InsertedDate ascending select x).ToList();

            for (int i = 0; i < ContactForms.Count; i++)
            {
                string EMail = ContactForms[i].Email;
                string NameSurname = ContactForms[i].NameSurname;
                string Ip = ContactForms[i].Ip;
                string Message = ContactForms[i].Message;


                dt.Rows.Add(EMail, NameSurname, Ip, Message);

            }


            gv.DataSource = dt;
            gv.DataBind();
            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment; filename=ContactForms.xls");
            Response.ContentType = "application/ms-excel";
            Response.ContentEncoding = Encoding.GetEncoding("ISO-8859-9");
            Response.Charset = "ISO-8859-9";
            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            gv.RenderControl(htw);
            Response.Output.Write(sw.ToString());
            Response.Flush();
            Response.End();

            return RedirectToAction("ContactForms");
        }

    }
}
