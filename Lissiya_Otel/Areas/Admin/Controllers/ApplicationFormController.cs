﻿using Lissiya_Otel.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Lissiya_Otel.Areas.Admin.Controllers
{
    public class ApplicationFormController : Controller
    {

        lissiyaEntities db = new lissiyaEntities();

        public ActionResult ApplicationForms()
        {
            var ApplicationFormList = (from x in db.ApplicationForms orderby x.Email ascending select x).ToList();

            return View(ApplicationFormList);
        }

        public ActionResult ApplicationFormDetail(int id)
        {
            var Detail = (from x in db.ApplicationForms where x.ApplicationFormId == id select x).SingleOrDefault();

            return View(Detail);
        }

        public ActionResult Delete(int id)
        {
            var detail = (from x in db.ApplicationForms where x.ApplicationFormId == id select x).FirstOrDefault();

            if (detail != null)
            {
                db.ApplicationForms.Remove(detail);
                db.SaveChanges();
            }
            db.Dispose();
            return RedirectToAction("ApplicationForms", "ApplicationForm");
        }

        public ActionResult ExportData()
        {
            GridView gv = new GridView();

            DataTable dt = new DataTable();
            dt.Columns.Add("E-Posta");
            dt.Columns.Add("Ad Soyad");
            dt.Columns.Add("Telefon");
            dt.Columns.Add("Adres");
            dt.Columns.Add("Mesaj");

            var ApplicationForms = (from x in db.ApplicationForms orderby x.InsertedDate ascending select x).ToList();

            for (int i = 0; i < ApplicationForms.Count; i++)
            {
                string EMail = ApplicationForms[i].Email;
                string NameSurname = ApplicationForms[i].Name + " " + ApplicationForms[i].Surname;
                string Phone = ApplicationForms[i].Phone;
                string Address = ApplicationForms[i].Address;
                string Message = ApplicationForms[i].Message;

                dt.Rows.Add(EMail, NameSurname, Phone, Address, Message);

            }


            gv.DataSource = dt;
            gv.DataBind();
            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment; filename=ApplicationForms.xls");
            Response.ContentType = "application/ms-excel";
            Response.ContentEncoding = Encoding.GetEncoding("ISO-8859-9");
            Response.Charset = "ISO-8859-9";
            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            gv.RenderControl(htw);
            Response.Output.Write(sw.ToString());
            Response.Flush();
            Response.End();

            return RedirectToAction("ApplicationForms");
        }

    }
}
