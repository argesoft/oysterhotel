﻿using Lissiya_Otel.Functions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Lissiya_Otel.Areas.Admin.Controllers
{
    public class SettingController : Controller
    {
        //
        // GET: /Admin/Setting/

        public ActionResult Index()
        {

            ViewBag.SiteTitle = GeneralFunctions.GetSettingValue("SiteTitle");
            ViewBag.ContactMail = GeneralFunctions.GetSettingValue("ContactMail");
            ViewBag.SeoTitle = GeneralFunctions.GetSettingValue("SeoTitle");
            ViewBag.SeoKeywords = GeneralFunctions.GetSettingValue("SeoKeywords");
            ViewBag.SeoDescription = GeneralFunctions.GetSettingValue("SeoDescription");
            ViewBag.FacebookAddress = GeneralFunctions.GetSettingValue("FacebookAddress");
            ViewBag.TwitterAddress = GeneralFunctions.GetSettingValue("TwitterAddress");
            ViewBag.InstagramAddress = GeneralFunctions.GetSettingValue("LinkedinAddress");
            ViewBag.GooglePlusAddress = GeneralFunctions.GetSettingValue("GooglePlusAddress");
            ViewBag.PanelTitle = GeneralFunctions.GetSettingValue("PanelTitle");
            ViewBag.PanelFooterText = GeneralFunctions.GetSettingValue("PanelFooterText");
            ViewBag.GoogleAnalytics = GeneralFunctions.GetSettingValue("GoogleAnalytics");
 
            return View();
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult UpdateSetting(string ContactMail, string SiteTitle, string SeoTitle, string SeoKeywords, string SeoDescription, string FacebookAddress, string TwitterAddress, string LinkedinAddress, string GooglePlusAddress, string PanelTitle, string PanelFooterText, HttpPostedFileBase filepanellogo, string GoogleAnalytics)
        {
            try
            {
                GeneralFunctions.SetSettingValue("SiteTitle", SiteTitle);
                GeneralFunctions.SetSettingValue("ContactMail", ContactMail);
                GeneralFunctions.SetSettingValue("SeoTitle", SeoTitle);
                GeneralFunctions.SetSettingValue("SeoKeywords", SeoKeywords);
                GeneralFunctions.SetSettingValue("SeoDescription", SeoDescription);
                GeneralFunctions.SetSettingValue("FacebookAddress", FacebookAddress);
                GeneralFunctions.SetSettingValue("TwitterAddress", TwitterAddress);
                GeneralFunctions.SetSettingValue("LinkedinAddress", LinkedinAddress);
                GeneralFunctions.SetSettingValue("GooglePlusAddress", GooglePlusAddress);
                GeneralFunctions.SetSettingValue("PanelTitle", PanelTitle);
                GeneralFunctions.SetSettingValue("PanelFooterText", PanelFooterText);
                GeneralFunctions.SetSettingValue("GoogleAnalytics", GoogleAnalytics);

                if (filepanellogo != null)
                {
                    string Panellogo = string.Empty;
                    var extension = filepanellogo.FileName.Substring(filepanellogo.FileName.LastIndexOf('.'));
                    Panellogo = Guid.NewGuid().ToString() + extension;
                    var imageToResize = System.Drawing.Image.FromStream(filepanellogo.InputStream);
                    ImageSave.InsertImage(imageToResize, Panellogo);
                    GeneralFunctions.SetSettingValue("PanelLogo", Panellogo);
                }

                TempData["Success"] = GeneralFunctions.ReturnSuccess(2);
                return RedirectToAction("Index", "Setting");

            }
            catch (Exception)
            {
                TempData["Error"] = GeneralFunctions.ReturnError(2);
                return RedirectToAction("Index", "Setting");
            }


        }

    }
}
