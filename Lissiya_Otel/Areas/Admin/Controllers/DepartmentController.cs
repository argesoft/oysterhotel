﻿using Lissiya_Otel.Functions;
using Lissiya_Otel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Lissiya_Otel.Areas.Admin.Controllers
{
    public class DepartmentController : Controller
    {
        lissiyaEntities db = new lissiyaEntities();

        public ActionResult DepartmentNames()
        {
            var DepartmentNameList = (from x in db.DepartmentNames orderby x.OrderNumber ascending select x).ToList();

            ViewBag.LanguageList = LanguageController.Languages();

            return View(DepartmentNameList);
        }

        public ActionResult CreateDepartmentNames()
        {
            return View();
        }

        public ActionResult CreateDepartmentLanguage()
        {
            var LangaugeList = LanguageController.Languages();
            if (LangaugeList.Count > 0)
            {
                ViewBag.LanguageList = LangaugeList;
            }
            return View();
        }

        [HttpPost]
        public ActionResult CreateDepartmentNames(DepartmentNames dn)
        {
            try
            {
                var row = new DepartmentNames
                {
                    Status = 1,
                    Name = dn.Name,
                    OrderNumber = 0,
                    InsertedDate = DateTime.Now
                };
                db.DepartmentNames.Add(row);
                db.SaveChanges();
                TempData["Success"] = GeneralFunctions.ReturnSuccess(1);

                return RedirectToAction("CreateDepartmentLanguage", "Department", new { id = row.DepartmentId });
            }
            catch (Exception)
            {
                TempData["Error"] = GeneralFunctions.ReturnError(1);
                return View();
            }

        }

        [HttpPost]
        public ActionResult CreateDepartmentLanguage(DepartmentLanguages adl, string updatecontrol, HttpPostedFileBase file)
        {
            var LangaugeList = LanguageController.Languages();
            if (LangaugeList.Count > 0)
            {
                ViewBag.LanguageList = LangaugeList;
            }
            try
            {

                if (adl.LanguageId != null)
                {
                    var Detail = (from x in db.DepartmentLanguages where x.DepartmentId == adl.DepartmentId && x.LanguageId == adl.LanguageId select x).SingleOrDefault();

                    if (Detail == null)
                    {
                        string logo = string.Empty;
                        if (file != null)
                        {
                            var extension = file.FileName.Substring(file.FileName.LastIndexOf('.'));
                            logo = Guid.NewGuid().ToString() + extension;
                            var imageToResize = System.Drawing.Image.FromStream(file.InputStream);
                            ImageSave.InsertImage(imageToResize, logo);
                        }
                        var row = new DepartmentLanguages
                        {

                            DepartmentId = adl.DepartmentId,
                            InsertedDate = DateTime.Now,
                            LanguageId = adl.LanguageId,
                            OrderNumber = 0,
                            Status = 1,
                            Title = adl.Title,
                            ImageName = logo,
                            Summary = adl.Summary
                        };

                        db.DepartmentLanguages.Add(row);
                        db.SaveChanges();
                        db.Dispose();
                        TempData["Success"] = GeneralFunctions.ReturnSuccess(1);
                        if (!string.IsNullOrEmpty(updatecontrol))
                        {
                            return RedirectToAction("UpdateDepartmentLanguage", "Department", new { id = row.DepartmentId, LangId = row.LanguageId });
                        }
                        else
                        {
                            return RedirectToAction("CreateDepartmentLanguage", "Department", new { id = row.DepartmentId });
                        }

                    }
                    else
                    {
                        TempData["Info"] = GeneralFunctions.ReturnInfo(1);
                        if (!string.IsNullOrEmpty(updatecontrol))
                        {
                            return RedirectToAction("UpdateDepartmentLanguage", "Department", new { id = adl.DepartmentId, LangId = adl.LanguageId });
                        }
                        else
                        {
                            return RedirectToAction("CreateDepartmentLanguage", "Department", new { id = adl.DepartmentId });
                        }
                    }
                }
                else
                {
                    TempData["Error"] = GeneralFunctions.ReturnError(4);
                    if (!string.IsNullOrEmpty(updatecontrol))
                    {
                        return RedirectToAction("UpdateDepartmentLanguage", "Department", new { id = adl.DepartmentId, LangId = adl.LanguageId });
                    }
                    else
                    {
                        return RedirectToAction("CreateDepartmentLanguage", "Department", new { id = adl.DepartmentId });
                    }
                }

            }
            catch (Exception)
            {
                TempData["Error"] = GeneralFunctions.ReturnError(1);
                if (!string.IsNullOrEmpty(updatecontrol))
                {
                    return RedirectToAction("UpdateDepartmentLanguage", "Department", new { id = adl.DepartmentId, LangId = adl.LanguageId });
                }
                else
                {
                    return RedirectToAction("CreateDepartmentLanguage", "Department", new { id = adl.DepartmentId });
                }

            }
        }

        public ActionResult UpdateDepartmentLanguage(int id)
        {
            int LanguageId = int.Parse(Request.QueryString["LangId"]);
            var Detail = (from x in db.DepartmentLanguages where x.DepartmentId == id && x.LanguageId == LanguageId select x).SingleOrDefault();
            if (Detail != null)
            {
                ViewBag.Detail = Detail;
                return View();
            }
            else
            {
                return View();
            }

        }

        [HttpPost]
        public ActionResult UpdateDepartmentLanguage(int id, DepartmentLanguages adl, HttpPostedFileBase file)
        {
            int LanguageId = int.Parse(Request.QueryString["LangId"]);
            var Detail = (from x in db.DepartmentLanguages where x.DepartmentId == id && x.LanguageId == LanguageId select x).SingleOrDefault();
            try
            {
                string logo = string.Empty;
                if (file != null)
                {
                    var extension = file.FileName.Substring(file.FileName.LastIndexOf('.'));
                    logo = Guid.NewGuid().ToString() + extension;
                    var imageToResize = System.Drawing.Image.FromStream(file.InputStream);
                    ImageSave.InsertImage(imageToResize, logo);
                    Detail.ImageName = logo;
                }

                Detail.Summary = adl.Summary;
                Detail.Title = adl.Title;

                db.SaveChanges();
                db.Dispose();
                TempData["Success"] = GeneralFunctions.ReturnSuccess(2);
                ViewBag.Detail = Detail;
                return View();
            }
            catch (Exception)
            {
                TempData["Error"] = GeneralFunctions.ReturnError(2);
                ViewBag.Detail = Detail;
                return View();
            }
        }

        public ActionResult UpdateDepartmentNames(int id)
        {
            var Detail = (from x in db.DepartmentNames where x.DepartmentId == id select x).SingleOrDefault();

            return View(Detail);
        }

        [HttpPost]
        public ActionResult UpdateDepartmentNames(int id, DepartmentNames adn)
        {
            var Detail = (from x in db.DepartmentNames where x.DepartmentId == id select x).SingleOrDefault();
            try
            {

                Detail.Name = adn.Name;

                db.SaveChanges();
                db.Dispose();
                TempData["Success"] = GeneralFunctions.ReturnSuccess(2);
                return View(Detail);
            }
            catch (Exception)
            {
                TempData["Error"] = GeneralFunctions.ReturnError(2);
                return View(Detail);
            }


        }

        public ActionResult Delete(int id)
        {
            var detail = (from x in db.DepartmentNames where x.DepartmentId == id select x).FirstOrDefault();
            var DepartmentLanguagesList = (from x in db.DepartmentLanguages where x.DepartmentId == id select x).ToList();

            if (detail != null)
            {
                db.DepartmentNames.Remove(detail);

                foreach (var item in DepartmentLanguagesList)
                {
                    var imagename = item.ImageName;
                    if (System.IO.File.Exists(Server.MapPath("/media/images/" + imagename)))
                    {
                        System.IO.File.Delete(Server.MapPath("/media/images/" + imagename));
                    }

                    if (System.IO.File.Exists(Server.MapPath("/media/images/thumbnails" + imagename)))
                    {
                        System.IO.File.Delete(Server.MapPath("/media/images/thumbnails" + imagename));
                    }

                    if (System.IO.File.Exists(Server.MapPath("/media/images/thumbnails40" + imagename)))
                    {
                        System.IO.File.Delete(Server.MapPath("/media/images/thumbnails40" + imagename));
                    }

                    db.DepartmentLanguages.Remove(item);

                }

                db.SaveChanges();
            }
            db.Dispose();
            return RedirectToAction("DepartmentNames", "Department");
        }

        public ActionResult DeleteLanguage(int id, int LangId)
        {
            var Detail = (from x in db.DepartmentLanguages where x.DepartmentId == id && x.LanguageId == LangId select x).SingleOrDefault();

            if (Detail != null)
            {
                db.DepartmentLanguages.Remove(Detail);
                db.SaveChanges();
                db.Dispose();
            }

            return RedirectToAction("CreateDepartmentLanguage", "Department", new { id = id });
        }

        public JsonResult Sort(List<string> Ids)
        {

            try
            {
                for (int i = 1; i < Ids.Count + 1; i++)
                {
                    var id = int.Parse(Ids[i - 1]);
                    var Department = (from x in db.DepartmentNames where x.DepartmentId == id select x).SingleOrDefault();
                    Department.OrderNumber = i;
                    db.SaveChanges();
                }
                db.Dispose();
                return Json(new { Code = 200 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { Code = 500 }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult ChangeStatus(ChangeStatus1 chs)
        {
            try
            {
                var Detail = (from x in db.DepartmentNames where x.DepartmentId == chs.id select x).SingleOrDefault();

                if (chs.Status == 2)
                {
                    Detail.Status = 2;
                }
                else
                {
                    Detail.Status = 1;
                }

                db.SaveChanges();
                db.Dispose();

                return Json(new { Ok = "OK" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { Ok = "BAD" }, JsonRequestBehavior.DenyGet);
            }


        }

        [Serializable]
        public class ChangeStatus1
        {
            public int id { get; set; }
            public int Status { get; set; }
        }

        public ActionResult MemberNames()
        {
            var MemberNameList = (from x in db.MemberNames orderby x.OrderNumber ascending select x).ToList();

            ViewBag.LanguageList = LanguageController.Languages();

            return View(MemberNameList);
        }

        public ActionResult CreateMemberNames()
        {
            ViewBag.DepartmentList = (from x in db.DepartmentNames orderby x.Name ascending select x).ToList();

            return View();
        }

        public ActionResult CreateMemberLanguage()
        {
            var LangaugeList = LanguageController.Languages();
            if (LangaugeList.Count > 0)
            {
                ViewBag.LanguageList = LangaugeList;
            }
            return View();
        }

        [HttpPost]
        public ActionResult CreateMemberNames(MemberNames dn)
        {
            try
            {
                var row = new MemberNames
                {
                    Status = 1,
                    Name = dn.Name,
                    OrderNumber = 0,
                    DepartmentId = dn.DepartmentId,
                    InsertedDate = DateTime.Now
                };
                db.MemberNames.Add(row);
                db.SaveChanges();
                TempData["Success"] = GeneralFunctions.ReturnSuccess(1);

                return RedirectToAction("CreateMemberLanguage", "Department", new { id = row.MemberId });
            }
            catch (Exception)
            {
                TempData["Error"] = GeneralFunctions.ReturnError(1);
                return View();
            }

        }

        [HttpPost]
        public ActionResult CreateMemberLanguage(MemberLanguages adl, string updatecontrol, HttpPostedFileBase file)
        {
            var LangaugeList = LanguageController.Languages();
            if (LangaugeList.Count > 0)
            {
                ViewBag.LanguageList = LangaugeList;
            }
            try
            {

                if (adl.LanguageId != null)
                {
                    var Detail = (from x in db.MemberLanguages where x.MemberId == adl.MemberId && x.LanguageId == adl.LanguageId select x).SingleOrDefault();

                    if (Detail == null)
                    {
                        string logo = string.Empty;
                        if (file != null)
                        {
                            var extension = file.FileName.Substring(file.FileName.LastIndexOf('.'));
                            logo = Guid.NewGuid().ToString() + extension;
                            var imageToResize = System.Drawing.Image.FromStream(file.InputStream);
                            ImageSave.InsertImage(imageToResize, logo);
                        }
                        var row = new MemberLanguages
                        {

                            MemberId = adl.MemberId,
                            InsertedDate = DateTime.Now,
                            LanguageId = adl.LanguageId,
                            OrderNumber = 0,
                            Status = 1,
                            Title = adl.Title,
                            ImageName = logo,
                            Summary = adl.Summary,
                            Email = adl.Email,
                            Name = adl.Name,
                            Surname = adl.Surname,
                            

                        };

                        db.MemberLanguages.Add(row);
                        db.SaveChanges();
                        db.Dispose();
                        TempData["Success"] = GeneralFunctions.ReturnSuccess(1);
                        if (!string.IsNullOrEmpty(updatecontrol))
                        {
                            return RedirectToAction("UpdateMemberLanguage", "Department", new { id = row.MemberId, LangId = row.LanguageId });
                        }
                        else
                        {
                            return RedirectToAction("CreateMemberLanguage", "Department", new { id = row.MemberId });
                        }

                    }
                    else
                    {
                        TempData["Info"] = GeneralFunctions.ReturnInfo(1);
                        if (!string.IsNullOrEmpty(updatecontrol))
                        {
                            return RedirectToAction("UpdateMemberLanguage", "Department", new { id = adl.MemberId, LangId = adl.LanguageId });
                        }
                        else
                        {
                            return RedirectToAction("CreateMemberLanguage", "Department", new { id = adl.MemberId });
                        }
                    }
                }
                else
                {
                    TempData["Error"] = GeneralFunctions.ReturnError(4);
                    if (!string.IsNullOrEmpty(updatecontrol))
                    {
                        return RedirectToAction("UpdateMemberLanguage", "Department", new { id = adl.MemberId, LangId = adl.LanguageId });
                    }
                    else
                    {
                        return RedirectToAction("CreateMemberLanguage", "Department", new { id = adl.MemberId });
                    }
                }

            }
            catch (Exception)
            {
                TempData["Error"] = GeneralFunctions.ReturnError(1);
                if (!string.IsNullOrEmpty(updatecontrol))
                {
                    return RedirectToAction("UpdateMemberLanguage", "Department", new { id = adl.MemberId, LangId = adl.LanguageId });
                }
                else
                {
                    return RedirectToAction("CreateMemberLanguage", "Department", new { id = adl.MemberId });
                }

            }
        }

        public ActionResult UpdateMemberLanguage(int id)
        {
            int LanguageId = int.Parse(Request.QueryString["LangId"]);
            var Detail = (from x in db.MemberLanguages where x.MemberId == id && x.LanguageId == LanguageId select x).SingleOrDefault();
            if (Detail != null)
            {
                ViewBag.Detail = Detail;
                return View();
            }
            else
            {
                return View();
            }

        }

        [HttpPost]
        public ActionResult UpdateMemberLanguage(int id, MemberLanguages adl, HttpPostedFileBase file)
        {
            int LanguageId = int.Parse(Request.QueryString["LangId"]);
            var Detail = (from x in db.MemberLanguages where x.MemberId == id && x.LanguageId == LanguageId select x).SingleOrDefault();
            try
            {
                string logo = string.Empty;
                if (file != null)
                {
                    var extension = file.FileName.Substring(file.FileName.LastIndexOf('.'));
                    logo = Guid.NewGuid().ToString() + extension;
                    var imageToResize = System.Drawing.Image.FromStream(file.InputStream);
                    ImageSave.InsertImage(imageToResize, logo);
                    Detail.ImageName = logo;
                }

                Detail.Summary = adl.Summary;
                Detail.Title = adl.Title;

                db.SaveChanges();
                db.Dispose();
                TempData["Success"] = GeneralFunctions.ReturnSuccess(2);
                ViewBag.Detail = Detail;
                return View();
            }
            catch (Exception)
            {
                TempData["Error"] = GeneralFunctions.ReturnError(2);
                ViewBag.Detail = Detail;
                return View();
            }
        }

        public ActionResult UpdateMemberNames(int id)
        {
            var Detail = (from x in db.MemberNames where x.MemberId == id select x).SingleOrDefault();

            return View(Detail);
        }

        [HttpPost]
        public ActionResult UpdateMemberNames(int id, MemberNames adn)
        {
            var Detail = (from x in db.MemberNames where x.MemberId == id select x).SingleOrDefault();
            try
            {

                Detail.Name = adn.Name;

                db.SaveChanges();
                db.Dispose();
                TempData["Success"] = GeneralFunctions.ReturnSuccess(2);
                return View(Detail);
            }
            catch (Exception)
            {
                TempData["Error"] = GeneralFunctions.ReturnError(2);
                return View(Detail);
            }


        }

        public ActionResult DeleteMember(int id)
        {
            var detail = (from x in db.MemberNames where x.MemberId == id select x).FirstOrDefault();
            var MemberLanguagesList = (from x in db.MemberLanguages where x.MemberId == id select x).ToList();

            if (detail != null)
            {
                db.MemberNames.Remove(detail);

                foreach (var item in MemberLanguagesList)
                {
                    var imagename = item.ImageName;
                    if (System.IO.File.Exists(Server.MapPath("/media/images/" + imagename)))
                    {
                        System.IO.File.Delete(Server.MapPath("/media/images/" + imagename));
                    }

                    db.MemberLanguages.Remove(item);

                }

                db.SaveChanges();
            }
            db.Dispose();
            return RedirectToAction("MemberNames", "Department");
        }

        public ActionResult DeleteMemberLanguage(int id, int LangId)
        {
            var Detail = (from x in db.MemberLanguages where x.MemberId == id && x.LanguageId == LangId select x).SingleOrDefault();

            if (Detail != null)
            {
                db.MemberLanguages.Remove(Detail);
                db.SaveChanges();
                db.Dispose();
            }

            return RedirectToAction("CreateMemberLanguage", "Department", new { id = id });
        }

        public JsonResult MemberSort(List<string> Ids)
        {

            try
            {
                for (int i = 1; i < Ids.Count + 1; i++)
                {
                    var id = int.Parse(Ids[i - 1]);
                    var Member = (from x in db.MemberNames where x.MemberId == id select x).SingleOrDefault();
                    Member.OrderNumber = i;
                    db.SaveChanges();
                }
                db.Dispose();
                return Json(new { Code = 200 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { Code = 500 }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult ChangeMemberStatus(ChangeStatus2 chs)
        {
            try
            {
                var Detail = (from x in db.MemberNames where x.MemberId == chs.id select x).SingleOrDefault();

                if (chs.Status == 2)
                {
                    Detail.Status = 2;
                }
                else
                {
                    Detail.Status = 1;
                }

                db.SaveChanges();
                db.Dispose();

                return Json(new { Ok = "OK" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { Ok = "BAD" }, JsonRequestBehavior.DenyGet);
            }


        }

        [Serializable]
        public class ChangeStatus2
        {
            public int id { get; set; }
            public int Status { get; set; }
        }



    }
}
