﻿using Lissiya_Otel.Functions;
using Lissiya_Otel.Models;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Lissiya_Otel.Areas.Admin.Controllers
{
    public class GalleryController : Controller
    {
        lissiyaEntities db = new lissiyaEntities();

        public ActionResult GalleryNames()
        {
            var GalleryNameList = (from x in db.GalleryNames orderby x.OrderNumber ascending select x).ToList();

            var MenuList = (from x in db.MenuNames where x.TopMenuId==31 orderby x.MenuId ascending select x).ToList();
            ViewBag.MenuList = MenuList;//

            ViewBag.LanguageList = LanguageController.Languages();//

            return View(GalleryNameList);
        }

        public ActionResult CreateGalleryNames()
        {
            var MenuList = (from x in db.MenuNames where x.TopMenuId == 31 orderby x.MenuId ascending select x).ToList();
            ViewBag.MenuList = MenuList;
            return View();
        }

        public ActionResult CreateGalleryLanguage()
        {
            var MenuList = (from x in db.MenuNames where x.TopMenuId == 31 orderby x.MenuId ascending select x).ToList();
            ViewBag.MenuList = MenuList;
            var LangaugeList = LanguageController.Languages();
            if (LangaugeList.Count > 0)
            {
                ViewBag.LanguageList = LangaugeList;
            }
            return View();
        }

        [HttpPost]
        public ActionResult CreateGalleryNames(GalleryNames gn, int menuId)
        {
            var MenuList = (from x in db.MenuNames where x.TopMenuId == 31 orderby x.MenuId ascending select x).ToList();
            ViewBag.MenuList = MenuList;
            try
            {

                var row = new GalleryNames
                {
                    Status = 1,
                    Name = gn.Name,
                    OrderNumber = 0,
                    InsertedDate = DateTime.Now,
                    MenuId=menuId
                    
                };
                db.GalleryNames.Add(row);
                db.SaveChanges();
                GalleryNames galeri = (from g in db.GalleryNames where g.MenuId == menuId select g).FirstOrDefault();
                string urlTr = "/Galeri/"+galeri.GalleryId;
                string urlEng = "/Gallery/" + galeri.GalleryId;
                var Detail = (from c in db.MenuLanguages where c.MenuId == menuId && c.LanguageId==1 select c).First();
                Detail.Url = urlTr;
                var Detail2 = (from c in db.MenuLanguages where c.MenuId == menuId && c.LanguageId == 2 select c).First();
                Detail2.Url = urlEng;
                db.SaveChanges();
               
                TempData["Success"] = GeneralFunctions.ReturnSuccess(1);

                return RedirectToAction("CreateGalleryLanguage", "Gallery", new { id = row.GalleryId });
            }
            catch (Exception)
            {
                TempData["Error"] = GeneralFunctions.ReturnError(1);
                return View();
            }

        }

        [HttpPost, ValidateInput(false)]
        public ActionResult CreateGalleryLanguage(GalleryLanguages clg, string updatecontrol)
        {
            var MenuList = (from x in db.MenuNames where x.TopMenuId == 31 orderby x.MenuId ascending select x).ToList();
            ViewBag.MenuList = MenuList;
            var LangaugeList = LanguageController.Languages();
            if (LangaugeList.Count > 0)
            {
                ViewBag.LanguageList = LangaugeList;
            }
            try
            {
                if (clg.LanguageId != null)
                {
                    var Detail = (from x in db.GalleryLanguages where x.GalleryId == clg.GalleryId && x.LanguageId == clg.LanguageId select x).SingleOrDefault();

                    if (Detail == null)
                    {
                        var row = new GalleryLanguages
                   {
                       InsertedDate = DateTime.Now,
                       LanguageId = clg.LanguageId,
                       OrderNumber = 0,
                       GalleryId = clg.GalleryId,
                       Title = clg.Title,
                       Status = 1,


                   };

                        db.GalleryLanguages.Add(row);
                        db.SaveChanges();
                        db.Dispose();
                        TempData["Success"] = GeneralFunctions.ReturnSuccess(1);
                        if (!string.IsNullOrEmpty(updatecontrol))
                        {
                            return RedirectToAction("UpdateGalleryLanguage", "Gallery", new { id = row.GalleryId, LangId = row.LanguageId });
                        }
                        else
                        {
                            return RedirectToAction("CreateGalleryLanguage", "Gallery", new { id = row.GalleryId });
                        }
                    }
                    else
                    {
                        TempData["Info"] = GeneralFunctions.ReturnInfo(1);
                        if (!string.IsNullOrEmpty(updatecontrol))
                        {
                            return RedirectToAction("UpdateGalleryLanguage", "Gallery", new { id = clg.GalleryId, LangId = clg.LanguageId });
                        }
                        else
                        {
                            return RedirectToAction("CreateGalleryLanguage", "Gallery", new { id = clg.GalleryId });
                        }
                    }

                }
                else
                {
                    TempData["Error"] = GeneralFunctions.ReturnError(4);
                    if (!string.IsNullOrEmpty(updatecontrol))
                    {
                        return RedirectToAction("UpdateGalleryLanguage", "Gallery", new { id = clg.GalleryId, LangId = clg.LanguageId });
                    }
                    else
                    {
                        return RedirectToAction("CreateGalleryLanguage", "Gallery", new { id = clg.GalleryId });
                    }

                }

            }
            catch (Exception)
            {
                TempData["Error"] = GeneralFunctions.ReturnError(1);
                if (!string.IsNullOrEmpty(updatecontrol))
                {
                    return RedirectToAction("UpdateGalleryLanguage", "Gallery", new { id = clg.GalleryId, LangId = clg.LanguageId });
                }
                else
                {
                    return RedirectToAction("CreateGalleryLanguage", "Gallery", new { id = clg.GalleryId });
                }

            }
        }

        public ActionResult UpdateGalleryLanguage(int id)
        {
            var MenuList = (from x in db.MenuNames where x.TopMenuId == 31 orderby x.MenuId ascending select x).ToList();
            ViewBag.MenuList = MenuList;
            int LanguageId = int.Parse(Request.QueryString["LangId"]);
            var Detail = (from x in db.GalleryLanguages where x.GalleryId == id && x.LanguageId == LanguageId select x).SingleOrDefault();
            if (Detail != null)
            {
                ViewBag.Detail = Detail;
                return View();
            }
            else
            {
                return View();
            }

        }

        [HttpPost, ValidateInput(false)]
        public ActionResult UpdateGalleryLanguage(int id, GalleryLanguages blg)
        {
            var MenuList = (from x in db.MenuNames where x.TopMenuId == 31 orderby x.MenuId ascending select x).ToList();
            ViewBag.MenuList = MenuList;
            int LanguageId = int.Parse(Request.QueryString["LangId"]);
            var Detail = (from x in db.GalleryLanguages where x.GalleryId == id && x.LanguageId == LanguageId select x).SingleOrDefault();
            try
            {
                Detail.Title = blg.Title;

                db.SaveChanges();
                db.Dispose();
                TempData["Success"] = GeneralFunctions.ReturnSuccess(2);
                ViewBag.Detail = Detail;
                return View();
            }
            catch (Exception)
            {
                TempData["Error"] = GeneralFunctions.ReturnError(2);
                ViewBag.Detail = Detail;
                return View();
            }

        }

        public ActionResult UpdateGalleryNames(int id)
        {
            var MenuList = (from x in db.MenuNames where x.TopMenuId == 31 orderby x.MenuId ascending select x).ToList();
            ViewBag.MenuList = MenuList;
            var Detail = (from x in db.GalleryNames where x.GalleryId == id select x).SingleOrDefault();
            return View(Detail);
        }

        [HttpPost]
        public ActionResult UpdateGalleryNames(int id, GalleryNames cnn,int menuId)
        {
            var MenuList = (from x in db.MenuNames where x.TopMenuId == 31 orderby x.MenuId ascending select x).ToList();
            ViewBag.MenuList = MenuList;
            var Detail = (from x in db.GalleryNames where x.GalleryId == id select x).SingleOrDefault();
            try
            {
                Detail.Name = cnn.Name;
                GalleryNames galeri = (from g in db.GalleryNames where g.MenuId == menuId select g).FirstOrDefault();
                string urlTr = "/Galeri/" + galeri.GalleryId;
                string urlEng = "/Gallery/" + galeri.GalleryId;
                var Detail3 = (from c in db.MenuLanguages where c.MenuId == menuId && c.LanguageId == 1 select c).First();
                Detail3.Url = urlTr;
                var Detail2 = (from c in db.MenuLanguages where c.MenuId == menuId && c.LanguageId == 2 select c).First();
                Detail2.Url = urlEng;
                db.SaveChanges();
                db.Dispose();
                TempData["Success"] = GeneralFunctions.ReturnSuccess(2);
                return View(Detail);
            }
            catch (Exception)
            {
                TempData["Error"] = GeneralFunctions.ReturnError(2);
                return View(Detail);
            }


        }

        public ActionResult Delete(int id)
        {
            var MenuList = (from x in db.MenuNames where x.TopMenuId == 31 orderby x.MenuId ascending select x).ToList();
            ViewBag.MenuList = MenuList;
            var detail = (from x in db.GalleryNames where x.GalleryId == id select x).FirstOrDefault();
            var GalleryLanguagesList = (from x in db.GalleryLanguages where x.GalleryId == id select x).ToList();
            var imageList = (from g in db.GalleryImages where g.GalleryId == id select g).ToList();
            if (detail != null)
            {
                db.GalleryNames.Remove(detail);

                foreach (var item in GalleryLanguagesList)
                {
                    db.GalleryLanguages.Remove(item);
                }
                foreach (var item in imageList)
                {
                    db.GalleryImages.Remove(item);
                }
                db.SaveChanges();
            }
            db.Dispose();
            return RedirectToAction("GalleryNames", "Gallery");
        }

        public ActionResult DeleteLanguage(int id, int LangId)
        {
            var MenuList = (from x in db.MenuNames where x.TopMenuId == 31 orderby x.MenuId ascending select x).ToList();
            ViewBag.MenuList = MenuList;
            var Detail = (from x in db.GalleryLanguages where x.GalleryId == id && x.LanguageId == LangId select x).SingleOrDefault();

            if (Detail != null)
            {
                db.GalleryLanguages.Remove(Detail);
                db.SaveChanges();
                db.Dispose();
            }

            return RedirectToAction("CreateGalleryLanguage", "Gallery", new { id = id });
        }

        public JsonResult Sort(List<string> Ids)
        {

            try
            {
                for (int i = 1; i < Ids.Count + 1; i++)
                {
                    var id = int.Parse(Ids[i - 1]);
                    var Gallery = (from x in db.GalleryNames where x.GalleryId == id select x).SingleOrDefault();
                    Gallery.OrderNumber = i;
                    db.SaveChanges();
                }
                db.Dispose();
                return Json(new { Code = 200 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { Code = 500 }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GalleryImages(int id)
        {
            var MenuList = (from x in db.MenuNames where x.TopMenuId == 31 orderby x.MenuId ascending select x).ToList();
            ViewBag.MenuList = MenuList;
            ViewBag.LanguageList = LanguageController.Languages();

            var List = (from x in db.GalleryImages join i in db.Images on x.ImageId equals i.ImageId where x.GalleryId == id select new { x, i }).ToList();

            var ImageList = new List<GalleryImage>();

            foreach (var item in List)
            {
                ImageList.Add(new GalleryImage
                {
                    Description = item.i.Description,
                    CoverPic = item.x.CoverPic.Value,
                    GalleryId = item.x.GalleryId.Value,
                    LanguageId = item.x.LanguageId.Value,
                    OrderNumber = item.x.OrderNumber.Value,
                    Status = item.x.Status.Value,
                    ImageId = item.i.ImageId,
                    ImageName = item.i.ImageName,
                    InsertedDate = item.i.InsertedDate.Value,
                    Title = item.i.Title,
                    GalleryImageId = item.x.GalleryImageId
                });
            }

            return View(ImageList);
        }

        public ActionResult CreateGalleryImage()
        {
            var MenuList = (from x in db.MenuNames where x.TopMenuId == 31 orderby x.MenuId ascending select x).ToList();
            ViewBag.MenuList = MenuList;
            var GalleryList = (from x in db.GalleryNames orderby x.Name ascending select x).ToList();

            ViewBag.GalleryList = GalleryList;

            ViewBag.LanguageList = LanguageController.Languages();

            return View();
        }

        [HttpPost]
        public JsonResult CreateGalleryImage(string GalleryIds, string LanguageIds, HttpPostedFileBase file)
        {
            var MenuList = (from x in db.MenuNames where x.TopMenuId == 31 orderby x.MenuId ascending select x).ToList();
            ViewBag.MenuList = MenuList;
            string result = "";
            List<string> filesaveerrors = new List<string>();
            try
            {
                if (file != null)
                {
                    string image = string.Empty;

                    var extension = file.FileName.Substring(file.FileName.LastIndexOf('.'));
                    image = Guid.NewGuid().ToString() + extension;
                    var imageToResize = System.Drawing.Image.FromStream(file.InputStream);
                    ImageSave.InsertImage(imageToResize, image);

                    var row = new Images
                    {
                        Description = "",
                        ImageName = image,
                        InsertedDate = DateTime.Now,
                        Title = ""
                    };

                    db.Images.Add(row);
                    db.SaveChanges();
                    
                    GalleryIds = GalleryIds.Substring(0, GalleryIds.Length - 1);
                    string[] GalleryIdArray = GalleryIds.Split(',');

                    LanguageIds = LanguageIds.Substring(0, LanguageIds.Length - 1);
                    string[] LanguageArray = LanguageIds.Split(',');


                    for (int i = 0; i < GalleryIdArray.Length; i++)
                    {
                        for (int k = 0; k < LanguageArray.Length; k++)
                        {
                            var grow = new GalleryImages
                            {
                                GalleryId = int.Parse(GalleryIdArray[i].ToString()),
                                ImageId = row.ImageId,
                                InsertedDate = DateTime.Now,
                                CoverPic = false,
                                LanguageId = int.Parse(LanguageArray[k].ToString()),
                                OrderNumber = 0,
                                Status = 1
                            };

                            db.GalleryImages.Add(grow);
                            db.SaveChanges();
                        }
                    }

                    TempData["Success"] = GeneralFunctions.ReturnSuccess(1);

                    db.Dispose();
                }
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                filesaveerrors.Add(file.FileName + " veritabanına kaydedilirken bir hata oluştu!");
                return Json(result, JsonRequestBehavior.DenyGet);
            }

        }

        public ActionResult UpdateGalleryImage(int id)
        {
            var MenuList = (from x in db.MenuNames where x.TopMenuId == 31 orderby x.MenuId ascending select x).ToList();
            ViewBag.MenuList = MenuList;
            var Detail = (from x in db.Images where x.ImageId == id select x).SingleOrDefault();

            ViewBag.GalleryList = (from x in db.GalleryNames select x).ToList();

            ViewBag.ImageGalleries = (from x in db.GalleryImages where x.ImageId == id select x).ToList();

            ViewBag.LanguageList = LanguageController.Languages();

            if (Detail != null)
            {
                return View(Detail);
            }
            else
            {
                return View();
            }
        }

        [HttpPost]
        public ActionResult UpdateGalleryImage(int id, Images giml, string[] GalleryIds, string[] LanguageIds, HttpPostedFileBase file)
        {
            var Detail = (from x in db.Images where x.ImageId == id select x).SingleOrDefault();

            ViewBag.GalleryList = (from x in db.GalleryNames select x).ToList();

            ViewBag.ImageGalleries = (from x in db.GalleryImages where x.ImageId == id select x).ToList();

            ViewBag.LanguageList = LanguageController.Languages();

            try
            {

                string image = string.Empty;
                if (file != null)
                {
                    var extension = file.FileName.Substring(file.FileName.LastIndexOf('.'));
                    image = Guid.NewGuid().ToString() + extension;
                    var imageToResize = System.Drawing.Image.FromStream(file.InputStream);
                    ImageSave.InsertImage(imageToResize, image);
                    Detail.ImageName = image;
                }

                Detail.Description = giml.Description;
                Detail.Title = giml.Title;

                db.SaveChanges();
                var GalleryImageList = (from x in db.GalleryImages where x.ImageId == id select x).ToList();
                foreach (var item in GalleryImageList)
                {
                    db.GalleryImages.Remove(item);
                }

                for (int i = 0; i < GalleryIds.Length; i++)
                {
                    for (int k = 0; k < LanguageIds.Length; k++)
                    {
                        var grow = new GalleryImages
                        {
                            CoverPic = false,
                            ImageId = id,
                            InsertedDate = DateTime.Now,
                            OrderNumber = 0,
                            Status = 1,
                            GalleryId = int.Parse(GalleryIds[i].ToString()),
                            LanguageId = int.Parse(LanguageIds[k].ToString()),
                        };

                        db.GalleryImages.Add(grow);
                        db.SaveChanges();
                    }
                }

                db.Dispose();
                TempData["Success"] = GeneralFunctions.ReturnSuccess(2);
                return View(Detail);
            }
            catch (Exception)
            {
                TempData["Error"] = GeneralFunctions.ReturnError(2);
                return View(Detail);
            }

        }


        public ActionResult CoverPic(int id, int GalleryId)
        {
            var GalleryImageList = (from x in db.GalleryImages where x.GalleryId == GalleryId select x).ToList();

            if (GalleryImageList.Count > 0)
            {
                foreach (var item in GalleryImageList)
                {
                    item.CoverPic = false;
                    db.SaveChanges();
                }
            }
            var GalleryImages = (from x in db.GalleryImages where x.GalleryId == GalleryId && x.ImageId == id select x).ToList();

            foreach (var item in GalleryImages)
            {
                item.CoverPic = true;
            }
            db.SaveChanges();

            return RedirectToAction("GalleryImages", "Gallery", new { id = GalleryId });
        }

        public ActionResult DeleteGalleryImage(int id)
        {
            var detail = (from x in db.Images where x.ImageId == id select x).FirstOrDefault();
            var GalleryImages = (from x in db.GalleryImages where x.ImageId == id select x).ToList();
            var GalleryId = 0;

            if (GalleryImages.Count > 0)
            {
                GalleryId = GalleryImages[0].GalleryId.Value;
            }

            if (detail != null)
            {

                var imagename = detail.ImageName;
                if (System.IO.File.Exists(Server.MapPath("/media/images/" + imagename)))
                {
                    System.IO.File.Delete(Server.MapPath("/media/images/" + imagename));
                }

                if (System.IO.File.Exists(Server.MapPath("/media/images/thumbnails" + imagename)))
                {
                    System.IO.File.Delete(Server.MapPath("/media/images/thumbnails" + imagename));
                }

                if (System.IO.File.Exists(Server.MapPath("/media/images/thumbnails40" + imagename)))
                {
                    System.IO.File.Delete(Server.MapPath("/media/images/thumbnails40" + imagename));
                }

                foreach (var item in GalleryImages)
                {
                    db.GalleryImages.Remove(item);
                }

                db.Images.Remove(detail);

                db.SaveChanges();
            }
            db.Dispose();
            return RedirectToAction("GalleryImages", "Gallery", new { id = GalleryId });
        }

        public JsonResult Imagesort(List<string> Ids)
        {
            try
            {
                for (int i = 1; i < Ids.Count + 1; i++)
                {
                    if (!string.IsNullOrEmpty(Ids[i - 1]))
                    {
                        var id = int.Parse(Ids[i - 1]);

                        var Image = (from x in db.GalleryImages where x.GalleryImageId == id select x).SingleOrDefault();
                        Image.OrderNumber = i;
                        db.SaveChanges();
                    }

                }
                db.Dispose();
                return Json(new { Code = 200 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { Code = 500 }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GalleryVideos(int id)
        {
            var List = (from x in db.GalleryLanguages join g in db.GalleryVideoLanguages on x.GalleryLanguageId equals g.GalleryLanguageId where x.GalleryId == id orderby g.OrderNumber ascending select new { x, g }).ToList();

            ViewBag.GalleryLanguageList = (from x in db.GalleryLanguages where x.GalleryId == id select x).ToList();


            var VideoList = new List<GalleryVideoLanguages>();

            foreach (var item in List)
            {
                VideoList.Add(new GalleryVideoLanguages
                {

                    Description = item.g.Description,
                    GalleryVideoLanguageId = item.g.GalleryVideoLanguageId,
                    GalleryLanguageId = item.g.GalleryLanguageId,
                    InsertedDate = item.g.InsertedDate,
                    OrderNumber = item.g.OrderNumber,
                    VideoIdentity = item.g.VideoIdentity,
                    VideoType = item.g.VideoType,
                    Status = item.g.Status,
                    Title = item.g.Title
                });
            }

            return View(VideoList);
        }

        public ActionResult CreateGalleryVideo(int id)
        {
            var GalleryLanguageList = (from x in db.GalleryLanguages where x.GalleryId == id select x).ToList();

            ViewBag.GalleryLanguageList = GalleryLanguageList;

            return View();
        }

        [HttpPost]
        public ActionResult CreateGalleryVideo(int id, GalleryVideoLanguages gvl)
        {
            var GalleryLanguageList = (from x in db.GalleryLanguages where x.GalleryId == id select x).ToList();

            ViewBag.GalleryLanguageList = GalleryLanguageList;
            try
            {

                string videoidentity = string.Empty;
                if (gvl.VideoType == 1)
                {
                    var VideoUrl = new Uri(gvl.VideoIdentity);
                    NameValueCollection nvc = new NameValueCollection(HttpUtility.ParseQueryString(VideoUrl.Query));
                    var VideoIdentity = nvc["v"];
                    videoidentity = VideoIdentity;
                }
                else
                {
                    var urlssplit = gvl.VideoIdentity.Split('/');

                    videoidentity = urlssplit[urlssplit.Length - 1].ToString();
                }

                var row = new GalleryVideoLanguages
                {
                    VideoType = gvl.VideoType,
                    Status = 1,
                    Description = gvl.Description,
                    VideoIdentity = videoidentity,
                    Title = gvl.Title,
                    GalleryLanguageId = gvl.GalleryLanguageId,
                    OrderNumber = 0,
                    InsertedDate = DateTime.Now
                };
                db.GalleryVideoLanguages.Add(row);
                db.SaveChanges(); db.Dispose();
                TempData["Success"] = GeneralFunctions.ReturnSuccess(1);

                return RedirectToAction("CreateGalleryVideo", "Gallery", new { id = id });
            }
            catch (Exception)
            {
                return RedirectToAction("CreateGalleryVideo", "Gallery", new { id = id });
            }

        }

        public ActionResult UpdateGalleryVideo(int id)
        {
            var Detail = (from x in db.GalleryVideoLanguages where x.GalleryVideoLanguageId == id select x).SingleOrDefault();

            if (Detail != null)
            {
                return View(Detail);
            }
            else
            {
                return View();
            }
        }

        [HttpPost]
        public ActionResult UpdateGalleryVideo(int id, GalleryVideoLanguages gvl)
        {
            var Detail = (from x in db.GalleryVideoLanguages where x.GalleryVideoLanguageId == id select x).SingleOrDefault();
            try
            {
                string videoidentity = string.Empty;
                if (gvl.VideoType == 1)
                {
                    var VideoUrl = new Uri(gvl.VideoIdentity);
                    NameValueCollection nvc = new NameValueCollection(HttpUtility.ParseQueryString(VideoUrl.Query));
                    var VideoIdentity = nvc["v"];
                    videoidentity = VideoIdentity;
                    Detail.VideoIdentity = videoidentity;
                }
                else
                {
                    var urlssplit = gvl.VideoIdentity.Split('/');
                    videoidentity = urlssplit[urlssplit.Length - 1].ToString();
                    Detail.VideoIdentity = videoidentity;
                }

                Detail.Description = gvl.Description;
                Detail.Title = gvl.Title;
                Detail.VideoType = gvl.VideoType;

                db.SaveChanges();
                db.Dispose();
                TempData["Success"] = GeneralFunctions.ReturnSuccess(2);
                return View(Detail);
            }
            catch (Exception)
            {
                TempData["Error"] = GeneralFunctions.ReturnError(2);
                return View(Detail);
            }

        }

        public ActionResult DeleteGalleryVideo(int id)
        {
            var detail = (from x in db.GalleryVideoLanguages where x.GalleryVideoLanguageId == id select x).FirstOrDefault();
            var GalleryDetail = (from x in db.GalleryLanguages where x.GalleryLanguageId == detail.GalleryLanguageId select x).SingleOrDefault();
            if (detail != null)
            {
                db.GalleryVideoLanguages.Remove(detail);
                db.SaveChanges();
            }
            db.Dispose();
            return RedirectToAction("GalleryVideos", "Gallery", new { id = GalleryDetail.GalleryId });
        }

        public JsonResult Videosort(List<string> Ids)
        {
            try
            {
                for (int i = 1; i < Ids.Count + 1; i++)
                {
                    if (!string.IsNullOrEmpty(Ids[i - 1]))
                    {
                        var id = int.Parse(Ids[i - 1]);

                        var Video = (from x in db.GalleryVideoLanguages where x.GalleryVideoLanguageId == id select x).SingleOrDefault();
                        Video.OrderNumber = i;
                        db.SaveChanges();
                    }

                }
                db.Dispose();
                return Json(new { Code = 200 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { Code = 500 }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult ChangeStatus(ChangeStatus1 chs)
        {
            try
            {
                var Detail = (from x in db.GalleryNames where x.GalleryId == chs.id select x).FirstOrDefault();

                if (chs.Status == 2)
                {
                    Detail.Status = 2;
                }
                else
                {
                    Detail.Status = 1;
                }

                db.SaveChanges();
                db.Dispose();

                return Json(new { Ok = "OK" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { Ok = "BAD" }, JsonRequestBehavior.DenyGet);
            }


        }

        [Serializable]
        public class ChangeStatus1
        {
            public int id { get; set; }
            public int Status { get; set; }
        }


        [Serializable]
        public class GalleryImage
        {
            public int GalleryImageId { get; set; }
            public int ImageId { get; set; }
            public string ImageName { get; set; }
            public string Title { get; set; }
            public string Description { get; set; }
            public DateTime InsertedDate { get; set; }
            public int GalleryId { get; set; }
            public int LanguageId { get; set; }
            public bool CoverPic { get; set; }
            public int OrderNumber { get; set; }
            public int Status { get; set; }

        }


        public ActionResult GalleryDocuments(int id)
        {
            var List = (from x in db.GalleryLanguages join g in db.GalleryDocumentLanguages on x.GalleryLanguageId equals g.GalleryLanguageId where x.GalleryId == id orderby g.OrderNumber ascending select new { x, g }).ToList();

            ViewBag.GalleryLanguageList = (from x in db.GalleryLanguages where x.GalleryId == id select x).ToList();


            var DocumentList = new List<GalleryDocumentLanguages>();

            foreach (var item in List)
            {
                DocumentList.Add(new GalleryDocumentLanguages
                {
                    GalleryDocumentLanguageId = item.g.GalleryDocumentLanguageId,
                    GalleryLanguageId = item.g.GalleryLanguageId,
                    DocumentName = item.g.DocumentName,
                    InsertedDate = item.g.InsertedDate,
                    OrderNumber = item.g.OrderNumber,
                    Status = item.g.Status,
                    Title = item.g.Title
                });
            }

            return View(DocumentList);
        }

        public ActionResult CreateGalleryDocument(int id)
        {
            var GalleryLanguageList = (from x in db.GalleryLanguages where x.GalleryId == id select x).ToList();

            ViewBag.GalleryLanguageList = GalleryLanguageList;

            return View();
        }

        [HttpPost]
        public ActionResult CreateGalleryDocument(int id, GalleryDocumentLanguages ciml, HttpPostedFileBase[] file)
        {
            var GalleryLanguageList = (from x in db.GalleryLanguages where x.GalleryId == id select x).ToList();

            ViewBag.GalleryLanguageList = GalleryLanguageList;
            try
            {
                if (file != null)
                {
                    foreach (var item in file)
                    {
                        string DocumentFile = string.Empty;
                        var extension = item.FileName.Substring(item.FileName.LastIndexOf('.'));
                        DocumentFile = Guid.NewGuid().ToString() + extension;
                        DocumentSave.InsertDocument(item, DocumentFile);

                        var row = new GalleryDocumentLanguages
                        {
                            Status = 1,
                            DocumentName = DocumentFile,
                            Title = "",
                            GalleryLanguageId = ciml.GalleryLanguageId,
                            OrderNumber = 0,
                            InsertedDate = DateTime.Now
                        };
                        db.GalleryDocumentLanguages.Add(row);
                        db.SaveChanges();
                        TempData["Success"] = GeneralFunctions.ReturnSuccess(1);
                    }
                    db.Dispose();
                }

                return RedirectToAction("CreateGalleryDocument", "Gallery", new { id = id });
            }
            catch (Exception)
            {
                return RedirectToAction("CreateGalleryDocument", "Gallery", new { id = id });
            }

        }

        public ActionResult UpdateGalleryDocument(int id)
        {
            var Detail = (from x in db.GalleryDocumentLanguages where x.GalleryDocumentLanguageId == id select x).SingleOrDefault();

            if (Detail != null)
            {
                return View(Detail);
            }
            else
            {
                return View();
            }
        }

        [HttpPost]
        public ActionResult UpdateGalleryDocument(int id, GalleryDocumentLanguages ciml, HttpPostedFileBase file)
        {
            var Detail = (from x in db.GalleryDocumentLanguages where x.GalleryDocumentLanguageId == id select x).SingleOrDefault();
            try
            {
                string DocumentFile = string.Empty;
                if (file != null)
                {
                    var extension = file.FileName.Substring(file.FileName.LastIndexOf('.'));
                    DocumentFile = Guid.NewGuid().ToString() + extension;
                    DocumentSave.InsertDocument(file, DocumentFile);
                    Detail.DocumentName = DocumentFile;
                }

                Detail.Title = ciml.Title;

                db.SaveChanges();
                db.Dispose();
                TempData["Success"] = GeneralFunctions.ReturnSuccess(2);
                return View(Detail);
            }
            catch (Exception)
            {
                TempData["Error"] = GeneralFunctions.ReturnError(2);
                return View(Detail);
            }

        }

        public ActionResult DeleteGalleryDocument(int id)
        {
            var detail = (from x in db.GalleryDocumentLanguages where x.GalleryDocumentLanguageId == id select x).FirstOrDefault();
            var GalleryDetail = (from x in db.GalleryLanguages where x.GalleryLanguageId == detail.GalleryLanguageId select x).SingleOrDefault();
            if (detail != null)
            {

                var documentname = detail.DocumentName;
                if (System.IO.File.Exists(Server.MapPath("/media/images/" + documentname)))
                {
                    System.IO.File.Delete(Server.MapPath("/media/images/" + documentname));
                }

                db.GalleryDocumentLanguages.Remove(detail);

                db.SaveChanges();
            }
            db.Dispose();
            return RedirectToAction("GalleryDocuments", "Gallery", new { id = GalleryDetail.GalleryId });
        }

        public JsonResult Documentsort(List<string> Ids)
        {
            try
            {
                for (int i = 1; i < Ids.Count + 1; i++)
                {
                    if (!string.IsNullOrEmpty(Ids[i - 1]))
                    {
                        var id = int.Parse(Ids[i - 1]);

                        var Document = (from x in db.GalleryDocumentLanguages where x.GalleryDocumentLanguageId == id select x).SingleOrDefault();
                        Document.OrderNumber = i;
                        db.SaveChanges();
                    }

                }
                db.Dispose();
                return Json(new { Code = 200 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { Code = 500 }, JsonRequestBehavior.AllowGet);
            }
        }

    }
}
