﻿using Lissiya_Otel.Functions;
using Lissiya_Otel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Lissiya_Otel.Areas.Admin.Controllers
{
    public class SliderController : Controller
    {
        //
        // GET: /Admin/Slider/

        lissiyaEntities db = new lissiyaEntities();

        public ActionResult SliderNames()
        {
            var SliderNameList = (from x in db.SliderNames select x).ToList();

            ViewBag.LanguageList = LanguageController.Languages();
            var MenuList = (from x in db.MenuNames orderby x.MenuId ascending select x).ToList();

            ViewBag.MenuList = MenuList;
            return View(SliderNameList);
        }

        public ActionResult CreateSliderNames()
        {
            var LangaugeList = LanguageController.Languages();
            if (LangaugeList.Count > 0)
            {
                ViewBag.LanguageList = LangaugeList;
            }
            var MenuList = (from x in db.MenuNames orderby x.MenuId ascending select x).ToList();

            ViewBag.MenuList = MenuList;
            return View();
        }

        public ActionResult CreateSliderImage()
        {

            return View();
        }

        public ActionResult SliderImages(int id)
        {
            var SliderImageList = (from x in db.SliderImages orderby x.OrderNumber ascending where x.SliderId == id select x).ToList();

            return View(SliderImageList);
        }

        [HttpPost]
        public ActionResult CreateSliderNames(SliderNames cnn, int menuId)
        {

            try
            { 
                //var Detail = (from x in db.MenuLanguages where x.MenuId == menuId && x.LanguageId==cnn.LanguageId select x).SingleOrDefault();
                var Detail = (from c in db.MenuLanguages where c.MenuId == menuId select c).First(); 
                Detail.SliderId = cnn.SliderId;
                var Detail2 = (from c in db.MenuLanguages where c.MenuId == menuId && c.LanguageId==2 select c).First();
                Detail2.SliderId = cnn.SliderId;
                //var detail=db.MenuLanguages.Find()
                //db.SaveChanges();
                var row = new SliderNames
                {
                    Status = 1,
                    Name = cnn.Name,
                    LanguageId = cnn.LanguageId,
                    InsertedDate = DateTime.Now
                };
                db.SliderNames.Add(row);
                db.SaveChanges();
                var MenuList = (from x in db.MenuNames orderby x.MenuId ascending select x).ToList();
                ViewBag.MenuList = MenuList;
                TempData["Success"] = GeneralFunctions.ReturnSuccess(1);

                return RedirectToAction("CreateSliderImage", "Slider", new { id = row.SliderId });
            }
            catch (Exception)
            {
                TempData["Error"] = GeneralFunctions.ReturnError(1);
                return View();
            }

        }

        [HttpPost, ValidateInput(false)]
        public ActionResult CreateSliderImage(int id, SliderImages sim, HttpPostedFileBase file, bool? Tab)
        {
            try
            {
                bool TabControl = false;
                if (Tab != null)
                {
                    TabControl = true;
                }
                string image = string.Empty;
                if (file != null)
                {
                    var extension = file.FileName.Substring(file.FileName.LastIndexOf('.'));
                    image = Guid.NewGuid().ToString() + extension;
                    var imageToResize = System.Drawing.Image.FromStream(file.InputStream);
                    ImageSave.InsertImage(imageToResize, image);
                }

                var row = new SliderImages
                {
                    Status = 1,
                    Description = sim.Description,
                    OrderNumber = 0,
                    SliderId = id,
                    ExternalLink = TabControl,
                    Title = sim.Title,
                    ImageName = image,
                    Url = sim.Url,
                    InsertedDate = DateTime.Now
                };
                db.SliderImages.Add(row);
                db.SaveChanges();
                TempData["Success"] = GeneralFunctions.ReturnSuccess(1);

                return RedirectToAction("CreateSliderImage", "Slider", new { id = row.SliderId });
            }
            catch (Exception)
            {
                TempData["Error"] = GeneralFunctions.ReturnError(1);
                return View();
            }
        }

        public ActionResult UpdateSliderImage(int id)
        {

            var Detail = (from x in db.SliderImages where x.SliderImageId == id select x).SingleOrDefault();
            if (Detail != null)
            {
                return View(Detail);
            }
            else
            {
                return View();
            }

        }

        [HttpPost, ValidateInput(false)]
        public ActionResult UpdateSliderImage(int id, SliderImages blg, HttpPostedFileBase file)
        {
            var Detail = (from x in db.SliderImages where x.SliderImageId == id select x).SingleOrDefault();
            try
            {
                string image = string.Empty;
                if (file != null)
                {
                    var extension = file.FileName.Substring(file.FileName.LastIndexOf('.'));
                    image = Guid.NewGuid().ToString() + extension;
                    var imageToResize = System.Drawing.Image.FromStream(file.InputStream);
                    ImageSave.InsertImage(imageToResize, image);
                    Detail.ImageName = image;
                }

                Detail.Description = blg.Description;
                Detail.Title = blg.Title;

                db.SaveChanges();
                db.Dispose();
                TempData["Success"] = GeneralFunctions.ReturnSuccess(2);
                return View(Detail);
            }
            catch (Exception)
            {
                TempData["Error"] = GeneralFunctions.ReturnError(2);
                return View(Detail);
            }

        }

        public ActionResult UpdateSliderNames(int id)
        {
         
            var MenuList = (from x in db.MenuNames orderby x.MenuId ascending select x).ToList();
            ViewBag.MenuList = MenuList;

            var Detail = (from x in db.SliderNames where x.SliderId == id select x).SingleOrDefault();
            try
            {
                var CurnetMenu = db.MenuLanguages.Where(p => p.SliderId == id).Take(1).SingleOrDefault();
                ViewBag.CurnetMenu = CurnetMenu.MenuId;
            }
            catch 
            {

            }
          
            return View(Detail);
        }

        [HttpPost]
        public ActionResult UpdateSliderNames(int id, SliderNames cnn,int menuId)
        {
           
            var Detail = (from x in db.SliderNames where x.SliderId == id select x).SingleOrDefault();
            try
            {
                var MenuList = (from x in db.MenuNames orderby x.MenuId ascending select x).ToList();
                
                ViewBag.MenuList = MenuList;
                var menulanguages = db.MenuLanguages.Where(p => p.SliderId == id).ToList();
                menulanguages.ForEach(p=>p.SliderId=0);
                var Detail2 = (from c in db.MenuLanguages where c.MenuId == menuId select c).First();
                Detail2.SliderId = Detail.SliderId;
                var Detail3 = (from c in db.MenuLanguages where c.MenuId == menuId && c.LanguageId==2 select c).First();
                Detail3.SliderId = Detail.SliderId;
                
                Detail.Name = cnn.Name;
                db.SaveChanges();
                db.Dispose();
                TempData["Success"] = GeneralFunctions.ReturnSuccess(2);
                ViewBag.CurnetMenu = menuId;
                return View(Detail);
            }
            catch (Exception)
            {
                TempData["Error"] = GeneralFunctions.ReturnError(2);
                return View(Detail);
            }


        }

        public ActionResult Delete(int id)
        {
            var detail = (from x in db.SliderNames where x.SliderId == id select x).FirstOrDefault();
            var SliderImageList = (from x in db.SliderImages where x.SliderId == id select x).ToList();
            var MenuList = (from x in db.MenuNames orderby x.MenuId ascending select x).ToList();
            ViewBag.MenuList = MenuList;
            if (detail != null)
            {
                db.SliderNames.Remove(detail);

                foreach (var item in SliderImageList)
                {
                    if (System.IO.File.Exists("/media/images/" + item.ImageName))
                    {
                        System.IO.File.Delete("/media/images/" + item.ImageName);
                    }

                    if (System.IO.File.Exists(Server.MapPath("/media/images/thumbnails" + item.ImageName)))
                    {
                        System.IO.File.Delete(Server.MapPath("/media/images/thumbnails" + item.ImageName));
                    }

                    if (System.IO.File.Exists(Server.MapPath("/media/images/thumbnails40" + item.ImageName)))
                    {
                        System.IO.File.Delete(Server.MapPath("/media/images/thumbnails40" + item.ImageName));
                    }

                    db.SliderImages.Remove(item);
                }

                db.SaveChanges();
            }
            db.Dispose();
            return RedirectToAction("SliderNames", "Slider");
        }

        public ActionResult DeleteSliderImage(int id)
        {
            var detail = (from x in db.SliderImages where x.SliderImageId == id select x).SingleOrDefault();
            int sliderid = (int )detail.SliderId;
            if (detail != null)
            {
                var imagename = detail.ImageName;
                if (System.IO.File.Exists(Server.MapPath("/media/images/" + imagename)))
                {
                    System.IO.File.Delete(Server.MapPath("/media/images/" + imagename));
                }

                db.SliderImages.Remove(detail);
                db.SaveChanges();
                
            }
            //  Response.Redirect("/Slider/SliderImages" + detail.SliderId.ToString());
            return RedirectToAction("SliderImages", "Slider", new { id = sliderid });
           
        }

        public JsonResult Sort(int id, List<string> Ids)
        {

            try
            {
                for (int i = 1; i < Ids.Count + 1; i++)
                {
                    var sid = int.Parse(Ids[i - 1]);
                    var Slider = (from x in db.SliderImages where x.SliderImageId == sid && x.SliderId == id select x).SingleOrDefault();
                    Slider.OrderNumber = i;
                    db.SaveChanges();
                }
                db.Dispose();
                return Json(new { Code = 200 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { Code = 500 }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult ChangeStatus(ChangeStatus1 chs)
        {
            try
            {
                var Detail = (from x in db.SliderNames where x.SliderId == chs.id select x).FirstOrDefault();

                if (chs.Status == 2)
                {
                    Detail.Status = 2;
                }
                else
                {
                    Detail.Status = 1;
                }

                db.SaveChanges();
                db.Dispose();

                return Json(new { Ok = "OK" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { Ok = "BAD" }, JsonRequestBehavior.DenyGet);
            }


        }

        [Serializable]
        public class ChangeStatus1
        {
            public int id { get; set; }
            public int Status { get; set; }
        }

        [HttpPost]
        public JsonResult ChangeStatusImage(ChangeStatusImage1 chs)
        {
            try
            {
                var Detail = (from x in db.SliderImages where x.SliderImageId == chs.id select x).FirstOrDefault();

                if (chs.Status == 2)
                {
                    Detail.Status = 2;
                }
                else
                {
                    Detail.Status = 1;
                }

                db.SaveChanges();
                db.Dispose();

                return Json(new { Ok = "OK" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { Ok = "BAD" }, JsonRequestBehavior.DenyGet);
            }


        }

        [Serializable]
        public class ChangeStatusImage1
        {
            public int id { get; set; }
            public int Status { get; set; }
        }


    }
}
