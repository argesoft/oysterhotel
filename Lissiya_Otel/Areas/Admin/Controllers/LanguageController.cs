﻿using Lissiya_Otel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Lissiya_Otel.Areas.Admin.Controllers
{
    public class LanguageController : Controller
    {
        //
        // GET: /Admin/Language/
        private static lissiyaEntities db = new lissiyaEntities();
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult GetLanguageDetail(int id)
        {
            var LanguageDetail = (from x in db.Languages where x.LanguageId == id select x).SingleOrDefault();

            return Json(new { Language = LanguageDetail }, JsonRequestBehavior.AllowGet);
        }

        public static List<Languages> Languages()
        {
            var LanguageList = (from x in db.Languages where x.Status == 1 select x).ToList();

            return LanguageList;
        }
    }
}
