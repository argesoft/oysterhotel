﻿using Lissiya_Otel.Functions;
using Lissiya_Otel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Lissiya_Otel.Areas.Admin.Controllers
{
    public class ServicesController : Controller
    {
        //
        // GET: /Admin/Services/
        lissiyaEntities db = new lissiyaEntities();

        public ActionResult ServiceList()
        {
            //var List = (from x in db.ServiceNames orderby x.OrderNumber ascending select x).ToList();
            //return View(List);

            var ServiceNameList = (from x in db.ServiceNames orderby x.OrderNumber ascending select x).ToList();
            ViewBag.LanguageList = LanguageController.Languages();
            return View(ServiceNameList);
        }

        public ActionResult CreateServiceNames()
        {
            return View();
        }

        public ActionResult CreateServiceLanguage()
        {
            var LangaugeList = LanguageController.Languages();
            if (LangaugeList.Count > 0)
            {
                ViewBag.LanguageList = LangaugeList;
            }
            return View();
        }

        [HttpPost]
        public ActionResult CreateServiceNames(ServiceNames bln)
        {
            try
            {
                var row = new ServiceNames
                {
                    Status = 1,
                    Name = bln.Name,
                    OrderNumber = 0,
                    InsertedDate = DateTime.Now
                };
                db.ServiceNames.Add(row);
                db.SaveChanges();
                TempData["Success"] = GeneralFunctions.ReturnSuccess(1);

                return RedirectToAction("CreateServiceLanguage", "Services", new { id = row.ServiceId });
            }
            catch (Exception)
            {
                TempData["Error"] = GeneralFunctions.ReturnError(1);
                return View();
            }
        }


        [HttpPost, ValidateInput(false)]
        public ActionResult CreateServiceLanguage(ServiceLanguages blg, string updatecontrol, HttpPostedFileBase file)
        {
            var LangaugeList = LanguageController.Languages();
            if (LangaugeList.Count > 0)
            {
                ViewBag.LanguageList = LangaugeList;
            }
            try
            {
                if (blg.LanguageID != null)
                {
                    var Detail = (from x in db.ServiceLanguages where x.ServiceID == blg.ServiceID && x.LanguageID == blg.LanguageID select x).SingleOrDefault();

                    if (Detail == null)
                    {
                        string image = string.Empty;
                        if (file != null)
                        {
                            var extension = file.FileName.Substring(file.FileName.LastIndexOf('.'));
                            image = Guid.NewGuid().ToString() + extension;
                            var imageToResize = System.Drawing.Image.FromStream(file.InputStream);
                            ImageSave.InsertImage(imageToResize, image);
                        }
                        var row = new ServiceLanguages
                        {
                            InsertedDate = DateTime.Now,
                            LanguageID = blg.LanguageID,
                            //OrderNumber = 0,
                            Title = blg.Title,
                            ServiceID = blg.ServiceID,
                            Description = blg.Description,
                            ImageName = image,
                            //InsertedDate = blg.InsertedDate,
                            SeoDescription = blg.SeoDescription,
                            SeoKeywords = blg.SeoKeywords,
                            SeoTitle = blg.SeoTitle,
                            Status = 1,
                            Summary = blg.Summary


                        };

                        db.ServiceLanguages.Add(row);
                        db.SaveChanges();
                        db.Dispose();
                        TempData["Success"] = GeneralFunctions.ReturnSuccess(1);
                        if (!string.IsNullOrEmpty(updatecontrol))
                        {
                            return RedirectToAction("UpdateServiceLanguage", "Services", new { id = row.ServiceID, LangId = row.LanguageID });
                        }
                        else
                        {
                            return RedirectToAction("CreateServiceLanguage", "Services", new { id = row.ServiceID });
                        }

                    }
                    else
                    {
                        TempData["Info"] = GeneralFunctions.ReturnInfo(1);
                        if (!string.IsNullOrEmpty(updatecontrol))
                        {
                            return RedirectToAction("UpdateServiceLanguage", "Services", new { id = blg.ServiceID, LangId = blg.LanguageID });
                        }
                        else
                        {
                            return RedirectToAction("CreateServiceLanguage", "Services", new { id = blg.ServiceID });
                        }
                    }



                }
                else
                {
                    TempData["Error"] = GeneralFunctions.ReturnError(4);
                    if (!string.IsNullOrEmpty(updatecontrol))
                    {
                        return RedirectToAction("UpdateServiceLanguage", "Services", new { id = blg.ServiceID, LangId = blg.LanguageID });
                    }
                    else
                    {
                        return RedirectToAction("CreateServiceLanguage", "Services", new { id = blg.ServiceID });
                    }

                }

            }
            catch (Exception)
            {
                TempData["Error"] = GeneralFunctions.ReturnError(1);
                if (!string.IsNullOrEmpty(updatecontrol))
                {
                    return RedirectToAction("UpdateServiceLanguage", "Services", new { id = blg.ServiceID, LangId = blg.LanguageID });
                }
                else
                {
                    return RedirectToAction("CreateServiceLanguage", "Services", new { id = blg.ServiceID });
                }

            }
        }



        public ActionResult UpdateServiceLanguage(int id)
        {
            int LanguageId = int.Parse(Request.QueryString["LangId"]);
            var Detail = (from x in db.ServiceLanguages where x.ServiceID == id && x.LanguageID == LanguageId select x).SingleOrDefault();
            if (Detail != null)
            {
                ViewBag.Detail = Detail;
                return View();
            }
            else
            {
                return View();
            }

        }

        [HttpPost, ValidateInput(false)]
        public ActionResult UpdateServiceLanguage(int id, ServiceLanguages blg, HttpPostedFileBase file)
        {
            int LanguageId = int.Parse(Request.QueryString["LangId"]);
            var Detail = (from x in db.ServiceLanguages where x.ServiceID == id && x.LanguageID == LanguageId select x).SingleOrDefault();
            try
            {
                string logo = string.Empty;
                if (file != null)
                {
                    var extension = file.FileName.Substring(file.FileName.LastIndexOf('.'));
                    logo = Guid.NewGuid().ToString() + extension;
                    var imageToResize = System.Drawing.Image.FromStream(file.InputStream);
                    ImageSave.InsertImage(imageToResize, logo);
                    Detail.ImageName = logo;
                }

                Detail.InsertedDate = blg.InsertedDate;
                Detail.Description = blg.Description;
                Detail.SeoDescription = blg.SeoDescription;
                Detail.SeoKeywords = blg.SeoKeywords;
                Detail.SeoTitle = blg.SeoTitle;
                Detail.Summary = blg.Summary;
                Detail.Title = blg.Title;

                db.SaveChanges();
                db.Dispose();
                TempData["Success"] = GeneralFunctions.ReturnSuccess(2);
                ViewBag.Detail = Detail;
                return View();
            }
            catch (Exception)
            {
                TempData["Error"] = GeneralFunctions.ReturnError(2);
                ViewBag.Detail = Detail;
                return View();
            }

        }

        public ActionResult UpdateServiceNames(int id)
        {
            var Detail = (from x in db.ServiceNames where x.ServiceId == id select x).SingleOrDefault();

            return View(Detail);
        }

        [HttpPost]
        public ActionResult UpdateServiceNames(int id, ServiceNames adn)
        {
            var Detail = (from x in db.ServiceNames where x.ServiceId == id select x).SingleOrDefault();
            try
            {
                Detail.Name = adn.Name;
                db.SaveChanges();
                db.Dispose();
                TempData["Success"] = GeneralFunctions.ReturnSuccess(2);
                return View(Detail);
            }
            catch (Exception)
            {
                TempData["Error"] = GeneralFunctions.ReturnError(2);
                return View(Detail);
            }


        }

        public ActionResult Delete(int id)
        {
            var detail = (from x in db.ServiceNames where x.ServiceId == id select x).FirstOrDefault();
            var ServiceLanguagesList = (from x in db.ServiceLanguages where x.ServiceID == id select x).ToList();

            if (detail != null)
            {
                db.ServiceNames.Remove(detail);

                foreach (var item in ServiceLanguagesList)
                {
                    var imagename = item.ImageName;
                    if (System.IO.File.Exists(Server.MapPath("/media/images/" + imagename)))
                    {
                        System.IO.File.Delete(Server.MapPath("/media/images/" + imagename));
                    }

                    if (System.IO.File.Exists(Server.MapPath("/media/images/thumbnails" + imagename)))
                    {
                        System.IO.File.Delete(Server.MapPath("/media/images/thumbnails" + imagename));
                    }

                    if (System.IO.File.Exists(Server.MapPath("/media/images/thumbnails40" + imagename)))
                    {
                        System.IO.File.Delete(Server.MapPath("/media/images/thumbnails40" + imagename));
                    }
                    db.ServiceLanguages.Remove(item);
                }

                db.SaveChanges();
            }
            db.Dispose();
            return RedirectToAction("ServiceList", "Services");
        }

        public ActionResult DeleteLanguage(int id, int LangId)
        {
            var Detail = (from x in db.ServiceLanguages where x.ServiceID == id && x.LanguageID == LangId select x).SingleOrDefault();

            if (Detail != null)
            {
                db.ServiceLanguages.Remove(Detail);
                db.SaveChanges();
                db.Dispose();
            }

            return RedirectToAction("CreateServiceLanguage", "Services", new { id = id });
        }

        public JsonResult Sort(List<string> Ids)
        {

            try
            {
                for (int i = 1; i < Ids.Count + 1; i++)
                {
                    var id = int.Parse(Ids[i - 1]);
                    var News = (from x in db.ServiceNames where x.ServiceId == id select x).SingleOrDefault();
                    News.OrderNumber = i;
                    db.SaveChanges();
                }
                db.Dispose();
                return Json(new { Code = 200 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { Code = 500 }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult ChangeStatus(ChangeStatus1 chs)
        {
            try
            {
                var Detail = (from x in db.ServiceNames where x.ServiceId == chs.id select x).FirstOrDefault();

                if (chs.Status == 2)
                {
                    Detail.Status = 2;
                }
                else
                {
                    Detail.Status = 1;
                }

                db.SaveChanges();
                db.Dispose();

                return Json(new { Ok = "OK" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { Ok = "BAD" }, JsonRequestBehavior.DenyGet);
            }




            //[HttpPost,ValidateInput(false)]
            //public ActionResult CreateService(ServiceNames sv, HttpPostedFileBase file)
            //{
            //    try
            //    {
            //        string image = string.Empty;
            //        if (file != null)
            //        {
            //            var extension = file.FileName.Substring(file.FileName.LastIndexOf('.'));
            //            image = Guid.NewGuid().ToString() + extension;
            //            var imageToResize = System.Drawing.Image.FromStream(file.InputStream);
            //            ImageSave.InsertImage(imageToResize, image);
            //        }
            //        var row = new ServiceNames
            //        {
            //            Status = 1,
            //            Content = sv.,
            //            Summary = sv.Summary,
            //            Title = sv.Title,
            //            Image = image,
            //            OrderNumber = 0,
            //            InsertedDate = DateTime.Now
            //        };
            //        db.Services.Add(row);
            //        db.SaveChanges();
            //        TempData["Success"] = GeneralFunctions.ReturnSuccess(1);

            //        return View();
            //    }
            //    catch (Exception)
            //    {
            //        TempData["Error"] = GeneralFunctions.ReturnError(1);
            //        return View();
            //    }
            //}

            //[HttpPost,ValidateInput(false)]
            //public ActionResult UpdateService(int id, Services sv,HttpPostedFileBase file)
            //{

            //    var Detail = (from x in db.Services where x.ServiceId == id select x).SingleOrDefault();
            //    try
            //    {
            //        string logo = string.Empty;
            //        if (file != null)
            //        {
            //            var extension = file.FileName.Substring(file.FileName.LastIndexOf('.'));
            //            logo = Guid.NewGuid().ToString() + extension;
            //            var imageToResize = System.Drawing.Image.FromStream(file.InputStream);
            //            ImageSave.InsertImage(imageToResize, logo);
            //            Detail.Image = logo;
            //        }

            //        Detail.Content =sv.Content;
            //        Detail.Summary = sv.Summary;
            //        Detail.Title = sv.Title;

            //        db.SaveChanges();
            //        db.Dispose();
            //        TempData["Success"] = GeneralFunctions.ReturnSuccess(2);
            //        return View(Detail);
            //    }
            //    catch (Exception)
            //    {
            //        TempData["Error"] = GeneralFunctions.ReturnError(2);

            //        return View(Detail);
            //    }
            //}

            //public ActionResult Delete(int id)
            //{
            //    var detail = (from x in db.Services where x.ServiceId == id select x).FirstOrDefault();


            //    if (detail != null)
            //    {
            //        db.Services.Remove(detail);

            //        db.SaveChanges();
            //    }
            //    db.Dispose();
            //    return RedirectToAction("ServiceList", "Services");
            //}

            //public JsonResult Sort(List<string> Ids)
            //{

            //    try
            //    {
            //        for (int i = 1; i < Ids.Count + 1; i++)
            //        {
            //            var id = int.Parse(Ids[i - 1]);
            //            var Service = (from x in db.Services where x.ServiceId == id select x).SingleOrDefault();
            //            Service.OrderNumber = i;
            //            db.SaveChanges();
            //        }
            //        db.Dispose();
            //        return Json(new { Code = 200 }, JsonRequestBehavior.AllowGet);
            //    }
            //    catch (Exception)
            //    {
            //        return Json(new { Code = 500 }, JsonRequestBehavior.AllowGet);
            //    }
            //}

            //[HttpPost]
            //public JsonResult ChangeStatus(ChangeStatus1 chs)
            //{
            //    try
            //    {
            //        var Detail = (from x in db.Services where x.ServiceId == chs.id select x).FirstOrDefault();

            //        if (chs.Status == 2)
            //        {
            //            Detail.Status = 2;
            //        }
            //        else
            //        {
            //            Detail.Status = 1;
            //        }

            //        db.SaveChanges();
            //        db.Dispose();

            //        return Json(new { Ok = "OK" }, JsonRequestBehavior.AllowGet);
            //    }
            //    catch (Exception)
            //    {
            //        return Json(new { Ok = "BAD" }, JsonRequestBehavior.DenyGet);
            //    }


        }

        // --- ODA ÖZELLİKLERİ ---
        public ActionResult OzellikNames()
        {
            var OzellikNameList = (from x in db.OzellikNames orderby x.OrderNumber ascending select x).ToList();

            ViewBag.LanguageList = LanguageController.Languages();

            return View(OzellikNameList);
        }

        public ActionResult CreateOzellikNames()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CreateOzellikNames(OzellikNames bln)
        {
            try
            {
                var row = new OzellikNames
                {
                    Status = 1,
                    Name = bln.Name,
                    OrderNumber = 0,
                    InsertedDate = DateTime.Now,
                    ServiceId = bln.ServiceId
                };
                db.OzellikNames.Add(row);
                db.SaveChanges();
                TempData["Success"] = GeneralFunctions.ReturnSuccess(1);

                return RedirectToAction("OzellikNames", "Services", new { id = row.ServiceId });
            }
            catch (Exception)
            {
                TempData["Error"] = GeneralFunctions.ReturnError(1);
                return View();
            }

        }

        public ActionResult UpdateOzellikNames(int id)
        {
            var Detail = (from x in db.OzellikNames where x.OzellikId == id select x).SingleOrDefault();

            return View(Detail);
        }

        [HttpPost]
        public ActionResult UpdateOzellikNames(int id, ServiceNames adn)
        {
            var Detail = (from x in db.OzellikNames where x.OzellikId == id select x).SingleOrDefault();
            try
            {
                Detail.Name = adn.Name;
                db.SaveChanges();
                db.Dispose();
                TempData["Success"] = GeneralFunctions.ReturnSuccess(2);
                return View(Detail);
            }
            catch (Exception)
            {
                TempData["Error"] = GeneralFunctions.ReturnError(2);
                return View(Detail);
            }


        }

        public ActionResult UpdateOzellikLanguage(int id)
        {
            int LanguageId = int.Parse(Request.QueryString["LangId"]);
            var Detail = (from x in db.OzellikLanguages where x.OzellikId == id && x.LanguageId == LanguageId select x).SingleOrDefault();
            if (Detail != null)
            {
                ViewBag.Detail = Detail;
                return View();
            }
            else
            {
                return View();
            }

        }

        [HttpPost, ValidateInput(false)]
        public ActionResult UpdateOzellikLanguage(int id, OzellikLanguages blg)
        {
            int LanguageId = int.Parse(Request.QueryString["LangId"]);
            var Detail = (from x in db.OzellikLanguages where x.OzellikId == id && x.LanguageId == LanguageId select x).SingleOrDefault();
            try
            {
                //string logo = string.Empty;
                //if (file != null)
                //{
                //    var extension = file.FileName.Substring(file.FileName.LastIndexOf('.'));
                //    logo = Guid.NewGuid().ToString() + extension;
                //    var imageToResize = System.Drawing.Image.FromStream(file.InputStream);
                //    ImageSave.InsertImage(imageToResize, logo);
                //    Detail.ImageName = logo;
                //}

                //Detail.NewsDate = blg.NewsDate;
                //Detail.Description = blg.Description;
                //Detail.SeoDescription = blg.SeoDescription;
                //Detail.SeoKeywords = blg.SeoKeywords;
                //Detail.SeoTitle = blg.SeoTitle;
                //Detail.Summary = blg.Summary;
                Detail.Baslik = blg.Baslik;

                db.SaveChanges();
                db.Dispose();
                TempData["Success"] = GeneralFunctions.ReturnSuccess(2);
                ViewBag.Detail = Detail;
                return View();
            }
            catch (Exception)
            {
                TempData["Error"] = GeneralFunctions.ReturnError(2);
                ViewBag.Detail = Detail;
                return View();
            }

        }

        [HttpPost, ValidateInput(false)]
        public ActionResult CreateOzellikLanguage(OzellikLanguages blg, string updatecontrol)
        {
            var LangaugeList = LanguageController.Languages();
            if (LangaugeList.Count > 0)
            {
                ViewBag.LanguageList = LangaugeList;
            }
            try
            {
                if (blg.LanguageId != null)
                {
                    var Detail = (from x in db.OzellikLanguages where x.OdaOzellikId == blg.OdaOzellikId && x.LanguageId == blg.LanguageId select x).SingleOrDefault();

                    if (Detail == null)
                    {

                        var row = new OzellikLanguages
                        {
                            Baslik = blg.Baslik,
                            LanguageId = blg.LanguageId,
                            OzellikId = blg.OzellikId,

                            Status = 1,


                        };

                        db.OzellikLanguages.Add(row);
                        db.SaveChanges();
                        db.Dispose();
                        TempData["Success"] = GeneralFunctions.ReturnSuccess(1);
                        if (!string.IsNullOrEmpty(updatecontrol))
                        {
                            return RedirectToAction("UpdateOzellikLanguage", "Services", new { id = row.OzellikId, LangId = row.LanguageId });
                        }
                        else
                        {
                            return RedirectToAction("CreateOzellikLanguage", "Services", new { id = row.OzellikId });
                        }
                    }
                    else
                    {
                        TempData["Info"] = GeneralFunctions.ReturnInfo(1);
                        if (!string.IsNullOrEmpty(updatecontrol))
                        {
                            return RedirectToAction("UpdateOzellikLanguage", "Services", new { id = blg.OzellikId, LangId = blg.LanguageId });
                        }
                        else
                        {
                            return RedirectToAction("CreateOzellikLanguage", "Services", new { id = blg.OzellikId });
                        }
                    }



                }
                else
                {
                    TempData["Error"] = GeneralFunctions.ReturnError(4);
                    if (!string.IsNullOrEmpty(updatecontrol))
                    {
                        return RedirectToAction("UpdateOzellikLanguage", "Services", new { id = blg.OzellikId, LangId = blg.LanguageId });
                    }
                    else
                    {
                        return RedirectToAction("CreateOzellikLanguage", "Services", new { id = blg.OzellikId });
                    }

                }

            }
            catch (Exception)
            {
                TempData["Error"] = GeneralFunctions.ReturnError(1);
                if (!string.IsNullOrEmpty(updatecontrol))
                {
                    return RedirectToAction("UpdateOzellikLanguage", "Services", new { id = blg.OzellikId, LangId = blg.LanguageId });
                }
                else
                {
                    return RedirectToAction("CreateOzellikLanguage", "Services", new { id = blg.OzellikId });
                }

            }
        }



        //--- ODA MADDELERİ ---
        public ActionResult MaddeNames()
        {
            var MaddeNameList = (from x in db.MaddeNames orderby x.OrderNumber ascending select x).ToList();

            ViewBag.LanguageList = LanguageController.Languages();

            return View(MaddeNameList);
        }

        public ActionResult CreateMaddeNames()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CreateMaddeNames(MaddeNames bln)
        {
            try
            {
                var row = new MaddeNames
                {
                    Status = 1,
                    Name = bln.Name,
                    OrderNumber = 0,
                    InsertedDate = DateTime.Now,
                    ServiceId = bln.ServiceId
                };
                db.MaddeNames.Add(row);
                db.SaveChanges();
                TempData["Success"] = GeneralFunctions.ReturnSuccess(1);

                return RedirectToAction("MaddeNames", "Services", new { id = row.ServiceId });
            }
            catch (Exception)
            {
                TempData["Error"] = GeneralFunctions.ReturnError(1);
                return View();
            }

        }

        public ActionResult UpdateMaddeNames(int id)
        {
            var Detail = (from x in db.MaddeNames where x.MaddeId == id select x).SingleOrDefault();

            return View(Detail);
        }

        [HttpPost]
        public ActionResult UpdateMaddeNames(int id, MaddeNames adn)
        {
            var Detail = (from x in db.MaddeNames where x.MaddeId == id select x).SingleOrDefault();
            try
            {
                Detail.Name = adn.Name;
                db.SaveChanges();
                db.Dispose();
                TempData["Success"] = GeneralFunctions.ReturnSuccess(2);
                return View(Detail);
            }
            catch (Exception)
            {
                TempData["Error"] = GeneralFunctions.ReturnError(2);
                return View(Detail);
            }


        }

        public ActionResult UpdateMaddeLanguage(int id)
        {
            int LanguageId = int.Parse(Request.QueryString["LangId"]);
            var Detail = (from x in db.MaddeLanguages where x.MaddeId == id && x.LanguageId == LanguageId select x).SingleOrDefault();
            if (Detail != null)
            {
                ViewBag.Detail = Detail;
                return View();
            }
            else
            {
                return View();
            }

        }

        [HttpPost, ValidateInput(false)]
        public ActionResult UpdateMaddeLanguage(int id, MaddeLanguages blg)
        {
            int LanguageId = int.Parse(Request.QueryString["LangId"]);
            var Detail = (from x in db.MaddeLanguages where x.MaddeId == id && x.LanguageId == LanguageId select x).SingleOrDefault();
            try
            {
                //string logo = string.Empty;
                //if (file != null)
                //{
                //    var extension = file.FileName.Substring(file.FileName.LastIndexOf('.'));
                //    logo = Guid.NewGuid().ToString() + extension;
                //    var imageToResize = System.Drawing.Image.FromStream(file.InputStream);
                //    ImageSave.InsertImage(imageToResize, logo);
                //    Detail.ImageName = logo;
                //}

                //Detail.NewsDate = blg.NewsDate;
                //Detail.Description = blg.Description;
                //Detail.SeoDescription = blg.SeoDescription;
                //Detail.SeoKeywords = blg.SeoKeywords;
                //Detail.SeoTitle = blg.SeoTitle;
                //Detail.Summary = blg.Summary;
                Detail.Baslik = blg.Baslik;

                db.SaveChanges();
                db.Dispose();
                TempData["Success"] = GeneralFunctions.ReturnSuccess(2);
                ViewBag.Detail = Detail;
                return View();
            }
            catch (Exception)
            {
                TempData["Error"] = GeneralFunctions.ReturnError(2);
                ViewBag.Detail = Detail;
                return View();
            }

        }

        [HttpPost, ValidateInput(false)]
        public ActionResult CreateMaddeLanguage(MaddeLanguages blg, string updatecontrol)
        {
            var LangaugeList = LanguageController.Languages();
            if (LangaugeList.Count > 0)
            {
                ViewBag.LanguageList = LangaugeList;
            }
            try
            {
                if (blg.LanguageId != null)
                {
                    var Detail = (from x in db.MaddeLanguages where x.OdaMaddeId == blg.OdaMaddeId && x.LanguageId == blg.LanguageId select x).SingleOrDefault();

                    if (Detail == null)
                    {

                        var row = new MaddeLanguages
                        {
                            Baslik = blg.Baslik,
                            LanguageId = blg.LanguageId,
                            MaddeId = blg.MaddeId,

                            Status = 1,


                        };

                        db.MaddeLanguages.Add(row);
                        db.SaveChanges();
                        db.Dispose();
                        TempData["Success"] = GeneralFunctions.ReturnSuccess(1);
                        if (!string.IsNullOrEmpty(updatecontrol))
                        {
                            return RedirectToAction("UpdateMaddeLanguage", "Services", new { id = row.MaddeId, LangId = row.LanguageId });
                        }
                        else
                        {
                            return RedirectToAction("CreateMaddeLanguage", "Services", new { id = row.MaddeId });
                        }
                    }
                    else
                    {
                        TempData["Info"] = GeneralFunctions.ReturnInfo(1);
                        if (!string.IsNullOrEmpty(updatecontrol))
                        {
                            return RedirectToAction("UpdateMaddeLanguage", "Services", new { id = blg.MaddeId, LangId = blg.LanguageId });
                        }
                        else
                        {
                            return RedirectToAction("CreateMaddeLanguage", "Services", new { id = blg.MaddeId });
                        }
                    }



                }
                else
                {
                    TempData["Error"] = GeneralFunctions.ReturnError(4);
                    if (!string.IsNullOrEmpty(updatecontrol))
                    {
                        return RedirectToAction("UpdateMaddeLanguage", "Services", new { id = blg.MaddeId, LangId = blg.LanguageId });
                    }
                    else
                    {
                        return RedirectToAction("CreateMaddeLanguage", "Services", new { id = blg.MaddeId });
                    }

                }

            }
            catch (Exception)
            {
                TempData["Error"] = GeneralFunctions.ReturnError(1);
                if (!string.IsNullOrEmpty(updatecontrol))
                {
                    return RedirectToAction("UpdateMaddeLanguage", "Services", new { id = blg.MaddeId, LangId = blg.LanguageId });
                }
                else
                {
                    return RedirectToAction("CreateMaddeLanguage", "Services", new { id = blg.MaddeId });
                }

            }
        }

        public ActionResult MaddeNameDelete(int id)
        {
            var detail = (from x in db.MaddeNames where x.MaddeId == id select x).FirstOrDefault();
            int maddeid = detail.ServiceId.Value;
            var MaddeLanguages = (from x in db.MaddeLanguages where x.MaddeId == detail.MaddeId select x).ToList();
            if (detail != null)
            {
                if (MaddeLanguages.Count > 0)
                {
                    foreach (var item in MaddeLanguages)
                    {
                        db.MaddeLanguages.Remove(item);
                    }
                }

                db.MaddeNames.Remove(detail);

                db.SaveChanges();
            }
            db.Dispose();
            return RedirectToAction("MaddeNames", "Services", new { id = maddeid });
        }




        // --- ODA FİYATLARI ---
        public ActionResult FiyatNames()
        {
            var FiyatNameList = (from x in db.FiyatNames orderby x.OrderNumber ascending select x).ToList();

            ViewBag.LanguageList = LanguageController.Languages();

            return View(FiyatNameList);
        }

        public ActionResult CreateFiyatNames()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CreateFiyatNames(FiyatNames bln)
        {
            try
            {
                var row = new FiyatNames
                {
                    Status = 1,
                    Name = bln.Name,
                    OrderNumber = 0,
                    InsertedDate = DateTime.Now,
                    ServiceId = bln.ServiceId
                };
                db.FiyatNames.Add(row);
                db.SaveChanges();
                TempData["Success"] = GeneralFunctions.ReturnSuccess(1);

                return RedirectToAction("FiyatNames", "Services", new { id = row.ServiceId });
            }
            catch (Exception)
            {
                TempData["Error"] = GeneralFunctions.ReturnError(1);
                return View();
            }

        }

        public ActionResult UpdateFiyatNames(int id)
        {
            var Detail = (from x in db.FiyatNames where x.FiyatId == id select x).SingleOrDefault();

            return View(Detail);
        }

        [HttpPost]
        public ActionResult UpdateFiyatNames(int id, FiyatNames adn)
        {
            var Detail = (from x in db.FiyatNames where x.FiyatId == id select x).SingleOrDefault();
            try
            {
                Detail.Name = adn.Name;
                db.SaveChanges();
                db.Dispose();
                TempData["Success"] = GeneralFunctions.ReturnSuccess(2);
                return View(Detail);
            }
            catch (Exception)
            {
                TempData["Error"] = GeneralFunctions.ReturnError(2);
                return View(Detail);
            }


        }

        public ActionResult UpdateFiyatLanguage(int id)
        {
            int LanguageId = int.Parse(Request.QueryString["LangId"]);
            var Detail = (from x in db.FiyatLanguages where x.FiyatId == id && x.LanguageId == LanguageId select x).SingleOrDefault();
            if (Detail != null)
            {
                ViewBag.Detail = Detail;
                return View();
            }
            else
            {
                return View();
            }

        }

        [HttpPost, ValidateInput(false)]
        public ActionResult UpdateFiyatLanguage(int id, FiyatLanguages blg)
        {
            int LanguageId = int.Parse(Request.QueryString["LangId"]);
            var Detail = (from x in db.FiyatLanguages where x.FiyatId == id && x.LanguageId == LanguageId select x).SingleOrDefault();
            try
            {
                Detail.Baslik = blg.Baslik;
                Detail.Fiyat = blg.Fiyat;
                Detail.Tarih1 = blg.Tarih1;
                Detail.Tarih2 = blg.Tarih2;

                db.SaveChanges();
                db.Dispose();
                TempData["Success"] = GeneralFunctions.ReturnSuccess(2);
                ViewBag.Detail = Detail;
                return View();
            }
            catch (Exception)
            {
                TempData["Error"] = GeneralFunctions.ReturnError(2);
                ViewBag.Detail = Detail;
                return View();
            }

        }

        [HttpPost, ValidateInput(false)]
        public ActionResult CreateFiyatLanguage(FiyatLanguages blg, string updatecontrol)
        {
            var LangaugeList = LanguageController.Languages();
            if (LangaugeList.Count > 0)
            {
                ViewBag.LanguageList = LangaugeList;
            }
            try
            {
                if (blg.LanguageId != null)
                {
                    var Detail = (from x in db.FiyatLanguages where x.OdaFiyatId == blg.OdaFiyatId && x.LanguageId == blg.LanguageId select x).SingleOrDefault();

                    if (Detail == null)
                    {

                        var row = new FiyatLanguages
                        {
                            Baslik = blg.Baslik,
                            LanguageId = blg.LanguageId,
                            FiyatId = blg.FiyatId,
                            Fiyat = blg.Fiyat,
                            Tarih1 = blg.Tarih1,
                            Tarih2 = blg.Tarih2,

                            Status = 1,


                        };

                        db.FiyatLanguages.Add(row);
                        db.SaveChanges();
                        db.Dispose();
                        TempData["Success"] = GeneralFunctions.ReturnSuccess(1);
                        if (!string.IsNullOrEmpty(updatecontrol))
                        {
                            return RedirectToAction("UpdateFiyatLanguage", "Services", new { id = row.FiyatId, LangId = row.LanguageId });
                        }
                        else
                        {
                            return RedirectToAction("CreateFiyatLanguage", "Services", new { id = row.FiyatId });
                        }
                    }
                    else
                    {
                        TempData["Info"] = GeneralFunctions.ReturnInfo(1);
                        if (!string.IsNullOrEmpty(updatecontrol))
                        {
                            return RedirectToAction("UpdateFiyatLanguage", "Services", new { id = blg.FiyatId, LangId = blg.LanguageId });
                        }
                        else
                        {
                            return RedirectToAction("CreateFiyatLanguage", "Services", new { id = blg.FiyatId });
                        }
                    }



                }
                else
                {
                    TempData["Error"] = GeneralFunctions.ReturnError(4);
                    if (!string.IsNullOrEmpty(updatecontrol))
                    {
                        return RedirectToAction("UpdateFiyatLanguage", "Services", new { id = blg.FiyatId, LangId = blg.LanguageId });
                    }
                    else
                    {
                        return RedirectToAction("CreateFiyatLanguage", "Services", new { id = blg.FiyatId });
                    }

                }

            }
            catch (Exception)
            {
                TempData["Error"] = GeneralFunctions.ReturnError(1);
                if (!string.IsNullOrEmpty(updatecontrol))
                {
                    return RedirectToAction("UpdateFiyatLanguage", "Services", new { id = blg.FiyatId, LangId = blg.LanguageId });
                }
                else
                {
                    return RedirectToAction("CreateFiyatLanguage", "Services", new { id = blg.FiyatId });
                }

            }
        }

        public ActionResult FiyatNameDelete(int id)
        {
            var detail = (from x in db.FiyatNames where x.FiyatId == id select x).FirstOrDefault();
            int fiyatid = detail.ServiceId.Value;
            var FiyatLanguages = (from x in db.FiyatLanguages where x.FiyatId == detail.FiyatId select x).ToList();
            if (detail != null)
            {
                if (FiyatLanguages.Count > 0)
                {
                    foreach (var item in FiyatLanguages)
                    {
                        db.FiyatLanguages.Remove(item);
                    }
                }

                db.FiyatNames.Remove(detail);

                db.SaveChanges();
            }
            db.Dispose();
            return RedirectToAction("FiyatNames", "Services", new { id = fiyatid });
        }





        // ODA RESİMLERİ
        public ActionResult ServiceImages(int id)
        {
            var List = (from x in db.ServiceLanguages join g in db.ServiceImageLanguages on x.ServicesLanguageID equals g.ServiceLanguageId where x.ServiceID == id orderby g.OrderNumber ascending select new { x, g }).ToList();

            ViewBag.ServiceLanguageList = (from x in db.ServiceLanguages where x.ServiceID == id select x).ToList();


            var ImageList = new List<ServiceImageLanguages>();

            foreach (var item in List)
            {
                ImageList.Add(new ServiceImageLanguages
                {
                    CoverPic = item.g.CoverPic,
                    Description = item.g.Description,
                    ServiceImageLanguageId = item.g.ServiceImageLanguageId,
                    ServiceLanguageId = item.g.ServiceLanguageId,
                    ImageName = item.g.ImageName,
                    InsertedDate = item.g.InsertedDate,
                    OrderNumber = item.g.OrderNumber,
                    Status = item.g.Status,
                    Title = item.g.Title
                });
            }

            return View(ImageList);
        }

        public ActionResult CreateServiceImage(int id)
        {
            var ServiceLanguageList = (from x in db.ServiceLanguages where x.ServiceID == id select x).ToList();

            ViewBag.ServiceLanguageList = ServiceLanguageList;

            return View();
        }

        [HttpPost]
        public ActionResult CreateServiceImage(int id, ServiceImageLanguages ciml, HttpPostedFileBase[] file)
        {
            var ServiceLanguageList = (from x in db.ServiceLanguages where x.ServiceID == id select x).ToList();

            ViewBag.ServiceLanguageList = ServiceLanguageList;
            try
            {
                if (file != null)
                {
                    foreach (var item in file)
                    {
                        string image = string.Empty;

                        var extension = item.FileName.Substring(item.FileName.LastIndexOf('.'));
                        image = Guid.NewGuid().ToString() + extension;
                        var imageToResize = System.Drawing.Image.FromStream(item.InputStream);
                        ImageSave.InsertImage(imageToResize, image);


                        var row = new ServiceImageLanguages
                        {
                            CoverPic = false,
                            Status = 1,
                            Description = "",
                            ImageName = image,
                            Title = "",
                            ServiceLanguageId = ciml.ServiceLanguageId,
                            

                            OrderNumber = 0,
                            InsertedDate = DateTime.Now
                        };
                        db.ServiceImageLanguages.Add(row);
                        db.SaveChanges();
                        TempData["Success"] = GeneralFunctions.ReturnSuccess(1);
                    }
                    db.Dispose();
                }

                return RedirectToAction("CreateServiceImage", "Services", new { id = id });
            }
            catch (Exception)
            {
                return RedirectToAction("CreateServiceImage", "Services", new { id = id });
            }

        }

        public ActionResult UpdateServiceImage(int id)
        {
            var Detail = (from x in db.ServiceImageLanguages where x.ServiceImageLanguageId == id select x).SingleOrDefault();

            if (Detail != null)
            {
                return View(Detail);
            }
            else
            {
                return View();
            }
        }

        [HttpPost]
        public ActionResult UpdateServiceImage(int id, ServiceImageLanguages ciml, HttpPostedFileBase file, bool? Tab)
        {
            var Detail = (from x in db.ServiceImageLanguages where x.ServiceImageLanguageId == id select x).SingleOrDefault();
            try
            {
                bool TabControl = false;
                if (Tab != null)
                {
                    TabControl = true;
                }
                string image = string.Empty;
                if (file != null)
                {
                    var extension = file.FileName.Substring(file.FileName.LastIndexOf('.'));
                    image = Guid.NewGuid().ToString() + extension;
                    var imageToResize = System.Drawing.Image.FromStream(file.InputStream);
                    ImageSave.InsertImage(imageToResize, image);
                    Detail.ImageName = image;
                }
                if (TabControl)
                {
                    var ServiceImageList = (from x in db.ServiceImageLanguages where x.ServiceLanguageId == Detail.ServiceLanguageId select x).ToList();

                    if (ServiceImageList.Count > 0)
                    {
                        foreach (var item in ServiceImageList)
                        {
                            item.CoverPic = false;
                            db.SaveChanges();
                        }
                    }

                    Detail.CoverPic = true;
                }

                Detail.Description = ciml.Description;
                Detail.Title = ciml.Title;

                db.SaveChanges();
                db.Dispose();
                TempData["Success"] = GeneralFunctions.ReturnSuccess(2);
                return View(Detail);
            }
            catch (Exception)
            {
                TempData["Error"] = GeneralFunctions.ReturnError(2);
                return View(Detail);
            }

        }

        public ActionResult DeleteServiceImage(int id)
        {
            var detail = (from x in db.ServiceImageLanguages where x.ServiceImageLanguageId == id select x).FirstOrDefault();
            var ServiceDetail = (from x in db.ServiceLanguages where x.ServicesLanguageID == detail.ServiceLanguageId select x).SingleOrDefault();
            if (detail != null)
            {
                var imagename = detail.ImageName;
                if (System.IO.File.Exists(Server.MapPath("/media/images/" + imagename)))
                {
                    System.IO.File.Delete(Server.MapPath("/media/images/" + imagename));
                }

                if (System.IO.File.Exists(Server.MapPath("/media/images/thumbnails" + imagename)))
                {
                    System.IO.File.Delete(Server.MapPath("/media/images/thumbnails" + imagename));
                }

                if (System.IO.File.Exists(Server.MapPath("/media/images/thumbnails40" + imagename)))
                {
                    System.IO.File.Delete(Server.MapPath("/media/images/thumbnails40" + imagename));
                }
                db.ServiceImageLanguages.Remove(detail);


                db.SaveChanges();
            }
            db.Dispose();
            return RedirectToAction("ServiceImages", "Services", new { id = ServiceDetail.ServiceID });
        }

        public JsonResult Imagesort(List<string> Ids)
        {
            try
            {
                for (int i = 1; i < Ids.Count + 1; i++)
                {
                    if (!string.IsNullOrEmpty(Ids[i - 1]))
                    {
                        var id = int.Parse(Ids[i - 1]);

                        var Image = (from x in db.ServiceImageLanguages where x.ServiceImageLanguageId == id select x).SingleOrDefault();
                        Image.OrderNumber = i;
                        db.SaveChanges();
                    }

                }
                db.Dispose();
                return Json(new { Code = 200 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { Code = 500 }, JsonRequestBehavior.AllowGet);
            }
        }


        [Serializable]
        public class ChangeStatus1
        {
            public int id { get; set; }
            public int Status { get; set; }
        }

        public ActionResult OzellikNameDelete(int id)
        {
            var detail = (from x in db.OzellikNames where x.OzellikId == id select x).FirstOrDefault();
            int serviceid = detail.ServiceId.Value;
            var OzellikLanguages = (from x in db.OzellikLanguages where x.OzellikId == detail.OzellikId select x).ToList();
            if (detail != null)
            {
                if (OzellikLanguages.Count > 0)
                {
                    foreach (var item in OzellikLanguages)
                    {
                        db.OzellikLanguages.Remove(item);
                    }
                }

                db.OzellikNames.Remove(detail);

                db.SaveChanges();
            }
            db.Dispose();
            return RedirectToAction("OzellikNames", "Services", new { id = serviceid });
        }


        public JsonResult ozellikSort(List<string> Ids)
        {

            try
            {
                for (int i = 1; i < Ids.Count + 1; i++)
                {
                    var id = int.Parse(Ids[i - 1]);
                    var Ozelliks = (from x in db.OzellikNames where x.OzellikId == id select x).SingleOrDefault();
                    Ozelliks.OrderNumber = i;
                    db.SaveChanges();
                }
                db.Dispose();
                return Json(new { Code = 200 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { Code = 500 }, JsonRequestBehavior.AllowGet);
            }
        }

    }

}
