﻿using Lissiya_Otel.Functions;
using Lissiya_Otel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Lissiya_Otel.Areas.Admin.Controllers
{
    public class SponsorController : Controller
    {
        //
        // GET: /Admin/Sponsor/
        lissiyaEntities db = new lissiyaEntities();

        public ActionResult SponsorNames()
        {
            var SponsorNameList = (from x in db.SponsorNames orderby x.OrderNumber ascending select x).ToList();

            ViewBag.LanguageList = LanguageController.Languages();

            return View(SponsorNameList);
        }

        public ActionResult CreateSponsorNames()
        {

            return View();
        }

        public ActionResult CreateSponsorLanguage()
        {
            var LangaugeList = LanguageController.Languages();
            if (LangaugeList.Count > 0)
            {
                ViewBag.LanguageList = LangaugeList;
            }
            return View();
        }

        [HttpPost]
        public ActionResult CreateSponsorNames(SponsorNames adn)
        {
            try
            {
                var row = new SponsorNames
                {
                    Status = 1,
                    Name = adn.Name,
                    OrderNumber = 0,
                    InsertedDate = DateTime.Now
                };
                db.SponsorNames.Add(row);
                db.SaveChanges();
                TempData["Success"] = GeneralFunctions.ReturnSuccess(1);

                return RedirectToAction("CreateSponsorLanguage", "Sponsor", new { id = row.SponsorId });
            }
            catch (Exception)
            {
                TempData["Error"] = GeneralFunctions.ReturnError(1);
                return View();
            }

        }

        [HttpPost, ValidateInput(false)]
        public ActionResult CreateSponsorLanguage(SponsorLanguages spl, string updatecontrol, HttpPostedFileBase file)
        {
            var LangaugeList = LanguageController.Languages();
            if (LangaugeList.Count > 0)
            {
                ViewBag.LanguageList = LangaugeList;
            }
            try
            {
                if (spl.LanguageId != null)
                {
                    var Detail = (from x in db.SponsorLanguages where x.SponsorId == spl.SponsorId && x.LanguageId == spl.LanguageId select x).SingleOrDefault();

                    if (Detail == null)
                    {
                        string logo = string.Empty;
                        if (file != null)
                        {
                            var extension = file.FileName.Substring(file.FileName.LastIndexOf('.'));
                            logo = Guid.NewGuid().ToString() + extension;
                            var imageToResize = System.Drawing.Image.FromStream(file.InputStream);
                            ImageSave.InsertImage(imageToResize, logo);
                        }

                        var row = new SponsorLanguages
                        {
                            Description = spl.Description,
                            InsertedDate = DateTime.Now,
                            LanguageId = spl.LanguageId,
                            Summary = spl.Summary,
                            SponsorId = spl.SponsorId,
                            ImageName = logo,
                            OrderNumber = 0,
                            Status = 1,
                            Title = spl.Title
                        };

                        db.SponsorLanguages.Add(row);
                        db.SaveChanges();
                        db.Dispose();
                        TempData["Success"] = GeneralFunctions.ReturnSuccess(1);
                        if (!string.IsNullOrEmpty(updatecontrol))
                        {
                            return RedirectToAction("UpdateSponsorLanguage", "Sponsor", new { id = row.SponsorId, LangId = row.LanguageId });
                        }
                        else
                        {
                            return RedirectToAction("CreateSponsorLanguage", "Sponsor", new { id = row.SponsorId });
                        }

                    }
                    else
                    {
                        TempData["Info"] = GeneralFunctions.ReturnInfo(1);
                        if (!string.IsNullOrEmpty(updatecontrol))
                        {
                            return RedirectToAction("UpdateSponsorLanguage", "Sponsor", new { id = spl.SponsorId, LangId = spl.LanguageId });
                        }
                        else
                        {
                            return RedirectToAction("CreateSponsorLanguage", "Sponsor", new { id = spl.SponsorId });
                        }
                    }
                    
                }
                else
                {
                    TempData["Error"] = GeneralFunctions.ReturnError(4);
                    if (!string.IsNullOrEmpty(updatecontrol))
                    {
                        return RedirectToAction("UpdateSponsorLanguage", "Sponsor", new { id = spl.SponsorId, LangId = spl.LanguageId });
                    }
                    else
                    {
                        return RedirectToAction("CreateSponsorLanguage", "Sponsor", new { id = spl.SponsorId });
                    }

                }

            }
            catch (Exception)
            {
                TempData["Error"] = GeneralFunctions.ReturnError(1);
                if (!string.IsNullOrEmpty(updatecontrol))
                {
                    return RedirectToAction("UpdateSponsorLanguage", "Sponsor", new { id = spl.SponsorId, LangId = spl.LanguageId });
                }
                else
                {
                    return RedirectToAction("CreateSponsorLanguage", "Sponsor", new { id = spl.SponsorId });
                }

            }
        }

        public ActionResult UpdateSponsorLanguage(int id)
        {
            int LanguageId = int.Parse(Request.QueryString["LangId"]);
            var Detail = (from x in db.SponsorLanguages where x.SponsorId == id && x.LanguageId == LanguageId select x).SingleOrDefault();
            if (Detail != null)
            {
                ViewBag.Detail = Detail;
                return View();
            }
            else
            {
                return View();
            }

        }

        [HttpPost, ValidateInput(false)]
        public ActionResult UpdateSponsorLanguage(int id, SponsorLanguages spl, HttpPostedFileBase file)
        {
            int LanguageId = int.Parse(Request.QueryString["LangId"]);
            var Detail = (from x in db.SponsorLanguages where x.SponsorId == id && x.LanguageId == LanguageId select x).SingleOrDefault();
            try
            {
                string logo = string.Empty;
                if (file != null)
                {
                    var extension = file.FileName.Substring(file.FileName.LastIndexOf('.'));
                    logo = Guid.NewGuid().ToString() + extension;
                    var imageToResize = System.Drawing.Image.FromStream(file.InputStream);
                    ImageSave.InsertImage(imageToResize, logo);
                    Detail.ImageName = logo;
                }

                Detail.Description = spl.Description;
                Detail.Summary = spl.Summary;
                Detail.Title = spl.Title;

                db.SaveChanges();
                db.Dispose();
                TempData["Success"] = GeneralFunctions.ReturnSuccess(2);
                ViewBag.Detail = Detail;
                return View();
            }
            catch (Exception)
            {
                TempData["Error"] = GeneralFunctions.ReturnError(2);
                ViewBag.Detail = Detail;
                return View();
            }

        }

        public ActionResult UpdateSponsorNames(int id)
        {
            var Detail = (from x in db.SponsorNames where x.SponsorId == id select x).SingleOrDefault();

            return View(Detail);
        }

        [HttpPost]
        public ActionResult UpdateSponsorNames(int id, SponsorNames spn)
        {
            var Detail = (from x in db.SponsorNames where x.SponsorId == id select x).SingleOrDefault();
            try
            {

                Detail.Name = spn.Name;

                db.SaveChanges();
                db.Dispose();
                TempData["Success"] = GeneralFunctions.ReturnSuccess(2);
                return View(Detail);
            }
            catch (Exception)
            {
                TempData["Error"] = GeneralFunctions.ReturnError(2);
                return View(Detail);
            }


        }

        public ActionResult Delete(int id)
        {
            var detail = (from x in db.SponsorNames where x.SponsorId == id select x).FirstOrDefault();
            var SponsorLanguagesList = (from x in db.SponsorLanguages where x.SponsorId == id select x).ToList();

            if (detail != null)
            {
                db.SponsorNames.Remove(detail);

                foreach (var item in SponsorLanguagesList)
                {
                    var imagename = item.ImageName;
                    if (System.IO.File.Exists(Server.MapPath("/media/images/" + imagename)))
                    {
                        System.IO.File.Delete(Server.MapPath("/media/images/" + imagename));
                    }

                    if (System.IO.File.Exists(Server.MapPath("/media/images/thumbnails" + imagename)))
                    {
                        System.IO.File.Delete(Server.MapPath("/media/images/thumbnails" + imagename));
                    }

                    if (System.IO.File.Exists(Server.MapPath("/media/images/thumbnails40" + imagename)))
                    {
                        System.IO.File.Delete(Server.MapPath("/media/images/thumbnails40" + imagename));
                    }

                    db.SponsorLanguages.Remove(item);
                }

                db.SaveChanges();
            }
            db.Dispose();
            return RedirectToAction("SponsorNames", "Sponsor");
        }

        public ActionResult DeleteLanguage(int id, int LangId)
        {
            var Detail = (from x in db.SponsorLanguages where x.SponsorId == id && x.LanguageId == LangId select x).SingleOrDefault();

            if (Detail != null)
            {
                db.SponsorLanguages.Remove(Detail);
                db.SaveChanges();
                db.Dispose();
            }

            return RedirectToAction("CreateSponsorLanguage", "Sponsor", new { id = id });
        }

        public JsonResult Sort(List<string> Ids)
        {

            try
            {
                for (int i = 1; i < Ids.Count + 1; i++)
                {
                    var id = int.Parse(Ids[i - 1]);
                    var Sponsor = (from x in db.SponsorNames where x.SponsorId == id select x).SingleOrDefault();
                    Sponsor.OrderNumber = i;
                    db.SaveChanges();
                }
                db.Dispose();
                return Json(new { Code = 200 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { Code = 500 }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult ChangeStatus(ChangeStatus1 chs)
        {
            try
            {
                var Detail = (from x in db.SponsorNames where x.SponsorId == chs.id select x).FirstOrDefault();

                if (chs.Status == 2)
                {
                    Detail.Status = 2;
                }
                else
                {
                    Detail.Status = 1;
                }

                db.SaveChanges();
                db.Dispose();

                return Json(new { Ok = "OK" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { Ok = "BAD" }, JsonRequestBehavior.DenyGet);
            }


        }

        [Serializable]
        public class ChangeStatus1
        {
            public int id { get; set; }
            public int Status { get; set; }
        }

    }
}
