﻿using Lissiya_Otel.Functions;
using Lissiya_Otel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Lissiya_Otel.Areas.Admin.Controllers
{
    public class BlogController : Controller
    {
        // GET: /Admin/Blog/
        lissiyaEntities db = new lissiyaEntities();

        public ActionResult BlogNames()
        {
            var BlogNameList = (from x in db.BlogNames orderby x.OrderNumber ascending select x).ToList();

            ViewBag.LanguageList = LanguageController.Languages();

            return View(BlogNameList);
        }

        public ActionResult CreateBlogNames()
        {
            return View();
        }

        public ActionResult CreateBlogLanguage()
        {
            var LangaugeList = LanguageController.Languages();
            if (LangaugeList.Count > 0)
            {
                ViewBag.LanguageList = LangaugeList;
            }
            return View();
        }

        [HttpPost]
        public ActionResult CreateBlogNames(BlogNames bln)
        {
            try
            {
                var row = new BlogNames
                {
                    Status = 1,
                    Name = bln.Name,
                    OrderNumber = 0,
                    InsertedDate = DateTime.Now
                };
                db.BlogNames.Add(row);
                db.SaveChanges();
                TempData["Success"] = GeneralFunctions.ReturnSuccess(1);

                return RedirectToAction("CreateBlogLanguage", "Blog", new { id = row.BlogId });
            }
            catch (Exception)
            {
                TempData["Error"] = GeneralFunctions.ReturnError(1);
                return View();
            }

        }

        [HttpPost, ValidateInput(false)]
        public ActionResult CreateBlogLanguage(BlogLanguages blg, string updatecontrol, HttpPostedFileBase file)
        {
            var LangaugeList = LanguageController.Languages();
            if (LangaugeList.Count > 0)
            {
                ViewBag.LanguageList = LangaugeList;
            }
            try
            {
                if (blg.LanguageId != null)
                {
                    var Detail = (from x in db.BlogLanguages where x.BlogId == blg.BlogId && x.LanguageId == blg.LanguageId select x).SingleOrDefault();

                    if (Detail == null)
                    {
                        string image = string.Empty;
                        if (file != null)
                        {
                            var extension = file.FileName.Substring(file.FileName.LastIndexOf('.'));
                            image = Guid.NewGuid().ToString() + extension;
                            var imageToResize = System.Drawing.Image.FromStream(file.InputStream);
                            ImageSave.InsertImage(imageToResize, image);
                        }
                        var row = new BlogLanguages
                        {
                            Author = blg.Author,
                            InsertedDate = DateTime.Now,
                            LanguageId = blg.LanguageId,
                            OrderNumber = 0,
                            Title = blg.Title,
                            BlogId = blg.BlogId,
                            Date = blg.Date,
                            Description = blg.Description,
                            Hit = 0,
                            ImageName = image,
                            SeoDescription = blg.SeoDescription,
                            SeoKeywords = blg.SeoKeywords,
                            SeoTitle = blg.SeoTitle,
                            Status = 1,
                            Summary = blg.Summary


                        };

                        db.BlogLanguages.Add(row);
                        db.SaveChanges();
                        db.Dispose();
                        TempData["Success"] = GeneralFunctions.ReturnSuccess(1);
                        if (!string.IsNullOrEmpty(updatecontrol))
                        {
                            return RedirectToAction("UpdateBlogLanguage", "Blog", new { id = row.BlogId, LangId = row.LanguageId });
                        }
                        else
                        {
                            return RedirectToAction("CreateBlogLanguage", "Blog", new { id = row.BlogId });
                        }
                    }
                    else
                    {
                        TempData["Info"] = GeneralFunctions.ReturnInfo(1);
                        if (!string.IsNullOrEmpty(updatecontrol))
                        {
                            return RedirectToAction("UpdateBlogLanguage", "Blog", new { id = blg.BlogId, LangId = blg.LanguageId });
                        }
                        else
                        {
                            return RedirectToAction("CreateBlogLanguage", "Blog", new { id = blg.BlogId });
                        }
                    }

                }
                else
                {
                    TempData["Error"] = GeneralFunctions.ReturnError(4);
                    if (!string.IsNullOrEmpty(updatecontrol))
                    {
                        return RedirectToAction("UpdateBlogLanguage", "Blog", new { id = blg.BlogId, LangId = blg.LanguageId });
                    }
                    else
                    {
                        return RedirectToAction("CreateBlogLanguage", "Blog", new { id = blg.BlogId });
                    }

                }

            }
            catch (Exception)
            {
                TempData["Error"] = GeneralFunctions.ReturnError(1);
                if (!string.IsNullOrEmpty(updatecontrol))
                {
                    return RedirectToAction("UpdateBlogLanguage", "Blog", new { id = blg.BlogId, LangId = blg.LanguageId });
                }
                else
                {
                    return RedirectToAction("CreateBlogLanguage", "Blog", new { id = blg.BlogId });
                }

            }
        }

        public ActionResult UpdateBlogLanguage(int id)
        {
            int LanguageId = int.Parse(Request.QueryString["LangId"]);
            var Detail = (from x in db.BlogLanguages where x.BlogId == id && x.LanguageId == LanguageId select x).SingleOrDefault();
            if (Detail != null)
            {
                ViewBag.Detail = Detail;
                return View();
            }
            else
            {
                return View();
            }

        }

        [HttpPost, ValidateInput(false)]
        public ActionResult UpdateBlogLanguage(int id, BlogLanguages blg, HttpPostedFileBase file)
        {
            int LanguageId = int.Parse(Request.QueryString["LangId"]);
            var Detail = (from x in db.BlogLanguages where x.BlogId == id && x.LanguageId == LanguageId select x).SingleOrDefault();
            try
            {
                string logo = string.Empty;
                if (file != null)
                {
                    var extension = file.FileName.Substring(file.FileName.LastIndexOf('.'));
                    logo = Guid.NewGuid().ToString() + extension;
                    var imageToResize = System.Drawing.Image.FromStream(file.InputStream);
                    ImageSave.InsertImage(imageToResize, logo);
                    Detail.ImageName = logo;
                }

                Detail.Author = blg.Author;
                Detail.Date = blg.Date;
                Detail.Description = blg.Description;
                Detail.SeoDescription = blg.SeoDescription;
                Detail.SeoKeywords = blg.SeoKeywords;
                Detail.SeoTitle = blg.SeoTitle;
                Detail.Summary = blg.Summary;
                Detail.Title = blg.Title;

                db.SaveChanges();
                db.Dispose();
                TempData["Success"] = GeneralFunctions.ReturnSuccess(2);
                ViewBag.Detail = Detail;
                return View();
            }
            catch (Exception)
            {
                TempData["Error"] = GeneralFunctions.ReturnError(2);
                ViewBag.Detail = Detail;
                return View();
            }

        }

        public ActionResult UpdateBlogNames(int id)
        {
            var Detail = (from x in db.BlogNames where x.BlogId == id select x).SingleOrDefault();

            return View(Detail);
        }

        [HttpPost]
        public ActionResult UpdateBlogNames(int id, BlogNames adn)
        {
            var Detail = (from x in db.BlogNames where x.BlogId == id select x).SingleOrDefault();
            try
            {
                Detail.Name = adn.Name;
                db.SaveChanges();
                db.Dispose();
                TempData["Success"] = GeneralFunctions.ReturnSuccess(2);
                return View(Detail);
            }
            catch (Exception)
            {
                TempData["Error"] = GeneralFunctions.ReturnError(2);
                return View(Detail);
            }


        }

        public ActionResult Delete(int id)
        {
            var detail = (from x in db.BlogNames where x.BlogId == id select x).FirstOrDefault();
            var BlogLanguagesList = (from x in db.BlogLanguages where x.BlogId == id select x).ToList();

            if (detail != null)
            {
                db.BlogNames.Remove(detail);

                foreach (var item in BlogLanguagesList)
                {
                    var imagename = item.ImageName;
                    if (System.IO.File.Exists(Server.MapPath("/media/images/" + imagename)))
                    {
                        System.IO.File.Delete(Server.MapPath("/media/images/" + imagename));
                    }

                    if (System.IO.File.Exists(Server.MapPath("/media/images/thumbnails" + imagename)))
                    {
                        System.IO.File.Delete(Server.MapPath("/media/images/thumbnails" + imagename));
                    }

                    if (System.IO.File.Exists(Server.MapPath("/media/images/thumbnails40" + imagename)))
                    {
                        System.IO.File.Delete(Server.MapPath("/media/images/thumbnails40" + imagename));
                    }

                    db.BlogLanguages.Remove(item);
                }

                db.SaveChanges();
            }
            db.Dispose();
            return RedirectToAction("BlogNames", "Blog");
        }

        public ActionResult DeleteLanguage(int id, int LangId)
        {
            var Detail = (from x in db.BlogLanguages where x.BlogId == id && x.LanguageId == LangId select x).SingleOrDefault();

            if (Detail != null)
            {
                db.BlogLanguages.Remove(Detail);
                db.SaveChanges();
                db.Dispose();
            }

            return RedirectToAction("CreateBlogLanguage", "Blog", new { id = id });
        }

        public JsonResult Sort(List<string> Ids)
        {

            try
            {
                for (int i = 1; i < Ids.Count + 1; i++)
                {
                    var id = int.Parse(Ids[i - 1]);
                    var Blog = (from x in db.BlogNames where x.BlogId == id select x).SingleOrDefault();
                    Blog.OrderNumber = i;
                    db.SaveChanges();
                }
                db.Dispose();
                return Json(new { Code = 200 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { Code = 500 }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult ChangeStatus(ChangeStatus1 chs)
        {
            try
            {
                var Detail = (from x in db.BlogNames where x.BlogId == chs.id select x).FirstOrDefault();

                if (chs.Status == 2)
                {
                    Detail.Status = 2;
                }
                else
                {
                    Detail.Status = 1;
                }

                db.SaveChanges();
                db.Dispose();

                return Json(new { Ok = "OK" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { Ok = "BAD" }, JsonRequestBehavior.DenyGet);
            }


        }

        [Serializable]
        public class ChangeStatus1
        {
            public int id { get; set; }
            public int Status { get; set; }
        }

    }
}
