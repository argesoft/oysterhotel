﻿using Lissiya_Otel.Functions;
using Lissiya_Otel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Lissiya_Otel.Areas.Admin.Controllers
{
    public class ClientTestimonialsController : Controller
    {
        //
        // GET: /Admin/ClientTestimonials/

        lissiyaEntities db = new lissiyaEntities();

        public ActionResult ClientList()
        {
            var List = (from x in db.ClientTestimonials orderby x.OrderNumber ascending select x).ToList();

            return View(List);
        }

        public ActionResult CreateClient()
        {
            return View();
        }

        public ActionResult UpdateClient(int id)
        {
            var Detail = (from x in db.ClientTestimonials where x.ClientId == id select x).SingleOrDefault();

            return View(Detail);
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult CreateClient(ClientTestimonials cl, HttpPostedFileBase file)
        {
            try
            {
                if(cl.DilSecenegi == 1)
                {
                    var row = new ClientTestimonials
                    {
                        Status = 1,
                        Client = cl.Client,
                        Testimonial = cl.Testimonial,
                        OrderNumber = 0,
                        InsertedDate = DateTime.Now,
                        Yazar = cl.Yazar,
                        DilSecenegi = 1,
                        CommentDate = cl.CommentDate
                    };
                    db.ClientTestimonials.Add(row);
                    db.SaveChanges();
                    TempData["Success"] = GeneralFunctions.ReturnSuccess(1);
                }
                else
                {
                    var row = new ClientTestimonials
                    {
                        Status = 1,
                        Client = cl.Client,
                        Testimonial = cl.Testimonial,
                        OrderNumber = 0,
                        InsertedDate = DateTime.Now,
                        Yazar = cl.Yazar,
                        DilSecenegi = 2
                    };
                    db.ClientTestimonials.Add(row);
                    db.SaveChanges();
                    TempData["Success"] = GeneralFunctions.ReturnSuccess(1);
                }
                
                
                return View();
            }
            catch (Exception)
            {
                TempData["Error"] = GeneralFunctions.ReturnError(1);
                return View();
            }
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult UpdateClient(int id, ClientTestimonials cl, HttpPostedFileBase file)
        {

            var Detail = (from x in db.ClientTestimonials where x.ClientId == id select x).SingleOrDefault();
            try
            {
                    Detail.Client = cl.Client;
                    Detail.Testimonial = cl.Testimonial;
                    Detail.Yazar = cl.Yazar;
                    Detail.CommentDate = cl.CommentDate;
           
              

                db.SaveChanges();
                db.Dispose();
                TempData["Success"] = GeneralFunctions.ReturnSuccess(2);
                return View(Detail);
            }
            catch (Exception)
            {
                TempData["Error"] = GeneralFunctions.ReturnError(2);

                return View(Detail);
            }
        }

        public ActionResult Delete(int id)
        {
            var detail = (from x in db.ClientTestimonials where x.ClientId == id select x).FirstOrDefault();


            if (detail != null)
            {
                db.ClientTestimonials.Remove(detail);

                db.SaveChanges();
            }
            db.Dispose();
            return RedirectToAction("ClientList", "ClientTestimonials");
        }

        public JsonResult Sort(List<string> Ids)
        {

            try
            {
                for (int i = 1; i < Ids.Count + 1; i++)
                {
                    var id = int.Parse(Ids[i - 1]);
                    var Client = (from x in db.ClientTestimonials where x.ClientId == id select x).SingleOrDefault();
                    Client.OrderNumber = i;
                    db.SaveChanges();
                }
                db.Dispose();
                return Json(new { Code = 200 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { Code = 500 }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult ChangeStatus(ChangeStatus1 chs)
        {
            try
            {
                var Detail = (from x in db.ClientTestimonials where x.ClientId == chs.id select x).FirstOrDefault();

                if (chs.Status == 2)
                {
                    Detail.Status = 2;
                }
                else
                {
                    Detail.Status = 1;
                }

                db.SaveChanges();
                db.Dispose();

                return Json(new { Ok = "OK" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { Ok = "BAD" }, JsonRequestBehavior.DenyGet);
            }


        }

        [Serializable]
        public class ChangeStatus1
        {
            public int id { get; set; }
            public int Status { get; set; }
        }

    }
}
