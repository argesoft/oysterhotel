﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Lissiya_Otel.Models;
using Lissiya_Otel.Functions;
using GAConnect;
using System.Data;
using System.Reflection;

namespace Lissiya_Otel.Areas.Admin.Controllers
{

    public class HomeController : Controller
    {
        //
        // GET: /Admin/Home/
        lissiyaEntities db = new lissiyaEntities();

        [Authorization]
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Login()
        {
            return View();
        }

        [Authorization]
        public ActionResult Dashboard()
        {
            if (Session["account"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
           // Response.Headers.Add("X-Frame-Options", "ALLOW-ALL");




            return View();

        }

        [HttpPost]
        public ActionResult Login(string username, string password, bool? Rememberme)
        {
            var UserDetail = (from x in db.LoginAccounts where x.LoginName == username && x.LoginPassword == password select x).SingleOrDefault();

            if (UserDetail != null)
            {
                if (Rememberme != null)
                {
                    HttpCookie cookie = new HttpCookie("rememberme");
                    cookie.Values["username"] = UserDetail.LoginName;
                    cookie.Values["password"] = UserDetail.LoginPassword;
                    cookie.Expires = DateTime.Now.AddDays(1);
                    HttpContext.Response.Cookies.Add(cookie);
                }
                Session["account"] = UserDetail;
                UserDetail.LoginDate = DateTime.Now;
                UserDetail.Ip = Request.ServerVariables["REMOTE_ADDR"];
                db.SaveChanges();
                return RedirectToAction("Dashboard", "Home");
            }
            else
            {
                ViewBag.Error = "Kullanıcı adı veya şifre hatalı";
                return View();
            }

        }

        public ActionResult Logout()
        {
            if (Request.Cookies["rememberme"] != null)
            {
                Response.Cookies["rememberme"].Expires = DateTime.Now.AddDays(-1);
            }

            Session.Clear();
            Session.Abandon();
            return RedirectToAction("Login", "Home");
        }

        public ActionResult GoogleAnalytics()
        {
            return View();
        }

        [HttpPost]
        public ActionResult GoogleAnalytics(string FirstDate, string SecondDate, string Dimensions, string Metrics, int Count)
        {
            DateTime fromDate = DateTime.Now.AddMonths(-1);
            DateTime toDate = DateTime.Now;
            if (!string.IsNullOrEmpty(FirstDate))
            {
                fromDate = DateTime.Parse(FirstDate);
            }
            if (!string.IsNullOrEmpty(SecondDate))
            {
                toDate = DateTime.Parse(SecondDate);
            }


            string email = GeneralFunctions.GetSettingValue("GoogleMail");
            string password = GeneralFunctions.GetSettingValue("GooglePassword");
            var gaDataFetcher = new GADataFetcher(email, password);
            var MetricList = new List<Metric>();
            var DimensionList = new List<Dimension>();

            Metrics = Metrics.Substring(0, Metrics.Length - 1);
            Dimensions = Dimensions.Substring(0, Dimensions.Length - 1);
            string[] MetricSplit = Metrics.Split(',');
            string[] DimensionSplit = Dimensions.Split(',');
            for (int i = 0; i < MetricSplit.Length; i++)
            {
                MetricList.Add((Metric)Enum.Parse(typeof(Metric), MetricSplit[i].ToString()));
            }

            for (int i = 0; i < DimensionSplit.Length; i++)
            {
                DimensionList.Add((Dimension)Enum.Parse(typeof(Dimension), DimensionSplit[i].ToString()));
            }
            string TableId = GeneralFunctions.GetSettingValue("GoogleTableId");
            IEnumerable<GAData> data = gaDataFetcher.GetAnalytics(TableId, fromDate, toDate, Count, DimensionList, MetricList, MetricList[0], GAConnect.SortDirection.Descending);

            DataTable dt = ToDataTable<GAData>(data);
            string dcCaption;
            List<DataColumn> unwantedColumns = new List<DataColumn>();
            foreach (DataColumn dc in dt.Columns)
            {
                dcCaption = dc.ToString().Substring(0, 1).ToLower() + dc.ToString().Substring(1);
                if (dcCaption == "ısMobile")
                {
                    if (!DimensionList.Contains((Dimension)Enum.Parse(typeof(Dimension), "isMobile")))
                        unwantedColumns.Add(dc);
                }
                if (Enum.IsDefined(typeof(Dimension), dcCaption))
                {
                    if (!DimensionList.Contains((Dimension)Enum.Parse(typeof(Dimension), dcCaption)))
                        unwantedColumns.Add(dc);
                }
                else if (Enum.IsDefined(typeof(Metric), dcCaption))
                {
                    if (!MetricList.Contains((Metric)Enum.Parse(typeof(Metric), dcCaption)))
                        unwantedColumns.Add(dc);
                }
            }
            foreach (DataColumn dc in unwantedColumns)
                dt.Columns.Remove(dc);

            ViewBag.Row = dt;

            return View();
        }

        public static DataTable ToDataTable<T>(IEnumerable<T> items)
        {
            var tb = new DataTable(typeof(T).Name);

            PropertyInfo[] props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);

            foreach (var prop in props)
            {
                tb.Columns.Add(prop.Name, prop.PropertyType);
            }
            foreach (var item in items)
            {
                var values = new object[props.Length];
                for (var i = 0; i < props.Length; i++)
                {
                    values[i] = props[i].GetValue(item, null);
                }

                tb.Rows.Add(values);
            }
            return tb;
        }

        public JsonResult EnterDate()
        {
            DateTime fromDate = DateTime.Now.AddDays(-15);
            DateTime toDate = DateTime.Now;

            string email = GeneralFunctions.GetSettingValue("GoogleMail");
            string password = GeneralFunctions.GetSettingValue("GooglePassword");
            var gaDataFetcher = new GADataFetcher(email, password);

            var MetricList = new List<Metric>();
            var DimensionList = new List<Dimension>();

            MetricList.Add((Metric)Enum.Parse(typeof(Metric), "entrances"));
            DimensionList.Add((Dimension)Enum.Parse(typeof(Dimension), "date"));
            string TableId = GeneralFunctions.GetSettingValue("GoogleTableId");
            IEnumerable<GAData> data = gaDataFetcher.GetAnalytics(TableId, fromDate, toDate, 50, DimensionList, MetricList, MetricList[0], GAConnect.SortDirection.Descending);

            DataTable dt = ToDataTable<GAData>(data);
            string dcCaption;
            List<DataColumn> unwantedColumns = new List<DataColumn>();
            foreach (DataColumn dc in dt.Columns)
            {
                dcCaption = dc.ToString().Substring(0, 1).ToLower() + dc.ToString().Substring(1);
                if (dcCaption == "ısMobile")
                {
                    if (!DimensionList.Contains((Dimension)Enum.Parse(typeof(Dimension), "isMobile")))
                        unwantedColumns.Add(dc);
                }
                if (Enum.IsDefined(typeof(Dimension), dcCaption))
                {
                    if (!DimensionList.Contains((Dimension)Enum.Parse(typeof(Dimension), dcCaption)))
                        unwantedColumns.Add(dc);
                }
                else if (Enum.IsDefined(typeof(Metric), dcCaption))
                {
                    if (!MetricList.Contains((Metric)Enum.Parse(typeof(Metric), dcCaption)))
                        unwantedColumns.Add(dc);
                }
            }
            foreach (DataColumn dc in unwantedColumns)
                dt.Columns.Remove(dc);

            List<EnteranceDate> listenter = new List<EnteranceDate>();
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    var newEntr = new EnteranceDate();
                    newEntr.entrances = int.Parse(dt.Rows[i][0].ToString());
                    string year = dt.Rows[i][1].ToString().Substring(0, 4);
                    string month = dt.Rows[i][1].ToString().Substring(4, 2);
                    string day = dt.Rows[i][1].ToString().Substring(6, 2);
                    string result = year + "-" + month + "-" + day;
                    newEntr.date = result;

                    listenter.Add(newEntr);
                }
            }

            return Json(new { data = listenter }, JsonRequestBehavior.AllowGet);
        }

        [Serializable]
        public class EnteranceDate
        {
            public int entrances { get; set; }
            public string date { get; set; }
        }

        public JsonResult PageViewsbyPage()
        {
            DateTime fromDate = DateTime.Now.AddDays(-7);
            DateTime toDate = DateTime.Now;

            string email = GeneralFunctions.GetSettingValue("GoogleMail");
            string password = GeneralFunctions.GetSettingValue("GooglePassword");
            var gaDataFetcher = new GADataFetcher(email, password);

            var MetricList = new List<Metric>();
            var DimensionList = new List<Dimension>();

            MetricList.Add((Metric)Enum.Parse(typeof(Metric), "pageviews"));
            DimensionList.Add((Dimension)Enum.Parse(typeof(Dimension), "pageTitle"));
            string TableId = GeneralFunctions.GetSettingValue("GoogleTableId");
            IEnumerable<GAData> data = gaDataFetcher.GetAnalytics(TableId, fromDate, toDate, 7, DimensionList, MetricList, MetricList[0], GAConnect.SortDirection.Descending);

            DataTable dt = ToDataTable<GAData>(data);
            string dcCaption;
            List<DataColumn> unwantedColumns = new List<DataColumn>();
            foreach (DataColumn dc in dt.Columns)
            {
                dcCaption = dc.ToString().Substring(0, 1).ToLower() + dc.ToString().Substring(1);
                if (dcCaption == "ısMobile")
                {
                    if (!DimensionList.Contains((Dimension)Enum.Parse(typeof(Dimension), "isMobile")))
                        unwantedColumns.Add(dc);
                }
                if (Enum.IsDefined(typeof(Dimension), dcCaption))
                {
                    if (!DimensionList.Contains((Dimension)Enum.Parse(typeof(Dimension), dcCaption)))
                        unwantedColumns.Add(dc);
                }
                else if (Enum.IsDefined(typeof(Metric), dcCaption))
                {
                    if (!MetricList.Contains((Metric)Enum.Parse(typeof(Metric), dcCaption)))
                        unwantedColumns.Add(dc);
                }
            }
            foreach (DataColumn dc in unwantedColumns)
                dt.Columns.Remove(dc);

            List<PageViewPage> list = new List<PageViewPage>();
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    var newpageview = new PageViewPage();
                    newpageview.pagetitle = dt.Rows[i][1].ToString();
                    newpageview.count = int.Parse(dt.Rows[i][0].ToString()); ;

                    list.Add(newpageview);
                }
            }

            return Json(new { data = list }, JsonRequestBehavior.AllowGet);
        }

        [Serializable]
        public class PageViewPage
        {
            public string pagetitle { get; set; }
            public int count { get; set; }
        }


        public JsonResult NewVisits()
        {
            DateTime fromDate = DateTime.Now.AddDays(-7);
            DateTime toDate = DateTime.Now;

            string email = GeneralFunctions.GetSettingValue("GoogleMail");
            string password = GeneralFunctions.GetSettingValue("GooglePassword");
            var gaDataFetcher = new GADataFetcher(email, password);

            var MetricList = new List<Metric>();
            var DimensionList = new List<Dimension>();

            MetricList.Add((Metric)Enum.Parse(typeof(Metric), "newVisits"));
            DimensionList.Add((Dimension)Enum.Parse(typeof(Dimension), "date"));
            string TableId = GeneralFunctions.GetSettingValue("GoogleTableId");
            IEnumerable<GAData> data = gaDataFetcher.GetAnalytics(TableId, fromDate, toDate, 7, DimensionList, MetricList, MetricList[0], GAConnect.SortDirection.Descending);

            DataTable dt = ToDataTable<GAData>(data);
            string dcCaption;
            List<DataColumn> unwantedColumns = new List<DataColumn>();
            foreach (DataColumn dc in dt.Columns)
            {
                dcCaption = dc.ToString().Substring(0, 1).ToLower() + dc.ToString().Substring(1);
                if (dcCaption == "ısMobile")
                {
                    if (!DimensionList.Contains((Dimension)Enum.Parse(typeof(Dimension), "isMobile")))
                        unwantedColumns.Add(dc);
                }
                if (Enum.IsDefined(typeof(Dimension), dcCaption))
                {
                    if (!DimensionList.Contains((Dimension)Enum.Parse(typeof(Dimension), dcCaption)))
                        unwantedColumns.Add(dc);
                }
                else if (Enum.IsDefined(typeof(Metric), dcCaption))
                {
                    if (!MetricList.Contains((Metric)Enum.Parse(typeof(Metric), dcCaption)))
                        unwantedColumns.Add(dc);
                }
            }
            foreach (DataColumn dc in unwantedColumns)
                dt.Columns.Remove(dc);

            List<NewVisit> list = new List<NewVisit>();
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    var newVisit = new NewVisit();
                    newVisit.count = int.Parse(dt.Rows[i][0].ToString());
                    string year = dt.Rows[i][1].ToString().Substring(0, 4);
                    string month = dt.Rows[i][1].ToString().Substring(4, 2);
                    string day = dt.Rows[i][1].ToString().Substring(6, 2);
                    string result = year + "-" + month + "-" + day;
                    newVisit.date = result;

                    list.Add(newVisit);
                }
            }

            return Json(new { data = list }, JsonRequestBehavior.AllowGet);
        }

        [Serializable]
        public class NewVisit
        {
            public string date { get; set; }
            public int count { get; set; }
        }


        public JsonResult VisitbyBrowser()
        {
            DateTime fromDate = DateTime.Now.AddDays(-7);
            DateTime toDate = DateTime.Now;

            string email = GeneralFunctions.GetSettingValue("GoogleMail");
            string password = GeneralFunctions.GetSettingValue("GooglePassword");
            var gaDataFetcher = new GADataFetcher(email, password);

            var MetricList = new List<Metric>();
            var DimensionList = new List<Dimension>();

            MetricList.Add((Metric)Enum.Parse(typeof(Metric), "entrances"));
            DimensionList.Add((Dimension)Enum.Parse(typeof(Dimension), "browser"));
            string TableId = GeneralFunctions.GetSettingValue("GoogleTableId");
            IEnumerable<GAData> data = gaDataFetcher.GetAnalytics(TableId, fromDate, toDate, 10, DimensionList, MetricList, MetricList[0], GAConnect.SortDirection.Descending);

            DataTable dt = ToDataTable<GAData>(data);
            string dcCaption;
            List<DataColumn> unwantedColumns = new List<DataColumn>();
            foreach (DataColumn dc in dt.Columns)
            {
                dcCaption = dc.ToString().Substring(0, 1).ToLower() + dc.ToString().Substring(1);
                if (dcCaption == "ısMobile")
                {
                    if (!DimensionList.Contains((Dimension)Enum.Parse(typeof(Dimension), "isMobile")))
                        unwantedColumns.Add(dc);
                }
                if (Enum.IsDefined(typeof(Dimension), dcCaption))
                {
                    if (!DimensionList.Contains((Dimension)Enum.Parse(typeof(Dimension), dcCaption)))
                        unwantedColumns.Add(dc);
                }
                else if (Enum.IsDefined(typeof(Metric), dcCaption))
                {
                    if (!MetricList.Contains((Metric)Enum.Parse(typeof(Metric), dcCaption)))
                        unwantedColumns.Add(dc);
                }
            }
            foreach (DataColumn dc in unwantedColumns)
                dt.Columns.Remove(dc);

            List<VisitBrowser> list = new List<VisitBrowser>();
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    var browservisit = new VisitBrowser();
                    browservisit.count = int.Parse(dt.Rows[i][0].ToString());
                    browservisit.browser = dt.Rows[i][1].ToString();

                    list.Add(browservisit);
                }
            }

            return Json(new { data = list }, JsonRequestBehavior.AllowGet);
        }

        [Serializable]
        public class VisitBrowser
        {
            public string browser { get; set; }
            public int count { get; set; }
        }

        public JsonResult VisitbyCity()
        {
            DateTime fromDate = DateTime.Now.AddDays(-7);
            DateTime toDate = DateTime.Now;

            string email = GeneralFunctions.GetSettingValue("GoogleMail");
            string password = GeneralFunctions.GetSettingValue("GooglePassword");
            var gaDataFetcher = new GADataFetcher(email, password);

            var MetricList = new List<Metric>();
            var DimensionList = new List<Dimension>();

            MetricList.Add((Metric)Enum.Parse(typeof(Metric), "entrances"));
            DimensionList.Add((Dimension)Enum.Parse(typeof(Dimension), "city"));
            string TableId = GeneralFunctions.GetSettingValue("GoogleTableId");
            IEnumerable<GAData> data = gaDataFetcher.GetAnalytics(TableId, fromDate, toDate, 10, DimensionList, MetricList, MetricList[0], GAConnect.SortDirection.Descending);

            DataTable dt = ToDataTable<GAData>(data);
            string dcCaption;
            List<DataColumn> unwantedColumns = new List<DataColumn>();
            foreach (DataColumn dc in dt.Columns)
            {
                dcCaption = dc.ToString().Substring(0, 1).ToLower() + dc.ToString().Substring(1);
                if (dcCaption == "ısMobile")
                {
                    if (!DimensionList.Contains((Dimension)Enum.Parse(typeof(Dimension), "isMobile")))
                        unwantedColumns.Add(dc);
                }
                if (Enum.IsDefined(typeof(Dimension), dcCaption))
                {
                    if (!DimensionList.Contains((Dimension)Enum.Parse(typeof(Dimension), dcCaption)))
                        unwantedColumns.Add(dc);
                }
                else if (Enum.IsDefined(typeof(Metric), dcCaption))
                {
                    if (!MetricList.Contains((Metric)Enum.Parse(typeof(Metric), dcCaption)))
                        unwantedColumns.Add(dc);
                }
            }
            foreach (DataColumn dc in unwantedColumns)
                dt.Columns.Remove(dc);

            List<VisitCity> list = new List<VisitCity>();
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    var browservisit = new VisitCity();
                    browservisit.count = int.Parse(dt.Rows[i][0].ToString());
                    if (dt.Rows[i][1].ToString() == "(not set)")
                    {
                        browservisit.city = "Diğer";
                    }
                    else
                    {
                        browservisit.city = dt.Rows[i][1].ToString();
                    }


                    list.Add(browservisit);
                }
            }

            return Json(new { data = list }, JsonRequestBehavior.AllowGet);
        }

        [Serializable]
        public class VisitCity
        {
            public string city { get; set; }
            public int count { get; set; }
        }

        public JsonResult TimeOnSiteDate()
        {
            DateTime fromDate = DateTime.Now.AddDays(-15);
            DateTime toDate = DateTime.Now;

            string email = GeneralFunctions.GetSettingValue("GoogleMail");
            string password = GeneralFunctions.GetSettingValue("GooglePassword");
            var gaDataFetcher = new GADataFetcher(email, password);

            var MetricList = new List<Metric>();
            var DimensionList = new List<Dimension>();

            MetricList.Add((Metric)Enum.Parse(typeof(Metric), "timeOnSite"));
            DimensionList.Add((Dimension)Enum.Parse(typeof(Dimension), "date"));
            string TableId = GeneralFunctions.GetSettingValue("GoogleTableId");
            IEnumerable<GAData> data = gaDataFetcher.GetAnalytics(TableId, fromDate, toDate, 50, DimensionList, MetricList, MetricList[0], GAConnect.SortDirection.Descending);

            DataTable dt = ToDataTable<GAData>(data);
            string dcCaption;
            List<DataColumn> unwantedColumns = new List<DataColumn>();
            foreach (DataColumn dc in dt.Columns)
            {
                dcCaption = dc.ToString().Substring(0, 1).ToLower() + dc.ToString().Substring(1);
                if (dcCaption == "ısMobile")
                {
                    if (!DimensionList.Contains((Dimension)Enum.Parse(typeof(Dimension), "isMobile")))
                        unwantedColumns.Add(dc);
                }
                if (Enum.IsDefined(typeof(Dimension), dcCaption))
                {
                    if (!DimensionList.Contains((Dimension)Enum.Parse(typeof(Dimension), dcCaption)))
                        unwantedColumns.Add(dc);
                }
                else if (Enum.IsDefined(typeof(Metric), dcCaption))
                {
                    if (!MetricList.Contains((Metric)Enum.Parse(typeof(Metric), dcCaption)))
                        unwantedColumns.Add(dc);
                }
            }
            foreach (DataColumn dc in unwantedColumns)
                dt.Columns.Remove(dc);

            List<TimeOnSite> listenter = new List<TimeOnSite>();
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    var newEntr = new TimeOnSite();
                    newEntr.time = dt.Rows[i][0].ToString() + "sn";
                    string year = dt.Rows[i][1].ToString().Substring(0, 4);
                    string month = dt.Rows[i][1].ToString().Substring(4, 2);
                    string day = dt.Rows[i][1].ToString().Substring(6, 2);
                    string result = year + "-" + month + "-" + day;
                    newEntr.date = result;

                    listenter.Add(newEntr);
                }
            }

            return Json(new { data = listenter }, JsonRequestBehavior.AllowGet);
        }

        [Serializable]
        public class TimeOnSite
        {
            public string time { get; set; }
            public string date { get; set; }
        }

        public JsonResult TimeOnPage()
        {
            DateTime fromDate = DateTime.Now.AddDays(-15);
            DateTime toDate = DateTime.Now;

            string email = GeneralFunctions.GetSettingValue("GoogleMail");
            string password = GeneralFunctions.GetSettingValue("GooglePassword");
            var gaDataFetcher = new GADataFetcher(email, password);

            var MetricList = new List<Metric>();
            var DimensionList = new List<Dimension>();

            MetricList.Add((Metric)Enum.Parse(typeof(Metric), "timeOnPage"));
            DimensionList.Add((Dimension)Enum.Parse(typeof(Dimension), "pageTitle"));
            string TableId = GeneralFunctions.GetSettingValue("GoogleTableId");
            IEnumerable<GAData> data = gaDataFetcher.GetAnalytics(TableId, fromDate, toDate, 50, DimensionList, MetricList, MetricList[0], GAConnect.SortDirection.Descending);

            DataTable dt = ToDataTable<GAData>(data);
            string dcCaption;
            List<DataColumn> unwantedColumns = new List<DataColumn>();
            foreach (DataColumn dc in dt.Columns)
            {
                dcCaption = dc.ToString().Substring(0, 1).ToLower() + dc.ToString().Substring(1);
                if (dcCaption == "ısMobile")
                {
                    if (!DimensionList.Contains((Dimension)Enum.Parse(typeof(Dimension), "isMobile")))
                        unwantedColumns.Add(dc);
                }
                if (Enum.IsDefined(typeof(Dimension), dcCaption))
                {
                    if (!DimensionList.Contains((Dimension)Enum.Parse(typeof(Dimension), dcCaption)))
                        unwantedColumns.Add(dc);
                }
                else if (Enum.IsDefined(typeof(Metric), dcCaption))
                {
                    if (!MetricList.Contains((Metric)Enum.Parse(typeof(Metric), dcCaption)))
                        unwantedColumns.Add(dc);
                }
            }
            foreach (DataColumn dc in unwantedColumns)
                dt.Columns.Remove(dc);

            List<TimeOnPages> listenter = new List<TimeOnPages>();
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    var newEntr = new TimeOnPages();
                    newEntr.time = dt.Rows[i][0].ToString() + "sn";
                    newEntr.pagetitle = dt.Rows[i][1].ToString();

                    listenter.Add(newEntr);
                }
            }

            return Json(new { data = listenter }, JsonRequestBehavior.AllowGet);
        }

        [Serializable]
        public class TimeOnPages
        {
            public string time { get; set; }
            public string pagetitle { get; set; }
        }
    }
}
