﻿<%@ Page Language="C#" AutoEventWireup="True" CodeBehind="ImageBrowser.aspx.cs" Inherits="ImageBrowser" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
	<title>Resim Tarayıcı</title>
	<style type="text/css">
		body { margin: 0px; }
		form { width:780px;height:700px; background-color: #E3E3C7; }
		h1 { padding: 15px; margin: 0px; padding-bottom: 0px; font-family:Arial; font-size: 14pt; color: #737357; }
	</style>
</head>
<body>
	<form id="form1" runat="server" >
	<div>
		<h1>Resim Tarayıcı</h1>
        
		<table border="1" style="background-color:#F1F1E3; margin:15px; width: 720px; border-spacing: 0px;">
			<tr>
				<td style="width: 396px; vertical-align: middle; text-align: center; padding: 10px;">
					<asp:Image ID="Image1" runat="server" Style="max-height: 450px; max-width: 380px;" />
				</td>
				<td style="width: 324px; vertical-align: top; padding: 10px;">
					Dosyalar:<br />
					<asp:DropDownList ID="DirectoryList" runat="server" Style="width: 160px;" OnSelectedIndexChanged="ChangeDirectory" AutoPostBack="true" />
					<asp:Button ID="DeleteDirectoryButton" runat="server" Text="Sil" OnClick="DeleteFolder" OnClientClick="return confirm('Are you sure you want to delete this folder and all its contents?');" />
					<asp:HiddenField ID="NewDirectoryName" runat="server" />
					<asp:Button ID="NewDirectoryButton" runat="server" Text="Yeni" OnClick="CreateFolder" />
					<br /><br />
							
					<asp:Panel ID="SearchBox" runat="server" DefaultButton="SearchButton">
						Arama Yap:<br />
						<asp:TextBox ID="SearchTerms" runat="server"/>
						<asp:Button ID="SearchButton" runat="server" Text="Ara" OnClick="Search" UseSubmitBehavior="false" />
						<br />
					</asp:Panel>
					<asp:ListBox ID="ImageList" runat="server" Style="width: 280px; height: 180px;" OnSelectedIndexChanged="SelectImage" AutoPostBack="true" />

					<asp:HiddenField ID="NewImageName" runat="server" />
					<asp:Button ID="RenameImageButton" runat="server" Text="Yeni İsim Ver" OnClick="RenameImage" />
					<asp:Button ID="DeleteImageButton" runat="server" Text="Sil" OnClick="DeleteImage" OnClientClick="return confirm('Are you sure you want to delete this image?');" />
					<br />
					<br />
					Boyut:<br />
					<asp:TextBox ID="ResizeWidth" runat="server" Width="50" OnTextChanged="ResizeWidthChanged" />
					x
					<asp:TextBox ID="ResizeHeight" runat="server" Width="50" OnTextChanged="ResizeHeightChanged" />
					<asp:HiddenField ID="ImageAspectRatio" runat="server" />
					<asp:Button ID="ResizeImageButton" runat="server" Text="Boyutlandır" OnClick="ResizeImage" /><br />
					<asp:Label ID="ResizeMessage" runat="server" ForeColor="Red" />
					<br /><br />
					Resim yükle: (Maximum 10 MB)
					<asp:FileUpload ID="UploadedImageFile" runat="server" />
					<asp:Button ID="UploadButton" runat="server" Text="Yükle" OnClick="Upload" /><br />
					<br />
				</td>
			</tr>
		</table>
        
		<div style="text-align: center;">
			<asp:Button ID="OkButton" runat="server" Text="Tamam" OnClick="Clear" />
			<asp:Button ID="CancelButton" runat="server" Text="Vazgeç" OnClientClick="window.top.close(); window.top.opener.focus();" OnClick="Clear" />
			<br /><br />
		</div>
	</div>
	</form>
</body>
</html>
