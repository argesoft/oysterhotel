﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class form : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        bool result = true;
        string langid = Request.QueryString[6];
        try
        {

            string ns = Request.QueryString[0];
            string email = Request.QueryString[1];
            string d1 = Request.QueryString[2];
            string d2 = Request.QueryString[3];
            string count = Request.QueryString[4];
            string message = Request.QueryString[5];

            string host = "smtp.gmail.com";
            int port = 587;
            string username = "test@argesoft.com.tr";
            string password = "argesoft1234";
            string To = "info@lissiyahotel.com";
            int Ssl = 1;
            int Credantials = 1;

            MailMessage mailim = new MailMessage();

            mailim.To.Add(To);  // kime gidecek
            mailim.From = new MailAddress(username);   //kimden gidecek
            mailim.Subject = "Lissiya Hotel Rezervasyon Formu";   // e-mail konusu
            mailim.IsBodyHtml = true;
            mailim.Body = ("<table>" +
                            "<tr>" +
                                "<td>Ad Soyad : </td><td>" + ns + "</td>" +
                            "</tr>" +
                            "<tr>" +
                                "<td>E-Mail : </td><td>" + email + "</td>" +
                            "</tr>" +
                            "<tr>" +
                                "<td>Varış Tarihi : </td><td>" + d1 + "</td>" +
                            "</tr>" +
                              "<tr>" +
                                "<td>Ayrılış Tarihi Tarihi : </td><td>" + d2 + "</td>" +
                            "</tr>" +
                            "<tr>" +
                                "<td>Kişi Sayısı : </td><td>" + count + "</td>" +
                            "</tr>" +
                            "<tr>" +
                            "<td>Mesaj : </td><td>" + message + "</td>" +
                            "</tr>" +
                            "</table>");

            NetworkCredential yetki = new NetworkCredential(username, password);

            SmtpClient mail_client = new SmtpClient(host, port);

            if (Ssl == 1)
            {
                mail_client.EnableSsl = true;
            }
            else
            {
                mail_client.EnableSsl = false;
            }
            if (Credantials == 1)
            {
                mail_client.UseDefaultCredentials = true;
            }
            else
            {
                mail_client.UseDefaultCredentials = false;
            }

            mail_client.Credentials = yetki;
            mail_client.Send(mailim);

        }
        catch (Exception)
        {
            result = false;
        }
        if (result)
        {
            if (langid == "1")
            {
                Response.Redirect("~/rezervasyon.html?return=true");
            }
            else
            {
                Response.Redirect("~/eng/Reservation.html?return=true");
            }

        }
        else
        {
            if (langid == "1")
            {
                Response.Redirect("~/rezervasyon.html?return=false");
            }
            else
            {
                Response.Redirect("~/eng/Reservation.html?return=false");
            }

        }



    }
}