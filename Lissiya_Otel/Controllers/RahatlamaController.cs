﻿using Lissiya_Otel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Lissiya_Otel.Controllers
{
    public class RahatlamaController : Controller
    {
        //
        // GET: /Rahatlama/

        lissiyaEntities db = new lissiyaEntities();

        public ActionResult Index()
        {
            var rahatlama = db.ReferenceLanguages.OrderBy(x => x.OrderNumber).ToList();

            return View(rahatlama);
        }

        public ActionResult Detay(int id)
        {
            var resimler = (from ol in db.ReferenceImageLanguages
                            join ozn in db.ReferenceLanguages on ol.ReferenceLanguageId equals ozn.ReferenceLanguageId
                              where ozn.ReferenceId == id
                              select ol).ToList();
            ViewBag.resimler = resimler;

            // SEO
            ViewBag.rahatlamaSeo = db.ReferenceLanguages.Where(x => x.ReferenceId == id).ToList();

            return View();
        }

    }
}
