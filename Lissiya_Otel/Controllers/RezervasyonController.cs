﻿using Lissiya_Otel.Functions;
using Lissiya_Otel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Lissiya_Otel.Controllers
{
    public class RezervasyonController : Controller
    {
        //
        // GET: /Rezervasyon/

        public ActionResult Index()
        {
            return View();
        }

        lissiyaEntities db = new lissiyaEntities();

        [HttpPost]
        public ActionResult Index(ContactForm frm)
        {
            {
                try
                {
                    var row = new ContactForm
                    {
                        InsertedDate = DateTime.Now,
                        Email = frm.Email,
                        NameSurname = frm.NameSurname,
                        Message = frm.Message,
                        VarisTarihi = frm.VarisTarihi,
                        AyrilisTarihi = frm.AyrilisTarihi,
                        KisiSayisi = frm.KisiSayisi,                        
                    };

                    db.ContactForm.Add(row);
                    db.SaveChanges();
                    db.Dispose();
                    TempData["Success"] = GeneralFunctions.ReturnSuccess(1);

                    GeneralFunctions.SendMail("mail.mckare.com.tr", 587, "eray@mckare.com.tr", "Rzi53ist34", "huseyin@mckare.com.tr", "lissiyahotel", 0, 0, frm.NameSurname, frm.Email, frm.VarisTarihi, frm.AyrilisTarihi, Convert.ToInt32(frm.KisiSayisi), frm.Message);
                    

                    return RedirectToAction("Index", "Rezervasyon", new { id = 1 });
                }

                catch (Exception)
                {

                }
            }

            return View();
        }

    }
}
