﻿using Lissiya_Otel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Lissiya_Otel.Controllers
{
    public class KonaklamaController : Controller
    {
        //
        // GET: /Konaklama/

        lissiyaEntities db = new lissiyaEntities();

        public ActionResult Index()
        {
            var konaklamaYerleri = db.ServiceLanguages.OrderBy(x => x.ServicesLanguageID).ToList();


            return View(konaklamaYerleri);
        }

        public ActionResult Detay(int id)
        {
            // Oda Özellikleri
            //var odaTipleri = db.OzellikNames.Where(x => x.ServiceId == id).OrderBy(x => x.OrderNumber).ToList();
            var odaIsmi = (from ol in db.ServiceLanguages
                              join ozn in db.ServiceNames on ol.ServiceID equals ozn.ServiceId
                              orderby ozn.OrderNumber ascending
                              where ozn.ServiceId == id
                              select ol).ToList();
            ViewBag.odaIsmi = odaIsmi;

            var odaTipleri = (from ol in db.OzellikLanguages
                              join ozn in db.OzellikNames on ol.OzellikId equals ozn.OzellikId orderby ozn.OrderNumber ascending 
                              where ozn.ServiceId == id
                              select ol).ToList();
            ViewBag.odaTipleri = odaTipleri;

            // Oda Maddeleri
            var odaMaddeleri = (from ml in db.MaddeLanguages
                                join mn in db.MaddeNames on ml.MaddeId equals mn.MaddeId orderby mn.OrderNumber ascending 
                                where mn.ServiceId == id
                                select ml).ToList();
            ViewBag.odaMaddeleri = odaMaddeleri;

            // Oda Fiyatları
            var odaFiyatlari = (from ml in db.FiyatLanguages
                                join mn in db.FiyatNames on ml.FiyatId equals mn.FiyatId orderby mn.OrderNumber ascending 
                                where mn.ServiceId == id
                                select ml).ToList();
            ViewBag.odaFiyatlari = odaFiyatlari;


            // Oda Resimleri
            var odaResimleri = (from ol in db.ServiceImageLanguages
                                join ozn in db.ServiceLanguages on ol.ServiceLanguageId equals ozn.ServicesLanguageID orderby ol.OrderNumber ascending 
                            where ozn.ServiceID == id
                                select ol).ToList();
            ViewBag.odaResimleri = odaResimleri;

            // SEO
            ViewBag.odaSeo = db.ServiceLanguages.Where(x => x.ServiceID == id).ToList();

            return View();
        }
    }
}
