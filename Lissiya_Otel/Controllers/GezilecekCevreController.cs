﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Lissiya_Otel.Controllers
{
    public class GezilecekCevreController : Controller
    {
        //
        // GET: /GezilecekCevre/
        Lissiya_Otel.Models.lissiyaEntities db = new Models.lissiyaEntities();
        public ActionResult Index()
        {
            var blogListesi = db.BlogLanguages.OrderBy(x => x.OrderNumber).ToList();
            ViewBag.blogListesi = blogListesi;
            return View();
        }

        public ActionResult Detay(int id)
        {
            var blogDetayListesi = db.BlogLanguages.Where(x => x.BlogId == id) .ToList();
            ViewBag.blogDetayListesi = blogDetayListesi;

            var blogListesi = db.BlogLanguages.OrderBy(x => x.OrderNumber).ToList();
            ViewBag.blogListesi = blogListesi;

            ViewBag.blogSeo = db.BlogLanguages.Where(x => x.BlogId == id).ToList();

            return View();
        }

    }
}
