﻿using Lissiya_Otel.Functions;
using Lissiya_Otel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;

namespace Lissiya_Otel.Controllers
{
    public class IletisimController : Controller
    {
        //
        // GET: /Iletisim/

        lissiyaEntities db = new lissiyaEntities();

        public ActionResult Index()
        {
            var iletisimBilgileri = db.AddressLanguages.Where(x => x.AddressId == 1).ToList();
            return View(iletisimBilgileri);
        }
        [HttpPost]
        public JsonResult Index(ContactForm cf)
        {
          

          
            var deger = MailSend(cf);

            ViewBag.MailSend = deger;



            return Json(new { responseText = "true" }, JsonRequestBehavior.AllowGet);

        }

        private static bool MailSend(ContactForm cf)
        {
            string MailHost = "smtp.gmail.com";
            int MailPort = 587;
            string MailUserName = "oludeniz@oysterresidences.com";
            string MailPassword = "dnz957oyst";
            string ContactMailAddress = GeneralFunctions.GetSettingValue("ContactMail");
            int MailSsl = 1;
            int MailCredantials = 1;

            var deger = SendMail(MailHost, MailPort, MailUserName, MailPassword, ContactMailAddress, MailUserName, MailSsl, MailCredantials, cf.NameSurname, cf.Subject, cf.Email, cf.Message, "","", "", cf.Tel);
            return deger;
        }
        public class ContactForm
        {
     
            public string NameSurname { get; set; }
            public string Subject { get; set; }
            public string Tel { get; set; }
            public string Email { get; set; }
            public string Message { get; set; }
     
        }
        public static bool SendMail(string Host, int Port, string Username, string Password, string To, string From, int Ssl, int Credantials, string ContactNameSurname, string Subject, string MailAddress, string ContactMessage, string Date, string DateGiris, string DateCikis, string Tel)
        {
            try
            {

             

                string host = Host;
                int port = Port;
                string username = Username;
                string password = Password;
                string from = From;
                MailMessage mailim = new MailMessage();

                mailim.To.Add(To);  // kime gidecek
                mailim.From = new MailAddress(username);   //kimden gidecek
                mailim.Subject = "Oyster " + Subject;   // e-mail konusu
                mailim.IsBodyHtml = true;
                mailim.Body = ("<table>" +
                                "<tr>" +
                                    "<td>Ad Soyad: </td><td>" + ContactNameSurname + "</td>" +
                                "</tr>" + "<tr>" +
                                    "<td>Tel: </td><td>" + Tel + "</td>" +
                                "</tr>" +
                                "<tr>" +
                                    "<td>E-Mail: </td><td>" + MailAddress + "</td>" +
                                "</tr>" +
                               
                                "<tr>" +
                                    "<td>Konu: </td><td>" + Subject + "</td>" +
                                "</tr>" +
                                "<tr>" +
                                    "<td> Mesaj: </td><td>" + ContactMessage + "</td>" +
                                "</tr>" +
                                "</table>");   //e-mail içeriği

                NetworkCredential yetki = new NetworkCredential(username, password);

                SmtpClient mail_client = new SmtpClient(host, port);

                if (Ssl == 1)
                {
                    mail_client.EnableSsl = true;
                }
                else
                {
                    mail_client.EnableSsl = false;
                }
                if (Credantials == 1)
                {
                    mail_client.UseDefaultCredentials = true;
                }
                else
                {
                    mail_client.UseDefaultCredentials = false;
                }

                mail_client.Credentials = yetki;
                mail_client.Send(mailim);

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }


    }
}
