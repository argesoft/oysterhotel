﻿using Lissiya_Otel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using Lissiya_Otel.Functions;

namespace Lissiya_Otel.Controllers
{
    public class HomeController : Controller
    {
        private lissiyaEntities db = new lissiyaEntities();

        public ActionResult Index(int? PageID)
        {
            ViewBag.Message = "Site anasayfa.";

            var sliderlar = db.SliderImages.Where(x => x.SliderId == 1).OrderBy(x => x.SliderImageId).ToList();

            ViewBag.Sliderlar = sliderlar;
            if (PageID != null)
            {
                int id = Convert.ToInt32(PageID);

                //var language = Lissiya_Otel.Functions.GeneralFunctions.Resolve(ControllerContext.RouteData.Values["lang"]);
                int lang = Lissiya_Otel.Functions.GeneralFunctions.GetLanguage(ControllerContext.RouteData.Values["lang"]);
                

                var modal = db.ContentLanguages.Where(x => x.ContentNames.TopContentId == id && x.LanguageId == lang).ToList();
                var title = ""; var description = "";
                if (modal != null)
                {
                    foreach (var item in modal)
                    {
                        title = item.Title;
                        description = item.Description;
                    }
                }
                ViewBag.Modaltitle = title;
                ViewBag.Modaldescription = description;
                ViewBag.MenuId = id;

                var mns = db.MenuLanguages.Where(x => x.MenuId == id).ToList();
                int? sliderId = 1;
                foreach (var item in mns)
                {
                    sliderId = item.SliderId;
                }
                var images = db.SliderImages.Where(x => x.SliderId == sliderId).ToList();
                ViewBag.Sliderlar = images;
            }
            ViewBag.menuSpecialsEng = db.MenuLanguages.Where(m => m.MenuId == 40 && m.LanguageId == 2).FirstOrDefault();
            ViewBag.menuReservationsEng = db.MenuLanguages.Where(m => m.MenuId == 29 && m.LanguageId == 2).FirstOrDefault();

            return View();
        }

        [HttpPost]
        public ActionResult Index(ContactForm cf)
        {
            ViewBag.Message = "Site anasayfa.";

            var sliderlar = db.SliderImages.Where(x => x.SliderId == 1).OrderBy(x => x.SliderImageId).ToList();

            ViewBag.Sliderlar = sliderlar;

            var deger = MailSend(cf);

            ViewBag.MailSend = deger;

            ViewBag.menuSpecialsEng = db.MenuLanguages.Where(m => m.MenuId == 40 && m.LanguageId == 2).FirstOrDefault();
            ViewBag.menuReservationsEng = db.MenuLanguages.Where(m => m.MenuId == 29 && m.LanguageId == 2).FirstOrDefault();
            return View();
        }

        private static bool MailSend(ContactForm cf)
        {
            string MailHost = "smtp.gmail.com";
            int MailPort = 587;
            string MailUserName = "oludeniz@oysterresidences.com";
            string MailPassword = "dnz957oyst";
            string ContactMailAddress = GeneralFunctions.GetSettingValue("ContactMail");
            int MailSsl = 1;
            int MailCredantials = 1;

            var deger = SendMail(MailHost, MailPort, MailUserName, MailPassword, ContactMailAddress, MailUserName, MailSsl, MailCredantials, cf.NameSurname, cf.Subject, cf.Email, cf.Message, cf.Date, cf.DateGiris, cf.DateCikis, cf.Tel);
            return deger;
        }

        [HttpGet]
        public JsonResult asd(MenuLanguages menu)
        {
            //var modal = db.Modal.Where(x => x.MenuId == menu.MenuId).ToList();
            var modal = db.ContentLanguages.Where(x => x.ContentNames.TopContentId == menu.MenuId).ToList();
#pragma warning disable CS0219 // The variable 'name' is assigned but its value is never used
#pragma warning disable CS0219 // The variable 'footer' is assigned but its value is never used
            var title = ""; var description = ""; var footer = ""; var name = "";
#pragma warning restore CS0219 // The variable 'footer' is assigned but its value is never used
#pragma warning restore CS0219 // The variable 'name' is assigned but its value is never used
            if (modal != null)
            {
                foreach (var item in modal.Where(x => x.LanguageId == menu.LanguageId))
                {
                    title = item.Title;
                    description = item.Description;
                }
            }
            List<SliderImages> sliderImages = new List<SliderImages>();
            var images = db.SliderImages.Where(x => x.SliderId == menu.SliderId).ToList();
            foreach (var item in images)
            {
                sliderImages.Add(new SliderImages { SliderImageId = item.SliderImageId, SliderId = item.SliderId, ImageName = item.ImageName, Title = item.Title, Description = item.Description, Status = item.Status });
            }

            return Json(new { title = title, description = description, sliderImages }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult TopMenu()
        {
            try
            {
                var List = (from x in db.MenuNames join m in db.MenuLanguages on x.MenuId equals m.MenuId where x.Status == 1 && x.TopMenuId == 0 && x.MenuPositionType == 1 orderby x.OrderNumber ascending select m).ToList();

                var MenuList = (from x in db.MenuNames join m in db.MenuLanguages on x.MenuId equals m.MenuId where x.Status == 1 && x.MenuPositionType == 1 orderby x.OrderNumber ascending select m).ToList();

                ViewBag.MenuList = MenuList;

                return View(List);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public ActionResult TopMenuPage()
        {
            try
            {
                var List = (from x in db.MenuNames join m in db.MenuLanguages on x.MenuId equals m.MenuId where x.Status == 1 && x.TopMenuId == 0 && x.MenuPositionType == 1 orderby x.OrderNumber ascending select m).ToList();

                var MenuList = (from x in db.MenuNames join m in db.MenuLanguages on x.MenuId equals m.MenuId where x.Status == 1 && x.MenuPositionType == 1 orderby x.OrderNumber ascending select m).ToList();

                ViewBag.MenuList = MenuList;

                return View(List);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public ActionResult FotMenu()
        {
            try
            {
                var List = (from x in db.MenuNames join m in db.MenuLanguages on x.MenuId equals m.MenuId where x.Status == 1 && x.TopMenuId == 0 && x.MenuPositionType == 2 orderby x.OrderNumber ascending select m).ToList();

                var MenuList = (from x in db.MenuNames join m in db.MenuLanguages on x.MenuId equals m.MenuId where x.Status == 1 && x.MenuPositionType == 2 orderby x.OrderNumber ascending select m).ToList();

                ViewBag.MenuList = MenuList;

                return View(List);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public ActionResult FotMenuPage()
        {
            try
            {
                var List = (from x in db.MenuNames join m in db.MenuLanguages on x.MenuId equals m.MenuId where x.Status == 1 && x.TopMenuId == 0 && x.MenuPositionType == 2 orderby x.OrderNumber ascending select m).ToList();

                var MenuList = (from x in db.MenuNames join m in db.MenuLanguages on x.MenuId equals m.MenuId where x.Status == 1 && x.MenuPositionType == 2 orderby x.OrderNumber ascending select m).ToList();

                ViewBag.MenuList = MenuList;

                return View(List);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public ActionResult MobilMenu()
        {
            try
            {
                var List = (from x in db.MenuNames join m in db.MenuLanguages on x.MenuId equals m.MenuId where x.Status == 1 && x.TopMenuId == 0 orderby x.OrderNumber ascending select m).ToList();

                var MenuList = (from x in db.MenuNames join m in db.MenuLanguages on x.MenuId equals m.MenuId where x.Status == 1 orderby x.OrderNumber ascending select m).ToList();

                ViewBag.MenuList = MenuList;

                return View(List);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public ActionResult MobilMenuPage()
        {
            try
            {
                var List = (from x in db.MenuNames join m in db.MenuLanguages on x.MenuId equals m.MenuId where x.Status == 1 && x.MenuPositionType == 1 orderby x.OrderNumber ascending select m).ToList();

                //  var MenuList = (from x in db.MenuNames join m in db.MenuLanguages on x.MenuId equals m.MenuId where x.Status == 1 && x.MenuPositionType == 1 orderby x.OrderNumber ascending select m).ToList();

                // ViewBag.MenuList = MenuList;

                return View(List);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public class ContactForm
        {
            public int BlogLanguageId { get; set; }
            public string NameSurname { get; set; }
            public string Subject { get; set; }
            public string Tel { get; set; }
            public string Email { get; set; }
            public string Message { get; set; }
            public string language { get; set; }
            public string Date { get; set; }
            public string DateGiris { get; set; }
            public string DateCikis { get; set; }
        }

        public static bool SendMail(string Host, int Port, string Username, string Password, string To, string From, int Ssl, int Credantials, string ContactNameSurname, string Subject, string MailAddress, string ContactMessage, string Date, string DateGiris, string DateCikis, string Tel)
        {
            try
            {
                string tarih = "Seçilmedi";
                if (Subject == "Book a Table")
                {
                    tarih = "<td>Tarih: </td><td>" + Date + "</td>";
                }
                else
                {
                    tarih = "<td>Giriş Tarihi: </td><td>" + DateGiris + "</td>" + "<td>Çıkış Tarihi: </td><td>" + DateCikis + "</td>";
                }

                string host = Host;
                int port = Port;
                string username = Username;
                string password = Password;
                string from = From;
                MailMessage mailim = new MailMessage();

                mailim.To.Add(To);  // kime gidecek
                mailim.From = new MailAddress(username);   //kimden gidecek
                mailim.Subject = "Oyster " + Subject;   // e-mail konusu
                mailim.IsBodyHtml = true;
                mailim.Body = ("<table>" +
                                "<tr>" +
                                    "<td>Ad Soyad: </td><td>" + ContactNameSurname + "</td>" +
                                "</tr>" + "<tr>" +
                                    "<td>Tel: </td><td>" + Tel + "</td>" +
                                "</tr>" +
                                "<tr>" +
                                    "<td>E-Mail: </td><td>" + MailAddress + "</td>" +
                                "</tr>" +
                                 "<tr>" +
                                    tarih +
                                "</tr>" +
                                "<tr>" +
                                    "<td>Konu: </td><td>" + Subject + "</td>" +
                                "</tr>" +
                                "<tr>" +
                                    "<td> Mesaj: </td><td>" + ContactMessage + "</td>" +
                                "</tr>" +
                                "</table>");   //e-mail içeriği

                NetworkCredential yetki = new NetworkCredential(username, password);

                SmtpClient mail_client = new SmtpClient(host, port);

                if (Ssl == 1)
                {
                    mail_client.EnableSsl = true;
                }
                else
                {
                    mail_client.EnableSsl = false;
                }
                if (Credantials == 1)
                {
                    mail_client.UseDefaultCredentials = true;
                }
                else
                {
                    mail_client.UseDefaultCredentials = false;
                }

                mail_client.Credentials = yetki;
                mail_client.Send(mailim);

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private class GoogleResponse
        {
#pragma warning disable CS0649 // Field 'HomeController.GoogleResponse.success' is never assigned to, and will always have its default value false
            public bool success;
#pragma warning restore CS0649 // Field 'HomeController.GoogleResponse.success' is never assigned to, and will always have its default value false
#pragma warning disable CS0649 // Field 'HomeController.GoogleResponse.errorcodes' is never assigned to, and will always have its default value null
            public string errorcodes;
#pragma warning restore CS0649 // Field 'HomeController.GoogleResponse.errorcodes' is never assigned to, and will always have its default value null
        }
    }
}