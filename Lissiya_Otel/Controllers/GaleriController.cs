﻿using Lissiya_Otel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Lissiya_Otel.Controllers
{
    public class GaleriController : Controller
    {
        //
        // GET: /Galeri/

        lissiyaEntities db = new lissiyaEntities();

        public ActionResult Index(int ID)
        {

            //var galeriId = db.GalleryNames.Where(x=>x.MenuId==ID).ToList();
           
            var galeriResimleri = db.GalleryImages.Where(x => x.GalleryId == ID).ToList();

            //ViewBag.galeriResimleri = galeriResimleri;

            return View(galeriResimleri);
        }

    }
}
