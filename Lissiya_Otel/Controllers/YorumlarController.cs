﻿using Lissiya_Otel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;

namespace Lissiya_Otel.Controllers
{
    public class YorumlarController : Controller
    {
        //
        // GET: /Yorumlar/
        lissiyaEntities db = new lissiyaEntities();

        public ActionResult Index(int? page)
        {
            int pageIndex = page ?? 1;
            var language = Lissiya_Otel.Functions.GeneralFunctions.Resolve(RouteData.Values["lang"]);
           
                if (language == "en")
                {
                    var yorumlar = db.ClientTestimonials.Where(x => x.Status == 1 && x.DilSecenegi == 2).OrderBy(x => x.OrderNumber).ToPagedList(pageIndex, 5);
                    return View(yorumlar);
                }
                else
                {
                    var yorumlar = db.ClientTestimonials.Where(x => x.Status == 1 && x.DilSecenegi == 1).OrderBy(x => x.OrderNumber).ToPagedList(pageIndex, 5);
                    return View(yorumlar);
                }
            
        }

        [HttpPost]
        public ActionResult Index(ClientTestimonials tst)
        {
            {
                try
                {
                    var lang = Request.Form["lang"];
                    
                        var row = new ClientTestimonials
                        {
                            InsertedDate = DateTime.Now,
                            OrderNumber = 0,
                            Status = 0,
                            TestimonialEN = tst.Testimonial,
                            YazarEN = tst.Yazar,
                            ClientEN = tst.Client,
                            Testimonial = tst.Testimonial,
                            Yazar = tst.Yazar,
                            Client = tst.Client,
                            DilSecenegi =(lang=="en"?2:1),
                            CommentDate = DateTime.Now
                        };
                        db.ClientTestimonials.Add(row);
                        db.SaveChanges();
                        db.Dispose();
                    if(lang=="en")
                        return RedirectToAction("Index", "Yorumlar", new { id = 1 });
                  
                        return RedirectToRoute("Yorumlar");
                    

                    //TempData["Success"] = GeneralFunctions.ReturnSuccess(1);
                    //GeneralFunctions.SendMail("mail.mckare.com.tr", 587, "eray@mckare.com.tr", "Rzi53ist34", "huseyin@mckare.com.tr", "lissiyahotel", 0, 0, frm.NameSurname, frm.Email, frm.VarisTarihi, frm.AyrilisTarihi, Convert.ToInt32(frm.KisiSayisi), frm.Message);

                    //return RedirectToAction("Index", "Yorumlar", new { id = 1 });

                }

                catch (Exception)
                {

                }
            }

            return View();
        }

    }
}
