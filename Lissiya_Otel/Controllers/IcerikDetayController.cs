﻿using Lissiya_Otel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Lissiya_Otel.Controllers
{
    public class IcerikDetayController : Controller
    {
        //
        // GET: /IcerikDetay/
        lissiyaEntities db = new lissiyaEntities();

        public ActionResult Index(int id)
        {
            var icerikDetayi = (from ol in db.ContentLanguages
                              join ozn in db.ContentNames on ol.ContentId equals ozn.ContentId orderby ozn.OrderNumber ascending
                                where ozn.ContentId == id
                              select ol).ToList();
            ViewBag.icerikSeo = icerikDetayi;

            return View(icerikDetayi);
        }

    }
}
