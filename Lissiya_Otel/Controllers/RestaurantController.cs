﻿using Lissiya_Otel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Lissiya_Otel.Controllers
{
    public class RestaurantController : Controller
    {
        //
        // GET: /Restaurant/

        lissiyaEntities db = new lissiyaEntities();

        public ActionResult Index()
        {
            var restorantlar = db.ProjectLanguages.OrderBy(x => x.ProjectId).ToList();

            return View(restorantlar);
        }

        public ActionResult Detay(int id)
        {
            var resimler = (from ol in db.ProjectImageLanguages
                            join ozn in db.ProjectLanguages on ol.ProjectLanguageId equals ozn.ProjectLanguageId
                            where ozn.ProjectId == id
                            select ol).ToList();
            ViewBag.resimler = resimler;

            // SEO
            ViewBag.restaurantSeo = db.ProjectLanguages.Where(x => x.ProjectId == id).ToList();

            return View();
        }

    }
}
