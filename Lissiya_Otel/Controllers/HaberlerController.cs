﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
namespace Lissiya_Otel.Controllers
{
    public class HaberlerController : Controller
    {
        //
        // GET: /Haberler/

        Lissiya_Otel.Models.lissiyaEntities db = new Models.lissiyaEntities();

        public ActionResult Index(int? page)
        {
            int pageIndex = page ?? 1;
            var language = Lissiya_Otel.Functions.GeneralFunctions.Resolve(RouteData.Values["lang"]);
            if (language == "en")
            {
                var haberler = (from x in db.NewsNames join m in db.NewsLanguages on x.NewsId equals m.NewsId where x.Status == 1 && m.LanguageId == 2 orderby x.OrderNumber ascending select m).ToPagedList(pageIndex, 10);

                return View(haberler);
            }
            else
            {
                var haberler = (from x in db.NewsNames join m in db.NewsLanguages on x.NewsId equals m.NewsId where x.Status == 1 && m.LanguageId == 1 orderby x.OrderNumber ascending select m).ToPagedList(pageIndex, 10);

                return View(haberler);
            }

        }

    }
}
