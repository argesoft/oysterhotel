//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Lissiya_Otel.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class MenuLanguages
    {
        public int MenuLanguageId { get; set; }
        public Nullable<int> LanguageId { get; set; }
        public Nullable<int> MenuId { get; set; }
        public Nullable<int> Type { get; set; }
        public Nullable<int> ContentId { get; set; }
        public string Title { get; set; }
        public string Url { get; set; }
        public string MenuDocument { get; set; }
        public Nullable<int> Status { get; set; }
        public Nullable<int> OrderNumber { get; set; }
        public Nullable<System.DateTime> InsertedDate { get; set; }
        public Nullable<int> SliderId { get; set; }
    
        public virtual MenuNames MenuNames { get; set; }
        public virtual SliderNames SliderNames { get; set; }
    }
}
