//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Lissiya_Otel.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class DepartmentNames
    {
        public DepartmentNames()
        {
            this.DepartmentLanguages = new HashSet<DepartmentLanguages>();
            this.MemberNames = new HashSet<MemberNames>();
        }
    
        public int DepartmentId { get; set; }
        public string Name { get; set; }
        public Nullable<int> OrderNumber { get; set; }
        public Nullable<int> Status { get; set; }
        public Nullable<System.DateTime> InsertedDate { get; set; }
    
        public virtual ICollection<DepartmentLanguages> DepartmentLanguages { get; set; }
        public virtual ICollection<MemberNames> MemberNames { get; set; }
    }
}
