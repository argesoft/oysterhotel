//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Lissiya_Otel.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class BlogLanguages
    {
        public int BlogLanguageId { get; set; }
        public Nullable<int> BlogId { get; set; }
        public Nullable<int> LanguageId { get; set; }
        public string Title { get; set; }
        public string Summary { get; set; }
        public string Description { get; set; }
        public string ImageName { get; set; }
        public string Author { get; set; }
        public Nullable<System.DateTime> Date { get; set; }
        public Nullable<int> Hit { get; set; }
        public string SeoTitle { get; set; }
        public string SeoKeywords { get; set; }
        public string SeoDescription { get; set; }
        public Nullable<int> Status { get; set; }
        public Nullable<int> OrderNumber { get; set; }
        public Nullable<System.DateTime> InsertedDate { get; set; }
    
        public virtual BlogNames BlogNames { get; set; }
    }
}
