//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Lissiya_Otel.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class ApplicationForms
    {
        public int ApplicationFormId { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public string Message { get; set; }
        public string CV { get; set; }
        public Nullable<int> Status { get; set; }
        public Nullable<System.DateTime> InsertedDate { get; set; }
    }
}
